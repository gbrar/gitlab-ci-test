

    select 
    cond.smsf_condition_number as condition_number,
    cond.smsf_condition as condition,
    cond.smsf_condition_dtl as condition_dtl,
    cond.smsf_condition_start_dt as condition_start_dt,
    cap.smsf_num as num_capacity, 
    cap.smsf_locality as locality, 
    cap.smsf_susp_start_dt as susp_start_dt, 
    cap.smsf_name as name, 
    cap.smsf_post_code as post_code, 
    cap.register_name as register_name, 
    cap.smsf_capacity_firm_name as firm_name, 
    cap.smsf_attribute_type as attribute_type, 
    cap.smsf_status as status, 
    cap.smsf_person_abn as abn, 
    cap.smsf_capacity as capacity, 
    cap.smsf_reg_dt as reg_dt, 
    cap.smsf_susp_end_dt as susp_end_dt, 
    cap.smsf_state as state, 
    cap.smsf_capacity_start_dt as capacity_start_dt
    from smsf_condition cond, smsf_capacity cap
    where cond.smsf_num = cap.smsf_num

