

    select 
smsf_num as num_capacity, 
smsf_locality as locality, 
smsf_susp_start_dt as susp_start_dt, 
smsf_name as name, 
smsf_post_code as post_code, 
register_name as register_name, 
smsf_capacity_firm_name as firm_name, 
smsf_attribute_type as attribute_type, 
smsf_status as status, 
smsf_person_abn as abn, 
smsf_capacity as capacity, 
smsf_reg_dt as reg_dt, 
smsf_susp_end_dt as susp_end_dt, 
smsf_state as state, 
smsf_capacity_start_dt as capacity_start_dt
from smsf_capacity

select 
smsf_num as num_capacity,
smsf_condition_number as condition_number,
smsf_condition as condition,
smsf_condition_dtl as condition_dtl,
smsf_condition_start_dt as condition_start_dt
from smsf_condition

