

select 
  aud.row_id as row_id,
  aud.auditor_id as auditor_id,
  aud.register_name as register_name,
  aud.status as status,
  aud.reg_dt as reg_dt,
  aud.susp_start_dt as susp_start_dt,
  aud.susp_end_dt as susp_end_dt,
  aud.location_id as location_id,
  aud.attrib_type as attrib_type,
  aud.capacity_id as capacity_id,
  '60' as securityclearance
from 
  (select 
    *
  from
  (select 
    row_number() over (partition by row_id order by extract_ts desc) as rn,
    *
  from 
    smsf.auditor
  where
    unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') > <track-time>
  ) as aud_temp
where aud_temp.rn=1
) as aud

          
select 
  aud.row_id as row_id,
  concat(aud.row_id, '#', att.id, '#', att.source) as subdoc_id,
  att.id as attachment_id,
  att.hdfs_location as hdfs_location,
  concat("http://10.10.10.150:14000/webhdfs/v1",att.hdfs_location) as hdfs_link,
  att.filename as filename,
  att.ext as ext,
  att.source as source
from 
  (select 
    *
  from
   (select 
      row_number() over (partition by row_id order by extract_ts desc) as rn,
      *
    from 
      smsf.auditor
   ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
    (select 
      row_number() over (partition by auditor_id order by extract_ts desc) as rn,
      *
    from 
      smsf.aud_id_to_intrnl_id
    ) as idmap_temp
  where idmap_temp.rn=1
  ) as idmap,
  (select 
    *
  from
    (select 
      row_number() over (partition by internal_id order by extract_ts desc) as rn,
      *
    from 
      smsf.id
    where
      type_cd = 5 and
      status = 5
    ) as id_temp
  where id_temp.rn=1
  ) as id,
  (select 
    *
  from
    (select 
      row_number() over (partition by id order by extract_ts desc) as rn,
      *
    from 
      smsf.attachments
    where
      source_type = 1
    ) as att_temp
  where att_temp.rn=1
  ) as att
where
  aud.auditor_id = idmap.auditor_id and
  idmap.internal_id = id.internal_id and
  cast(id.value as bigint) = att.abn and
  (unix_timestamp(aud.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(idmap.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(id.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(att.extract_ts, 'yyyyMMdd_HHmmss') > <track-time>)
                
select 
  aud.row_id as row_id,

  concat(aud.row_id, '#', att.id, '#', att.source) as subdoc_id,
  att.id as attachment_id,
  att.hdfs_location as hdfs_location,
  concat("http://10.10.10.150:14000/webhdfs/v1",att.hdfs_location) as hdfs_link,
  att.filename as filename,
  att.ext as ext,
  att.source as source
from 
  (select 
    *
  from
   (select 
      row_number() over (partition by row_id order by extract_ts desc) as rn,
      *
    from 
      smsf.auditor
   ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
    (select 
      row_number() over (partition by auditor_id order by extract_ts desc) as rn,
      *
    from 
      smsf.aud_id_to_intrnl_id
    ) as idmap_temp
  where idmap_temp.rn=1
  ) as idmap,
  (select 
    *
  from
    (select 
      row_number() over (partition by internal_id order by extract_ts desc) as rn,
      *
    from 
      smsf.id
    where
      type_cd = 5 and
      status = 5
    ) as id_temp
  where id_temp.rn=1
  ) as id,
  (select 
    *
  from
    (select 
      row_number() over (partition by id order by extract_ts desc) as rn,
      *
    from 
      smsf.attachments
    where
      source_type = 2
    ) as att_temp
  where att_temp.rn=1
  ) as att
where
  aud.auditor_id = idmap.auditor_id and
  idmap.internal_id = id.internal_id and
  cast(id.value as bigint) = att.abn and
  (unix_timestamp(aud.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(idmap.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(id.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(att.extract_ts, 'yyyyMMdd_HHmmss') > <track-time>)    
                
select 
  cond.par_row_id as row_id,
  concat(cond.par_row_id, '#', cond.condition_num, '#', cond.attrib_type) as subdoc_id,
  cond.condition_num as condition_num,
  cond.cond_shrt as cond_shrt,
  cond.cond_long as cond_long,
  cond.cond_start_dt as cond_start_dt,
  cond.attrib_type as source
from 
(select 
  *
from
  (select 
    row_number() over (partition by condition_num order by extract_ts desc) as rn,
    *
  from 
    smsf.condition
  where
    unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') > <track-time>
  ) as cond_temp
where cond_temp.rn=1
) as cond    
                
select 
  aud.row_id as row_id,
  cap.capacity_id as capacity_id,
  cap.name as name,
  cap.firm_name as firm_name,
  cap.start_date as start_date
from 
  (select 
    *
  from
  (select 
    row_number() over (partition by row_id order by extract_ts desc) as rn,
    *
  from 
    smsf.auditor
  where
    unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') > <track-time>
  ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
    (select 
      row_number() over (partition by capacity_id order by extract_ts desc) as rn,
      *
    from 
      smsf.capacity
    where
      unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') > <track-time>
    ) as cap_temp
  where cap_temp.rn=1
  ) as cap
where
  cap.capacity_id = aud.capacity_id								
                 
select 
  aud.row_id as row_id,
  loc.location_id as location_id,
  loc.locality as locality,
  loc.post_code as post_code,
  loc.state as state
from 
  (select 
    *
  from
    (select 
      row_number() over (partition by row_id order by extract_ts desc) as rn,
      *
    from 
      smsf.auditor
    ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
    (select 
      row_number() over (partition by location_id order by extract_ts desc) as rn,
      *
    from 
      smsf.address
    ) as loc_temp
  where loc_temp.rn=1
  ) as loc
where
  loc.location_id = aud.location_id and
  (unix_timestamp(aud.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(loc.extract_ts, 'yyyyMMdd_HHmmss') > <track-time>)
                 
select 
  aud.row_id as row_id,
  name.value as auditor_name
from 
  (select 
    *
  from
    (select 
      row_number() over (partition by row_id order by extract_ts desc) as rn,
      *
    from 
      smsf.auditor
    ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
    (select 
      row_number() over (partition by auditor_id order by extract_ts desc) as rn,
      *
    from 
      smsf.aud_id_to_intrnl_id
    ) as idmap_temp
  where idmap_temp.rn=1
  ) as idmap,
  (select 
    *
  from
    (select 
      row_number() over (partition by internal_id order by extract_ts desc) as rn,
      *
    from 
      smsf.name
    where
      type_cd = 5 and
      status = 5
    ) as name_temp
  where name_temp.rn=1
  ) as name
where
  aud.auditor_id = idmap.auditor_id and
  idmap.internal_id = name.internal_id and
  (unix_timestamp(aud.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(idmap.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(name.extract_ts, 'yyyyMMdd_HHmmss') > <track-time>)
                 
select 
  aud.row_id as row_id,
  id.value as auditor_abn
from 
  (select 
    *
  from
  (select 
    row_number() over (partition by row_id order by extract_ts desc) as rn,
    *
  from 
    smsf.auditor
  ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
  (select 
    row_number() over (partition by auditor_id order by extract_ts desc) as rn,
    *
  from 
    smsf.aud_id_to_intrnl_id
  ) as idmap_temp
  where idmap_temp.rn=1
  ) as idmap,
  (select 
    *
  from
  (select 
    row_number() over (partition by internal_id order by extract_ts desc) as rn,
    *
  from 
    smsf.id
  where
    type_cd = 5 and
    status = 5
  ) as id_temp
  where id_temp.rn=1
  ) as id
where
  aud.auditor_id = idmap.auditor_id and
  idmap.internal_id = id.internal_id and
  (unix_timestamp(aud.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(idmap.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(id.extract_ts, 'yyyyMMdd_HHmmss') > <track-time>)
                 
select 
  aud.row_id as row_id,
  id.value as auditor_number
from 
  (select 
    *
  from
  (select 
    row_number() over (partition by row_id order by extract_ts desc) as rn,
    *
  from 
    smsf.auditor
  ) as aud_temp
  where aud_temp.rn=1
  ) as aud,
  (select 
    *
  from
    (select 
      row_number() over (partition by auditor_id order by extract_ts desc) as rn,
      *
    from 
      smsf.aud_id_to_intrnl_id
    ) as idmap_temp
  where idmap_temp.rn=1
  ) as idmap,
  (select 
    *
  from
  (select 
    row_number() over (partition by internal_id order by extract_ts desc) as rn,
    *
  from 
    smsf.id
  where
    type_cd = 10 and
    status = 5
  ) as id_temp
  where id_temp.rn=1
  ) as id
where
  aud.auditor_id = idmap.auditor_id and
  idmap.internal_id = id.internal_id and
  (unix_timestamp(aud.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(idmap.extract_ts, 'yyyyMMdd_HHmmss') > <track-time> or
  unix_timestamp(id.extract_ts, 'yyyyMMdd_HHmmss') > <track-time>)
                
