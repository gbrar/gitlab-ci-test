

select 
 aud.row_id,
 aud.auditor_id,
 aud.register_name,
 aud.status,
 aud.reg_dt,
 aud.susp_start_dt,
 aud.susp_end_dt,
 aud.location_id,
 aud.location_id as hdfs_location,
 aud.attrib_type as projectClassification,
 aud.attrib_type,
 aud.capacity_id,
 '60' as 'securityclearance'
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud


select 
 aud.row_id,
 concat(aud.row_id, '#', att.id, '#', att.source) as subdoc_id,
 att.hdfs_location as abr_hdfs_location,
 concat("http://10.10.10.150:14000/webhdfs/v1",att.hdfs_location) as abr_hdfs_link,
 att.filename as abr_filename,
 att.ext as abr_ext,
 att.source as abr_source
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by auditor_id order by extract_ts desc) as rn,
 *
 from 
 smsf.aud_id_to_intrnl_id
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as idmap_temp
 where idmap_temp.rn=1
 ) as idmap,
 (select 
 *
 from
 (select 
 row_number() over (partition by internal_id order by extract_ts desc) as rn,
 *
 from 
 smsf.id
 where
 type_cd = 5 and
 status = 5 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as id_temp
 where id_temp.rn=1
 ) as id,
 (select 
 *
 from
 (select 
 row_number() over (partition by id order by extract_ts desc) as rn,
 *
 from 
 smsf.attachments
 where
 source_type = 1 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as att_temp
 where att_temp.rn=1
 ) as att
where
 aud.auditor_id = idmap.auditor_id and
 idmap.internal_id = id.internal_id and
 cast(id.value as bigint) = att.abn
 
select 
 aud.row_id,
 concat(aud.row_id, '#', att.id, '#', att.source) as subdoc_id,
 att.hdfs_location as asic_hdfs_location,
 concat("http://10.10.10.150:14000/webhdfs/v1",att.hdfs_location) as asic_hdfs_link,
 att.filename as asic_filename,
 att.ext as asic_ext,
 att.source as asic_source
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by auditor_id order by extract_ts desc) as rn,
 *
 from 
 smsf.aud_id_to_intrnl_id
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as idmap_temp
 where idmap_temp.rn=1
 ) as idmap,
 (select 
 *
 from
 (select 
 row_number() over (partition by internal_id order by extract_ts desc) as rn,
 *
 from 
 smsf.id
 where
 type_cd = 5 and
 status = 5 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as id_temp
 where id_temp.rn=1
 ) as id,
 (select 
 *
 from
 (select 
 row_number() over (partition by id order by extract_ts desc) as rn,
 *
 from 
 smsf.attachments
 where
 source_type = 2 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as att_temp
 where att_temp.rn=1
 ) as att
where
 aud.auditor_id = idmap.auditor_id and
 idmap.internal_id = id.internal_id and
 cast(id.value as bigint) = att.abn 

select 
 cond.par_row_id as row_id,
 concat(cond.par_row_id, '#', cond.condition_num, '#', cond.attrib_type) as subdoc_id,
 cond.condition_num,
 cond.cond_shrt,
 cond.cond_long,
 cond.cond_start_dt
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by condition_num order by extract_ts desc) as rn,
 *
 from 
 smsf.condition
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as cond_temp
 where cond_temp.rn=1
 ) as cond 

    select    
    cap.name as dummy_join_name,
    cap.firm_name as dummy_join_firm_name    
    from 
    (select 
    *
    from
    (select 
    row_number() over (partition by row_id order by extract_ts desc) as rn,
    *
    from 
    smsf.auditor
    where
    unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
    ) as aud_temp
    where aud_temp.rn=1
    ) as aud,
    (select 
    *
    from
    (select 
    row_number() over (partition by capacity_id order by extract_ts desc) as rn,
    *
    from 
    smsf.capacity
    where
    unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
    ) as cap_temp
    where cap_temp.rn=1
    ) as cap
    where
    cap.capacity_id = aud.capacity_id
   
select 
 aud.row_id,
 cap.capacity_id,
 cap.name,
 cap.firm_name,
 cap.start_date
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by capacity_id order by extract_ts desc) as rn,
 *
 from 
 smsf.capacity
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as cap_temp
 where cap_temp.rn=1
 ) as cap
where
 cap.capacity_id = aud.capacity_id
 
select 
 aud.row_id,
 loc.location_id,
 loc.locality,
 loc.post_code,
 loc.state
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by location_id order by extract_ts desc) as rn,
 *
 from 
 smsf.locality
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as loc_temp
 where loc_temp.rn=1
 ) as loc
where
 loc.location_id = aud.location_id
 
select 
 aud.row_id,
 name.value as auditor_name
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by auditor_id order by extract_ts desc) as rn,
 *
 from 
 smsf.aud_id_to_intrnl_id
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as idmap_temp
 where idmap_temp.rn=1
 ) as idmap,
 (select 
 *
 from
 (select 
 row_number() over (partition by internal_id order by extract_ts desc) as rn,
 *
 from 
 smsf.name
 where
 type_cd = 5 and
 status = 5 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as name_temp
 where name_temp.rn=1
 ) as name
where
 aud.auditor_id = idmap.auditor_id and
 idmap.internal_id = name.internal_id
 
select 
 aud.row_id,
 id.value as auditor_abn
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by auditor_id order by extract_ts desc) as rn,
 *
 from 
 smsf.aud_id_to_intrnl_id
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as idmap_temp
 where idmap_temp.rn=1
 ) as idmap,
 (select 
 *
 from
 (select 
 row_number() over (partition by internal_id order by extract_ts desc) as rn,
 *
 from 
 smsf.id
 where
 type_cd = 5 and
 status = 5 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as id_temp
 where id_temp.rn=1
 ) as id
where
 aud.auditor_id = idmap.auditor_id and
 idmap.internal_id = id.internal_id
 
select 
 aud.row_id,
 id.value as auditor_number
from 
 (select 
 *
 from
 (select 
 row_number() over (partition by row_id order by extract_ts desc) as rn,
 *
 from 
 smsf.auditor
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as aud_temp
 where aud_temp.rn=1
 ) as aud,
 (select 
 *
 from
 (select 
 row_number() over (partition by auditor_id order by extract_ts desc) as rn,
 *
 from 
 smsf.aud_id_to_intrnl_id
 where
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as idmap_temp
 where idmap_temp.rn=1
 ) as idmap,
 (select 
 *
 from
 (select 
 row_number() over (partition by internal_id order by extract_ts desc) as rn,
 *
 from 
 smsf.id
 where
 type_cd = 10 and
 status = 5 and
 unix_timestamp(extract_ts, 'yyyyMMdd_HHmmss') ;gt ;lttrack-time;gt
 ) as id_temp
 where id_temp.rn=1
 ) as id
where
 aud.auditor_id = idmap.auditor_id and
 idmap.internal_id = id.internal_id

