select
  caseid as caseid,
  casesecrtyclsncd as casesecrtyclsncd,
  casespclintstind as casespclintstind,
  casetaxofcstfind as casetaxofcstfind,
  hwicd as hwicd,
  casetyp as casetyp,
  casesubtyp as casesubtyp,
  concat_ws("|", casetyp, casesubtyp) as hiercasetyp,
  casests as casests,
  casesubsts as casesubsts,
  concat_ws("|", casests, casesubsts) as hiercasests,
  lastupdnmtxt as lastupdnmtxt,
  casecrtddt / 1000 as casecrtddt,
  caselastupddt / 1000 as caselastupddt,
  case when casecmplndt is not null then casecmplndt / 1000 else casecmplndt end as casecmplndt,
  casebsltxt as casebsltxt,
  caseprdnm as caseprdnm,
  caseprjtxt as caseprjtxt,
  caseprjtypcd as caseprjtypcd,
  concat_ws("|", caseprjtypcd , caseprjtxt) as hierprjtyp,
  casedescn as casedescn
from case_data.case_details
where (ingesttm / 1000) > <track-time>

select 
  cse_att.caseid as caseid,
  concat_ws("#", cse_att.caseid, cse_att.attid) as subdocid,
  cse_att.attid as childid,
  "Case Attachment" as notetype,  
  cse_att.iodlid as iodlid,
  cse_att.fileext as fileext,
  cse_att.filename as filename,
  concat(cse_att.filename, ".", cse_att.fileext) as subdoc_title,
  concat("http://",regexp_replace(cseatt.documentimg,'\\?op\=OPEN$','')) as hdfslocation,
  case when (lower(cse_att.fileext) like 'doc%' or lower(cse_att.fileext) = 'rtf' or lower(cse_att.fileext) = 'txt' or lower(cse_att.fileext) = 'wpd') then "Document" else
    case when (lower(cse_att.fileext) = 'csv' or lower(cse_att.fileext) = 'xls' or lower(cse_att.fileext) = 'xlsm') then "Spreadsheet" else
      case when (lower(cse_att.fileext) = 'msg') then "Email" else
        case when (lower(cse_att.fileext) = 'pdf') then "PDF" else
          case when (lower(cse_att.fileext) = 'ppt') then "Presentation" else
            case when (lower(cse_att.fileext) like 'tif%' or lower(cse_att.fileext) = 'bmp' or lower(cse_att.fileext) = 'jpg' or lower(cse_att.fileext) = 'png') then "Image" 
            else 
              "Other"
            end
          end
        end
      end
    end
  end as filetype
from case_data.case_attachment as cse_att, case_data.caseattachment as cseatt
where 
  cse_att.iodlid = cseatt.documentiodlid and
  ((cse_att.ingesttm / 1000) > <track-time> or
  (cseatt.ingesttm / 1000) > <track-time>)

select 
  acty.caseid as caseid,
  concat_ws("#", acty.caseid, acty_att.act_id, acty_att.att_id) as subdocid,
  acty_att.act_id as childid,
  acty_att.act_id as actyid,
  acty_att.att_id as grandchildid,
  "Activity Attachment" as notetype,  
  acty.type as type,
  concat_ws("|", "Activity", acty.type) as hiertyp,
  acty_att.lastupdnmtxt as lastupduserid,
  acty.status as status,
  acty.activitymailrecddt / 1000 as actymailrcptdt,
  coalesce(acty.finalisationcd, 'Not Applicable') as fnlsncdtypnm,
  coalesce(acty.capabilitycd, 'Not Applicable') as taxofccpbltynm,
  acty_att.iodlid as iodlid,
  acty_att.fileext as fileext,
  acty_att.filename as filename,
  concat(acty_att.filename, ".", acty_att.fileext) as subdoc_title,
  concat("http://",regexp_replace(cseatt.documentimg,'\\?op\=OPEN$','')) as hdfslocation,
  case when (lower(acty_att.fileext) like 'doc%' or lower(acty_att.fileext) = 'rtf' or lower(acty_att.fileext) = 'txt' or lower(acty_att.fileext) = 'wpd') then "Document" else
    case when (lower(acty_att.fileext) = 'csv' or lower(acty_att.fileext) = 'xls' or lower(acty_att.fileext) = 'xlsm') then "Spreadsheet" else
      case when (lower(acty_att.fileext) = 'msg') then "Email" else
        case when (lower(acty_att.fileext) = 'pdf') then "PDF" else
          case when (lower(acty_att.fileext) = 'ppt') then "Presentation" else
            case when (lower(acty_att.fileext) like 'tif%' or lower(acty_att.fileext) = 'bmp' or lower(acty_att.fileext) = 'jpg' or lower(acty_att.fileext) = 'png') then "Image" 
            else 
              "Other"
            end
          end
        end
      end
    end
  end as filetype
from case_data.activity_attachment as acty_att, case_data.caseattachment as cseatt, case_data.activity as acty
where 
  acty.act_id = acty_att.act_id and
  acty_att.iodlid = cseatt.documentiodlid and
  ((acty_att.ingesttm / 1000) > <track-time> or
  (cseatt.ingesttm / 1000) > <track-time> or
  (acty.ingesttm / 1000) > <track-time>)


select 
  caseid as caseid,
  concat_ws("#", caseid, act_id) as subdocid,
  act_id as childid,
  act_id as actyid,
  "Activity" as notetype,  
  "Siebel Text" as filetype,
  type as type,
  concat_ws("|", "Activity", type) as hiertyp,  
  lastupdnmtxt as lastupduserid,
  status as status,
  activitymailrecddt / 1000 as actymailrcptdt,
  coalesce(finalisationcd, 'Not Applicable') as fnlsncdtypnm,
  coalesce(capabilitycd, 'Not Applicable') as taxofccpbltynm,
  actyplancmnt as cseactyplncmnt,
  actydescn as actydescn,
  concat("Activity : ", act_id) as subdoc_title,
  concat_ws(" ", actyplancmnt, actydescn) as subdoc_text
from case_data.activity
where 
  (ingesttm / 1000) > <track-time>


select 
  clnt.caseid as caseid,
  max (
    case when clnt.typ_cd = 1 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        clnt.value_id 
      else 
        ''   
      end 
    end
  ) as clientname,
  max (
    case when clnt.typ_cd = 2 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then
        clnt.value_id 
      else 
        ''   
      end 
    end
  ) as clnttfn,
  max (
    case when clnt.typ_cd = 3 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        clnt.value_id 
      else 
        ''   
      end 
    end
  ) as clntabn,
  max (
    case when clnt.typ_cd = 4 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        clnt.value_id 
      else 
        ''   
      end 
    end
  ) as clnttan,
  max (
    case when clnt.typ_cd = 5 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        clnt.value_id 
      else 
        ''   
      end 
    end
  ) as clntpostcode,
  max (
    case when clnt.typ_cd = 6 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        coalesce(clnt.value_id, 60)
      else 
        60   
      end 
    end
  ) as clientsecrtyclsncd,
  max (
    case when clnt.typ_cd = 6 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
        coalesce(clnt.value_id, 60) < cse.casesecrtyclsncd        
      then 
        coalesce(clnt.value_id, 60)
      else 
        cse.casesecrtyclsncd
      end 
    end
  ) as securityclearance,
  max (
    case when clnt.typ_cd = 7 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        coalesce(clnt.value_id, 'N')
      else 
        'N'   
      end 
    end
  ) as clientspclintstind,
  max (
    case when clnt.typ_cd = 8 then              
      case when 
        clnt.status_cd = 5 and 
        current_timestamp() between 
          cast(from_unixtime(unix_timestamp(clnt.dt_eff, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) and 
          cast(from_unixtime(unix_timestamp(clnt.dt_end, 'yyyyMMdd'), 'yyyy-MM-dd hh:mm:ss') as timestamp) 
      then 
        coalesce(clnt.value_id, 'N')
      else 
        'N'   
      end 
    end
  ) as clienttaxofcstfind
from case_data.client as clnt, case_data.case_details as cse
where
  clnt.caseid = cse.caseid and 
  ((clnt.ingesttm / 1000) > <track-time> or 
  (cse.ingesttm / 1000) > <track-time>)
group by clnt.caseid

select 
  caseid as caseid,
  concat_ws("#", caseid, noteid) as subdocid,
  noteid as childid,
  "Case Note" as notetype,
  "Siebel Text" as filetype,
  type as type,
  lastupdnmtxt as lastupduserid,
  status as status,
  subtyp as subtyp,
  concat_ws("|", "Case Note", type, subtyp) as hiertyp,  
  notetxt as casenotetxt,
  notesummary as casenotesumry,
  concat("Case Note: ", noteid) as subdoc_title,
  concat_ws(" ", notesummary, notetxt) as subdoc_text 
from case_data.case_note
where 
  ((notesummary is not null and notesummary != '') or
  (notetxt is not null and notetxt != '')) and
  (ingesttm / 1000) > <track-time> 

select 
  caseid as caseid,
  concat_ws("#", caseid, riskid) as subdocid,
  riskid as childid,
  "Risk" as notetype,
  "Siebel Text" as filetype,
  type as type,
  type as risktype,
  lastupdnmtxt as lastupduserid,
  status as status,
  subtyp as subtyp,
  subsubtyp as subsubtyp,
  concat_ws("|", "Risk", type, 
            case when subtyp <> "Not Applicable" then subtyp end, 
            case when subsubtyp <> "Not Applicable" then subsubtyp end) as hiertyp,  
  categorynmtxt as category,
  categorynmtxt as riskcategory,
  concat_ws("|", "Risk", categorynmtxt) as hiercategory,  
  risk_comment as caseriskcomment,
  risk_description as caseriskdesc,
  concat("Risk: ", riskid) as subdoc_title,
  concat_ws(" ", risk_description, risk_comment) as subdoc_text  
from case_data.risk
where 
  ((risk_description is not null and risk_description != '') or
  (risk_comment is not null and risk_comment != '')) and
  (ingesttm / 1000) > <track-time>

select 
  caseid as caseid,
  concat_ws("#", caseid, issid) as subdocid,
  issid as childid,
  "Issue" as notetype,
  "Siebel Text" as filetype,
  type as type,
  lastupdnmtxt as lastupduserid,
  status as status,
  subtyp as subtyp,
  subsubtyp as subsubtyp,
  concat_ws("|", "Issue", type, 
            case when subtyp <> "Not Applicable" then subtyp end, 
            case when subsubtyp <> "Not Applicable" then subsubtyp end) as hiertyp,  
  categorynmtxt as category,
  categorynmtxt as issuecategory,
  concat_ws("|", "Issue", categorynmtxt) as hiercategory,  
  iss_comment as caseissuecmnt,
  iss_description as caseissuedesc,
  concat("Issue: ", issid) as subdoc_title,
  concat_ws(" ", iss_description, iss_comment) as subdoc_text  
from case_data.issue
where 
  ((iss_description is not null and iss_description != '') or
  (iss_comment is not null and iss_comment != '')) and
  (ingesttm / 1000) > <track-time>


select 
  caseid as caseid,
  concat_ws("#", caseid, outcmid) as subdocid,
  outcmid as childid,
  "Outcome" as notetype,
  "Siebel Text" as filetype,
  type as type,
  lastupdnmtxt as lastupduserid,
  status as status,
  subtyp as subtyp,
  subtyp as outcmsubtyp,
  concat_ws("|", "Outcome", type, 
            case when subtyp <> "Not Applicable" then subtyp end) as hiertyp,
  categorynmtxt as category,
  categorynmtxt as outcmcategory,
  concat_ws("|", "Outcome", categorynmtxt) as hiercategory,  
  outcm_comment as caseoutcmcomment,
  outcm_description as caseoutcmdesc,
  concat("Outcome: ", outcmid) as subdoc_title,
  concat_ws(" ", outcm_description, outcm_comment) as subdoc_text  
from case_data.outcome
where 
  ((outcm_description is not null and outcm_description != '') or
  (outcm_comment is not null and outcm_comment != '')) and
  (ingesttm / 1000) > <track-time>

select 
  acty.caseid as caseid,
  concat_ws("#", acty.caseid, acty_att.act_id, acty_att.att_id) as subdocid,
  facet.contentinferred as contentinferred,
  facet.legref as legref,
  facet.industry as industry
from case_data.activity_attachment as acty_att, case_data.facet2 as facet, case_data.activity as acty
where 
  acty.act_id = acty_att.act_id and
  acty_att.iodlid = facet.iodlid and
  ((acty_att.ingesttm / 1000) > <track-time> or
  (facet.ingesttm / 1000)  > <track-time> or
  (acty.ingesttm / 1000)  > <track-time>)

select 
  acty.caseid as caseid,
  concat_ws("#", acty.caseid, acty_nt.act_id, acty_nt.note_id) as subdocid,
  acty_nt.act_id as childid,
  acty_nt.act_id as actyid,
  acty_nt.note_id as grandchildid,
  "Activity Note" as notetype,  
  "Siebel Text" as filetype,
  acty.type as type,
  concat_ws("|", "Activity", acty.type) as hiertyp,  
  acty_nt.lastupdnmtxt as lastupduserid,
  acty.status as status,
  acty.activitymailrecddt / 1000 as actymailrcptdt,
  coalesce(acty.finalisationcd, 'Not Applicable') as fnlsncdtypnm,
  coalesce(acty.capabilitycd, 'Not Applicable') as taxofccpbltynm,
  concat("Activity : ",  acty_nt.act_id, " - Activity Note: ", acty_nt.note_id) as subdoc_title,
  notetext as subdoc_text
from case_data.activity_note as acty_nt, case_data.activity as acty
where 
  acty.act_id = acty_nt.act_id and
  ((acty_nt.ingesttm / 1000) > <track-time> or
  (acty.ingesttm / 1000) > <track-time>)

select 
  caseid as caseid,
  concat_ws("#", caseid, dcsnid) as subdocid,
  dcsnid as childid,
  "Decision" as notetype,
  "Siebel Text" as filetype,
  type as type,
  lastupdnmtxt as lastupduserid,
  status as status,
  subtyp as subtyp,
  concat_ws("|", "Decision", type, subtyp) as hiertyp, 
  categorynmtxt as category,
  categorynmtxt as decisioncategory,
  concat_ws("|", "Decision", categorynmtxt) as hiercategory,  
  dcsn_comment as casedcsncmnt,
  dcsn_dtl as casedcsndtl,
  dcsn_ctgry_nm as dcsn_ctgry_nm,
  concat("Decision: ", dcsnid, case when (dcsn_ctgry_nm is not null and dcsn_ctgry_nm != '') then concat(" - ", dcsn_ctgry_nm) else '' end) as subdoc_title,
  concat_ws(" ", dcsn_comment, dcsn_dtl) as subdoc_text  
from case_data.decision
where 
  ((dcsn_dtl is not null and dcsn_dtl != '') or
  (dcsn_comment is not null and dcsn_comment != '') or
  (dcsn_ctgry_nm is not null and dcsn_ctgry_nm != '')) and
  (ingesttm / 1000) > <track-time>

select 
  outcm.caseid as caseid,
  concat_ws("#", outcm.caseid, outcm_det.outcmid, outcm_det.outcmdtlid) as subdocid,
  outcm_det.outcmid as childid,
  outcm_det.outcmdtlid as grandchildid,
  "Outcome Detail" as notetype,
  "Siebel Text" as filetype,
  outcm.type as type,
  outcm.lastupdnmtxt as lastupduserid,
  outcm.status as status,
  outcm.subtyp as subtyp,
  outcm.subtyp as outcmsubtyp,
  outcm_det.subtyp as subsubtyp,
  concat_ws("|", "Outcome", type, 
            case when outcm.subtyp <> "Not Applicable" then outcm.subtyp end, 
            case when outcm_det.subtyp <> "Not Applicable" then outcm_det.subtyp end) as hiertyp,
  outcm.categorynmtxt as category,
  outcm.categorynmtxt as outcmcategory,
  concat_ws("|", "Outcome", outcm.categorynmtxt) as hiercategory,  
  concat("Outcome: ", outcm_det.outcmid, " - Detail Comment: ", outcm_det.outcmdtlid) as subdoc_title,
  outcm_det.outcmdtl_comment as subdoc_text  
from case_data.outcome as outcm, case_data.outcome_detail as outcm_det
where 
  outcm.outcmid = outcm_det.outcmid and
  (outcm_det.outcmdtl_comment is not null and outcm_det.outcmdtl_comment != '') and
  ((outcm.ingesttm / 1000) > <track-time> or
  (outcm_det.ingesttm / 1000) > <track-time>)

select 
  cse_att.caseid as caseid,
  concat_ws("#", cse_att.caseid, cse_att.attid) as subdocid,
  facet.contentinferred as contentinferred,
  facet.legref as legref,
  facet.industry as industry
from case_data.case_attachment as cse_att, case_data.facet2 as facet
where 
  cse_att.iodlid = facet.iodlid and
  ((cse_att.ingesttm / 1000) > <track-time> or
  (facet.ingesttm / 1000) > <track-time>)

