# Where to create the private package repo
# Files in here need to be accessible at PRIVATE_REPO_URL.
PRIVATE_REPO_DIR := /srv/frisk-repo

# Set to amd64 for 64 bit version.
ARCH := amd64
ARCHES := i386 amd64 source

# Set to -m32 for 32 bit version.
GCC_ARCH_OVERRIDE := -m64

SUBDIRS = . test

# Debian version to build on top of.
# NB More work may be needed than simply changing this setting.
CODENAME := jessie

CODENAMES := jessie

CURDIR := $(shell pwd)
GNUPGHOME := $(CURDIR)/../frisk.gnupg
export GNUPGHOME

PERL5LIB = $(CURDIR)
export PERL5LIB

# Perl scripts to install in /usr/bin mode 0755.
PERL_SCRIPTS :=  \
	frisk-process-xml 

# XSLT scripts to install in /etc/frisk/xml-configuration mode 0755.

XSLT_SCRIPTS := \
	./xml-configuration/readme.txt \
	./xml-configuration/copy_sql_from_xml.xsl \
	./xml-configuration/generate_json_for_ui.xsl \
	./xml-configuration/generate_sql_from_xml.xsl \
	./xml-configuration/script_to_generate_index.xsl \
	./xml-configuration/create_dataset_longname.xsl \
	./xml-configuration/create_shown_fields.xsl \
	./xml-configuration/create_filtertypes_map.xsl \
	./xml-configuration/create_attribute_prefix.xsl \
	./xml-configuration/create_dataset_conf_file.xsl \
	./xml-configuration/create_xml_for_preprocess_ui.xsl \
	./xml-configuration/create_xml_for_preprocess_index.xsl \
	./xml-configuration/create_xml_for_preprocess_shown.xsl \
	./xml-configuration/frisk_index_utils.xsl \
	./xml-configuration/frisk_shown_fields_utils.xsl \
	./xml-configuration/frisk_constants.xsl \
	./xml-configuration/make_config_all_dbs.xsl \
	./xml-configuration/create_pre_boolean_slots.xsl  \
	./xml-configuration/create_post_boolean_slots.xsl \
	./xml-configuration/create_constant_prefixes.xsl  \
	./xml-configuration/FISC_CONSTANTS.xml \
	./xml-configuration/frisk_xml_parser_utils.xsl

# Note: awk's $2 needs escaping in this Makefile to $$2
VERSION:=$(shell dpkg-parsechangelog | grep '^Version: ' | awk '{print $$2}')

all: build

build:
	: # Nothing to do for the "upstream" build, just some pre-build tasks.

install:

# Make sure the paths exist.
	install -d '$(DESTDIR)/usr/bin'
	install -d '$(DESTDIR)/etc/frisk/xml-configuration'
	install -d '$(DESTDIR)/etc/frisk/examples'

# Install any feature switches.
	install -m644 -D -T test/input_xml/file_share_demo.xml '$(DESTDIR)/etc/frisk/examples/file_share.xml'
	install -m644 -D -T test/input_xml/smsf_api_custom_searchtype.xml '$(DESTDIR)/etc/frisk/examples/custom_api_searchtype.xml'
	install -m644 -D -T test/input_xml/smsf_subdocs_old.xml '$(DESTDIR)/etc/frisk/examples/smsf_subdocs.xml'
	install -m644 -D -T test/input_xml/case.xml '$(DESTDIR)/etc/frisk/examples/case.xml'

# Perl scripts to install.
# Added check for stretch as the command change was provided.
ifeq ($(CODENAME),stretch)
	for s in $(PERL_SCRIPTS) ; do \
		set -e ;\
		perl -I. -wc "$$s";\
		install -m755 "$$s" '$(DESTDIR)/usr/bin';\
	done
else
	for s in $(PERL_SCRIPTS) ; do \
		set -e ;\
		perl -wc "$$s";\
		install -m755 "$$s" '$(DESTDIR)/usr/bin';\
	done
endif

	for s in $(XSLT_SCRIPTS) ; do \
		set -e ;\
		install -m644 "$$s" '$(DESTDIR)/etc/frisk/xml-configuration/';\
		done

	
	# Perl modules to install.
	perl -wc FriskXMLParserUtils.pm
	install -m644 -D -T FriskXMLParserUtils.pm '$(DESTDIR)/usr/share/perl5/FriskXMLParserUtils.pm'

clean:

.PHONY: test

test:
	cd test && ./test_all_xmls

test-case:
	cd test && ./test_case

debuild_opts ?= -us -uc
debuild:
	# Remove all but the last 10 versions of all files, but all .deb files.
	# ls is considered bad form for this sort of task - use find instead.
	rm -f ../frisk-xml-parser_*.deb
	find ../ -maxdepth 1 -type f -printf '%Ts\t%p\n' | grep "frisk-xml-parser" | sort -n | head -n -60 | cut -f 2- | xargs rm -f --
#	debuild clean
ifeq ($(CODENAME),jessie)
	pdebuild -- --basepath "/var/cache/pbuilder/base.cow.jessie.amd64/"
else
	CFLAGS=$(GCC_ARCH_OVERRIDE) debuild $(debuild_opts) -a$(ARCH)
endif

update-repo: update-repo-frisk-xml-parser update-ec2

update-repo-frisk-xml-parser: debuild
	reprepro -Vb '$(PRIVATE_REPO_DIR)' includedeb $(CODENAME)-proposed ../frisk-xml-parser_*.deb
	# Keep older package versions around for debugging.
	#reprepro -Vb '$(PRIVATE_REPO_DIR)' deleteunreferenced

approve-repo: approve-repo-frisk-xml-parser update-ec2

approve-repo-frisk-xml-parser:
	reprepro -Vb '$(PRIVATE_REPO_DIR)' copysrc $(CODENAME) $(CODENAME)-proposed frisk-xml-parser
	reprepro -Vb '$(PRIVATE_REPO_DIR)' removesrc $(CODENAME)-proposed frisk-xml-parser

update-ec2:
	rsync -a -v /srv/frisk-repo/ ec2:/srv/frisk-repo/

