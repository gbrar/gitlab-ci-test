# FriskXMLParserUtils.pm
#
# Copyright Frisk IP Pty Ltd. 2017
#
# All rights reserved. No part of this publication may be reproduced in
# any material form or transmitted to any other person without the prior
# written permission of Frisk IP Pty Ltd or as otherwise permitted under
# the Copyright Act 1968 (as amended).


=head1 FriskXMLParserUtils.pm

Common functions used by the Frisk XML Parser.

Installed by the Frisk Debian package as /usr/bin/FriskXMLParserUtils.

=cut

package FriskXMLParserUtils;

use strict;
use warnings;

use Exporter;

use Scalar::Util qw( blessed );

our @ISA = qw( Exporter );


# Define the export sets.

my @std_export = qw(
    commify
    get_OS_version
    seconds_to_DHMS
);

my @debug_level = qw(
    DEBUG_OFF
    DEBUG_SCRIPT
    DEBUG_FRISK
    DEBUG_ALL
);

my @okay = qw(
    OKAY
    ERROR
);

my @truth = qw(
    TRUE
    FALSE
);

my @version = qw(
    VERSION_CODENAME
    VERSION_ID
    VERSION_NAME
    VERSION_OS_NAME
    VERSION_OS_PRETTY_NAME
);

my @constants = ( @debug_level, @okay, @truth, @version );

my @all = ( @constants, @std_export );


# EXPORT makes the following items available by default.
our @EXPORT = @std_export;


# EXPORT_OK makes the following items available on request.
our @EXPORT_OK = @constants;


# EXPORT_TAGS allow groups of items to be imported at once.
our %EXPORT_TAGS = (
    all             =>  [ @all ],
    constants       =>  [ @constants ],
    debug_level     =>  [ @debug_level ],
    functions       =>  [ @std_export ],
    okay            =>  [ @okay ],
    truth           =>  [ @truth ],
    version         =>  [ @version ]
);


=head2 Global Constants

Global constants to enable easy identification of state.

=over
=item C<TRUE>

True value constant. (1)

=item C<FALSE>

False value constant. (0)

=item C<OKAY>

Success value constant. (0)

=item C<ERROR>

Error value constant. (1)

=back
=cut

# Define constants.

use constant TRUE   =>  1;
use constant FALSE  =>  0;

use constant OKAY   =>  0;
use constant ERROR  =>  1;


=head2 Debug Constants

Global constants to define the debug level.

B<Note:> Debug is only enabled where there is a debug option available.

=over
=item Constant / Debug Level
C<DEBUG_OFF>    No debug information is output.
C<DEBUG_SCRIPT> Output debug information for this script only.
C<DEBUG_FRISK>  Output debug information for any Frisk script or module.
C<DEBUG_ALL>    Output debug information for all used scripts and modules.

=back
=cut

# Define the debug level.
use constant DEBUG_OFF      =>  0;
use constant DEBUG_SCRIPT   =>  1;
use constant DEBUG_FRISK    =>  2;
use constant DEBUG_ALL      =>  3;


=head2 OS Version Constants

Global constants used to access OS version details.

=over
=item Constant / Version Information
C<VERSION_CODENAME>         Version codename.
C>VERSION_ID>               Version ID.
C<VERSION_NAME>             Version details as a number and codename.
C<VERSION_OS_NAME>          OS name from the full version.
C<VERSION_OS_PRETTY_NAME>   Debian full OS / version name - includes OS and version details.

For example:
    VERSION_CODENAME        stretch
    VERSION_ID              9
    VERSION_NAME            9 (stretch)
    VERSION_OS_NAME         Debian GNU/Linux
    VERSION_OS_PRETTY_NAME  Debian GNU/Linux 9 (stretch)

=back
=cut

# Define the OS version keys.
use constant VERSION_CODENAME       =>  0;
use constant VERSION_ID             =>  1;
use constant VERSION_NAME           =>  2;
use constant VERSION_OS_NAME        =>  3;
use constant VERSION_OS_PRETTY_NAME =>  4;


=head2 Data Reference print function Constants

Global constant used by the print and sprint of data reference functions.

=over
=item C<KEY_SEPARATOR>

Default separator to use between keys of a data object (hash, array). May be a string. (Default is underscore "_")

B<Note:> The separator between keys of an object may be used by downstream commands / external tools. Take stock before modifying this default.

=back
=cut

# Print and sprint data object default separator.
use constant KEY_SEPARATOR => "_";


=head2 Global Variables

Exposed and not-exposed global variables.

=head3 DEBUG status

Holds the current status of debug.

=over
=item C<$DEBUG>

Set default debug state for functions which have debugging. (Default: off or false)

=back
=cut

my $DEBUG = FALSE;


=head2 Local functions

=head3 C<set_debug($state)>

Set debug to on or off. Default is Off.

Usage:
    C<set_debug($state);>

=over
=item C<$state>

Boolean value to indicate whether debug is enabled.
    TRUE  = Enabled.
    FALSE = Disabled.

=back
=cut

sub set_debug {
    my ($state) = @_;

    # Set current debug state.

    $DEBUG = (defined $state) ? $state : FALSE;
    
    print "FriskXMLParserUtils::set_debug(state [on])\n" if $DEBUG;
}


=head2 General Functions

=head3 C<commify($text)>

Commify any  numeric respresentation in the supplied text.

B<NOTE:> This is not a copyrighted function - it has been sourced from external free perl code for reuse.

Usage:
    C<my $text = commify($text);>
or:
    C<my $text = commify(sprintf("Integer %d  Float %.3f", $integer, $float));>

This function parses the supplied text for numeric strings and adds commas based on the Australian locale.

=over
=item C<$text>

String (text) containing numeric strings to be commified.

=item 
returned values

    C<$text> is the original input string (text) with any numberic strings commified.

=back
=cut

sub commify {
    # Commify a numeric string as #,##0.0

    my $text = reverse $_[0];
    $text =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;
    return scalar reverse $text;
}


=head3 C<get_OS_version($part)>

Get an aspect of the OS version, either ID or name.

Usage:
    C<my $version_id = get_OS_version(VERSION_ID);>
    C<my $version_name = get_OS_version(VERSION_NAME);>

The version is sourced from the os-release file on all Debian systems.

=over
=item C<$part>

The part of the version to return. See above OS Version constants. Default is C<VERSION_CODENAME>.

=item returned values

    C<part> is the request part of the OS Version details.
    C<undef> is returned if the file is not readable.

=back

Description:

I tried a couple of other methods to obtain this information, but neither provided what I was after.
Perl libraries:
    C<Config> contains a lot of values regarding the Perl environment but not the actual OS version or name.
    C<Linux::Distribution> will provide version number but not name.

In the end it was eaier to just Slurp the os-release file contents and break out the value of interest.

The C</etc/os-release/> link points to the file containing the Debian Linux version details.
For example, on stretch:
=over
C<PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/">

=back
=cut

sub get_OS_version {
    my ($part) = @_;

    # Get the specified part of the OS Version.

    print sprintf("get_OS_version(part [%s])\n",
            $part // "undef") if $DEBUG;

    # Set default prt if not supplied.
    unless ( (defined $part) && ($part >= VERSION_CODENAME) && ($part <= VERSION_OS_PRETTY_NAME) ) {
        $part = VERSION_CODENAME;
        print sprintf("get_OS_version: part is invalid: using default [%s]\n", $part) if $DEBUG;
    }

    # Get the OS Version details.
    my $versionText = read_file("/etc/os-release");

    print sprintf("OS Release file contents [%s]\n", $versionText) if $DEBUG;

    # Check for an error.
    unless (defined $versionText) {
        print STDERR "get_OS_version: OS version file read failed\n";
        return undef;
    }

    my $one;

    # Return the request part.
    if ($part == VERSION_CODENAME) {
        $versionText =~ /VERSION\=\"[0-9]+\s+\(([^\)]+)/;
        $one = $1
    }
    elsif ($part == VERSION_ID) {
        $versionText =~ /VERSION_ID\=\"([0-9]+)/;
        $one = $1
    }
    elsif ($part == VERSION_NAME) {
        $versionText =~ /VERSION\=\"([^\"]+)/;
        $one = $1
    }
    elsif ($part == VERSION_OS_NAME) {
        $versionText =~ /[^_]NAME\=\"([^\"]+)/;
        $one = $1
    }
    elsif ($part == VERSION_OS_PRETTY_NAME) {
        $versionText =~ /PRETTY_NAME\=\"([^\"]+)/;
        $one = $1
    }
    else {
        # Should never get here.
        return undef;
    }

    print sprintf("Part [%s] Result [%s] Source [%s]\n", $part, $one, $versionText) if $DEBUG;

    return $one;
}

=head3 C<seconds_to_DHMS($seconds)>

Converts the supplied value in seconds into a string as days, hours, minutes and seconds, with any fractional part retained.

Usage:
    C<my $strTime = seconds_to_DHMS($seconds)>
    C<print sprintf("..ran for [%s] seconds (or %s)\n", $seconds, seconds_to_DHMS($seconds) )>

=over
=item C<$seconds>

The number of seconds to be converted to a string in days, hours, minutes and seconds format. May include decimal component which is retained on output.

=item returned values

    C<string> is a string containing the converted seconds in the format: d h:mm:ss.ttt.

=back
Description:

=over
There are many functions to convert date and times from one format to another, but as yet I have not found one to reformat a duration in seconds to a more readale format. When I do, I will replace this function.

Until then, this function uses a simple approach using modulus aritghmatic to reduce a duration in seconds to a string in the form: days hours:minutes:seconds.thousandths

The thousandths is only returned if it is present in the suplied argument.

For example:

C<my $seconds = 94001;
print sprintf("..ran for [%s] seconds (or %s)\n", $seconds, seconds_to_DHMS($seconds) )>
my $fseconds = 94001.753;
print sprintf("..ran for [%s] seconds (or %s)\n", $fseconds, seconds_to_DHMS($fseconds) )>

will yield:
    ..ran for [94001] seconds (or 1 2:06:41)
    ..ran for [94001] seconds (or 1 2:06:41.753)

=back
=cut

sub seconds_to_DHMS {
    my ($seconds) = @_;

    # Convert a durtation in seconds to a string in the form: d h:mm:ss.ttt.

    print sprintf("seconds_to_DHMS(seconds [%s])\n",
            $seconds // "undef") if $DEBUG;

    # Check we have a valid number to convert.
    unless (defined $seconds) {
        print STDERR sprintf("seconds_to_DHMS: Missing mandatory fields (seconds [%s])\n",
                $seconds // "undef");
        return "";
    }

    # Check for the really small value.
    return "0 0:00:00.001" if ( $seconds < 0.001 );

    if ( $seconds =~ /[^\d\.\,]/ ) {
        print STDERR sprintf("seconds_to_DHMS: Parameter seconds [%s] is invalid\n", $seconds);
        return "";
    }

    # Remove any unwanted commas - valid in numbers but not calculations on them.
    $seconds =~ s/\,//g;

    # Save off any decimal part.
    my $int = int($seconds);
    my $decimal = $seconds - $int;

    # Calulate the parts of the string.
    my $secs = $int % 60;
    $int -= $secs;

    $int /= 60;             # $int is now whole minutes.
    my $mins = $int % 60;
    $int -= $mins;

    $int /= 60;             # $int is now whole hours.
    my $hrs = $int % 24;
    $int -= $hrs;

    $int /= 24;             # $int is now whole days.

    return sprintf("%d %d:%02d:%02d%s",
            $int, $hrs, $mins, $secs,
            ($decimal > 0) ? sprintf(".%03d", $decimal*1000) : "" );
}


# End of module - return true value
TRUE;
