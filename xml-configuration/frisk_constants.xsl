<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
   
  <xsl:variable name="split_ext">
    <xsl:value-of select="'**MULTI_SEP**'"/>
  </xsl:variable>
  
  <xsl:variable name="split_int">
    <xsl:value-of select="'==SINGLE_SEP=='"/>
  </xsl:variable>
  
     
  <!-- define prefix for icon name fa- -->
  <xsl:variable name="PREFIX_ICON_NAME" select="'fa-'"/>
  
  <!-- TODO Implement it in a map -->
  <!-- Separate the valid style with ~ including at begining and end-->
  <xsl:variable name="icon_list_styles">
    <xsl:value-of select="concat($split_ext,'solid',$split_ext,'regular',$split_ext,'light',$split_ext,'brands',$split_ext)"/>    
  </xsl:variable>
  
  <!-- Make a map of style and code
  Each key value pair is separated by ~ (including at begining and end)
  Key is separated from value by :
  Map of Style name to style code prefix to be used in config.json    
  The styles map to:
       solid - fas,regular - far,light - fal,brands - fab  -->
  <xsl:variable name="map_style_to_its_prefix">
    <xsl:value-of select="concat($split_ext,'solid',$split_int,'fas',$split_ext,'regular',$split_int,'far',$split_ext,'light',$split_int,'fal',$split_ext,'brands',$split_int,'fab',$split_ext)"/>
  </xsl:variable>
  
  <xsl:template name="get_code_from_style">
    <xsl:param name="style"/>
    <xsl:variable name="key_for_map">
      <xsl:value-of select="concat($split_ext,$style,$split_int)"/>
    </xsl:variable>
    <xsl:variable name="Rest_String_After_key">
      <xsl:value-of select="substring-after($map_style_to_its_prefix,$key_for_map)"/>
    </xsl:variable>
    <xsl:if test="string-length($Rest_String_After_key) &gt; 0">
      <xsl:value-of select="substring-before($Rest_String_After_key,$split_ext)"/>
    </xsl:if>    
  </xsl:template>  
  
</xsl:stylesheet>