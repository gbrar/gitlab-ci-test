<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:ext="http://exslt.org/common">

  <xsl:import href="frisk_xml_parser_utils.xsl"/>
  <xsl:import href="frisk_index_utils.xsl"/>
 
  <!-- Identity template : copy all text nodes, elements and attributes -->
  <xsl:template match="node() | @*" mode="preprocess_index">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="preprocess_index"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="DataItem" mode="preprocess_index">
    <xsl:call-template name="validate_split_attribute"/>
    <xsl:call-template name="validate_alias_typefield"/>

    <xsl:if test="contains(@name,'MULTI_SEP')">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('DataItem ',@name,' contains MULTI_SEP which is not allowed ',$newLine)"/>
      </xsl:message>
    </xsl:if>
    <xsl:copy>

      <xsl:choose>
        <xsl:when test="not(@type) or @type = ''">
          <xsl:attribute name="type">
            <xsl:value-of select="'string'"/>
          </xsl:attribute>
        </xsl:when>
      </xsl:choose>

      <xsl:call-template name="make_all_other_attributes"/>

      <xsl:apply-templates select="@* | node()" mode="preprocess_index"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template match="Icon" mode="preprocess_index">
    <xsl:copy>
      
      <!-- parse="true" is valid only for child of DataItem -->
      <xsl:choose>
        <xsl:when test="@parse='true'">
          <xsl:choose>
            <xsl:when test="name(parent::*)='DataItem'"/>
            <xsl:when test="name(parent::*)='Display'"/>
            <xsl:when test="name(parent::*)='Preview'"/>
            <xsl:otherwise>
              <xsl:message terminate="yes">
                <xsl:value-of select="concat('parse=true for parent ',../@name,../@by,$newLine)"/>
              </xsl:message>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="string-length(@parse) &gt; 0">
              <xsl:if test="count(key('DataItems_By_Name', @parse)) &lt; 1">
                <xsl:message terminate="yes">
                  <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has invalid parse attribute.',$newLine)"/>
                </xsl:message>  
              </xsl:if>  
            </xsl:when>
            <xsl:otherwise>
              <xsl:if test="string-length(@name) &lt; 1 or string-length(@style) &lt; 1">
                <xsl:message terminate="yes">
                  <xsl:value-of select="concat('Both name and style should be present for parent ',../@name,$newLine)"/>
                </xsl:message>  
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose> 
        </xsl:otherwise>
      </xsl:choose>
      
      <xsl:variable name="field_name">
        <xsl:choose>
          <xsl:when test="name(parent::*)='DataItem' or name(parent::*)='Display' or name(parent::*)='Preview'">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
          </xsl:when>
          <xsl:when test="string-length(@parse) &gt; 0">
            <xsl:value-of select="@parse"/>
          </xsl:when>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:attribute name="field_name_attrib">
        <xsl:value-of select="$field_name"/>
      </xsl:attribute>
      
      <xsl:attribute name="make_icon_field_for_indexing">
        <xsl:choose>
          <xsl:when test="string-length($field_name) &gt; 0">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>     
      
      <xsl:apply-templates select="@* | node()" mode="preprocess_index"/>  
    </xsl:copy>
  </xsl:template>

  <xsl:template name="make_all_other_attributes">

    <xsl:call-template name="make_parent_key"/>

    <xsl:call-template name="make_split_string"/>

    <xsl:call-template name="make_dedup"/>

    <xsl:variable name="LineNamePre">
      <xsl:value-of select="@name"/>
    </xsl:variable>

    <xsl:call-template name="make_parsedate"/>

    <xsl:variable name="LineName_var">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="@name"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="make_Line_Name">
      <xsl:with-param name="LineName" select="$LineName_var"/>
    </xsl:call-template>

    <xsl:call-template name="make_linked_to_final_index">
      <xsl:with-param name="LineName" select="$LineName_var"/>
    </xsl:call-template>

    <xsl:call-template name="make_filter">
      <xsl:with-param name="LineName" select="$LineName_var"/>
    </xsl:call-template>

    <xsl:call-template name="make_indexed"/>

    <xsl:call-template name="make_unique_flag"/>

    <xsl:call-template name="make_display"/>
    
    <xsl:call-template name="make_display_fileshare_ref"/>
    
    <xsl:call-template name="make_child_of_link_and_parent_key"/>

  </xsl:template>

  <xsl:template match="DataSet" mode="preprocess_index">

    <xsl:if test="count(descendant::*[name() = 'Linked']) &gt; 1">
      <xsl:message terminate="yes">
        <xsl:text>There can be only max of 1 Linked Node in DataSet</xsl:text>
      </xsl:message>
    </xsl:if>
    
    <xsl:if test="@db='_all'">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('DataSet name cannot be _all ',$newLine)"/>
      </xsl:message>
    </xsl:if>
    
    <xsl:if test="@db='_all_dbs'">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('DataSet name cannot be _all_dbs ',$newLine)"/>
      </xsl:message>
    </xsl:if>

    <!-- Set Linked Node by value in DataSet for convenience -->
    <xsl:variable name="linked_value">
      <xsl:value-of select="descendant::*[name() = 'Linked']/@by"/>
    </xsl:variable>
    <xsl:copy>

      <xsl:choose>
        <xsl:when test="$linked_value != ''">
          <xsl:attribute name="Linked_By_Value">
            <xsl:value-of select="$linked_value"/>
          </xsl:attribute>
        </xsl:when>
      </xsl:choose>

      <!-- Also set if there is a DataItem of same name as By attribute at any level -->
      <xsl:for-each select="descendant::*[name() = 'DataItem']">
        <xsl:if test="$linked_value = @name">
          <xsl:attribute name="DataItem_same_as_linked">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:for-each>

      <xsl:apply-templates select="@* | node()" mode="preprocess_index"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="validate_split_attribute">
    <!-- split cant be present for type="number" -->
    <xsl:choose>
      <xsl:when test="@type = 'number'">
        <xsl:if test="string-length(@split) &gt; 0">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', @name, ' cannot have split attribute if its type is number', $newLine)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:when>
      <xsl:when test="@type = 'date'">
        <xsl:if test="string-length(@split) &gt; 0">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', @name, ' cannot have split attribute if its type is date', $newLine)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>

    <!-- Split and sort are not allowed in same DataItem-->
    <xsl:choose>
      <xsl:when test="string-length(@split) &gt; 0">
        <xsl:if test="Filter/Sort">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', @name, ' cannot have split character and also have Sort action on it', $newLine)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="validate_alias_typefield">
    <xsl:choose>
      <xsl:when test="string-length(@alias) &gt; 0">
        <xsl:if test="string-length(@typefield) &gt; 0">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', @name, ' cannot have alias and also have typefield attribute', $newLine)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  
</xsl:stylesheet>
