<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
  <xsl:include href="frisk_xml_parser_utils.xsl"/>
  
  <xsl:output omit-xml-declaration="yes"/>
  
  <!-- XAAB,eic_audit_status,XAAC,eic_embedded -->
  <xsl:template match="/">
    <xsl:value-of select="concat($FISC_EIC_AUDIT_STATUS,',eic_audit_status',',',$FISC_EMBEDDED_FILE,',eic_embedded}')"/>
  </xsl:template> 
 
  
</xsl:stylesheet>