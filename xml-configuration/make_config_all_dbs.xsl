<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
  <xsl:include href="frisk_xml_parser_utils.xsl"/>

  <xsl:template match="DataSetConfig">
    <xsl:apply-templates select="Security"/>
  </xsl:template>
  
  <xsl:template name="make_security_priority_list">
    <xsl:variable name="Sec_Priority_List">
      <xsl:apply-templates select="./descendant::*[name() = 'Value']" mode="SecurityPriorityList">
        <xsl:sort select="@level" order="ascending" data-type="number"/>
      </xsl:apply-templates>
    </xsl:variable>
    
    <xsl:variable name="Stripped_Priority_List">
      <xsl:choose>
        <xsl:when test="$Sec_Priority_List!=''">
          <xsl:call-template name="strip-end-characters">
            <xsl:with-param name="text" select="$Sec_Priority_List"/>
            <xsl:with-param name="strip-count" select="1"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$Sec_Priority_List"/>
        </xsl:otherwise>
      </xsl:choose>  
    </xsl:variable>
    
    <xsl:value-of select="concat('$set{SecurityPriorityList,$split{',$Stripped_Priority_List,'}}')"/>  
  </xsl:template>
  
  <xsl:template name="make_security_map">
    <xsl:variable name="security_map">
      <xsl:apply-templates select="./descendant::*[name() = 'Value']" mode="SecurityMap">
        <xsl:sort select="@level" order="ascending" data-type="number"/>  
      </xsl:apply-templates>
    </xsl:variable>
    
    <xsl:value-of select="concat('$setmap{SecurityMap',$security_map,'}')"/>  
  </xsl:template>
  
  <xsl:template name="make_notify_map">
    <xsl:variable name="notify_map">
      <xsl:apply-templates select="./descendant::*[name() = 'Value']" mode="NotifyMap">
        <xsl:sort select="@level" order="ascending" data-type="number"/>
      </xsl:apply-templates>
    </xsl:variable>
    <xsl:value-of select="concat('$setmap{NotifyMap',$notify_map,'}')"/>
  </xsl:template>

  <xsl:template match="Security">
    <xsl:call-template name="make_security_priority_list"/>
    <xsl:value-of select="$newLine"/>
    <xsl:call-template name="make_security_map"/>
    <xsl:value-of select="$newLine"/>
    <xsl:call-template name="make_notify_map"/>
    <xsl:value-of select="$newLine"/>
  </xsl:template>

  <xsl:template match="Value" mode="SecurityPriorityList">
    <xsl:value-of select="concat(@value, ' ')"/>
  </xsl:template>
  
  <xsl:template match="Value" mode="SecurityMap">
    <xsl:value-of select="concat(',',@value, ',',@text)"/>
  </xsl:template>
  
  <xsl:template match="Value" mode="NotifyMap">    
    <xsl:variable name="notify_boolean">
      <xsl:choose>
        <xsl:when test="@notify='true'">
          <xsl:value-of select="'true'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'false'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="concat(',',@value,',',$notify_boolean)"/>
  </xsl:template>
  

</xsl:stylesheet>
