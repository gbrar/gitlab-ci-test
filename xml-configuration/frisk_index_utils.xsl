<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="frisk_xml_parser_utils.xsl"/>
  <xsl:import href="frisk_constants.xsl"/>

  <xsl:template match="Filter | Security">
    <xsl:param name="parentKey"/>
    <xsl:param name="ref_name"/>
    <xsl:param name="childOfLink"/>
    <xsl:param name="precountPrefix"/>


    <xsl:variable name="currentNodeName">
      <xsl:value-of select="local-name()"/>
    </xsl:variable>

    <xsl:variable name="SecurityExists">
      <xsl:choose>
        <xsl:when test="$currentNodeName = 'Security'">
          <xsl:value-of select="'true'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'false'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <!--Only make prefix only if we have Filter/Boolean or Filter/SearchWithinResults
        or DataItems/Security-->
    <xsl:variable name="booleanNumber">
      <xsl:choose>
        <xsl:when test="Boolean or SearchWithinResults">
          <xsl:call-template name="calculatePrefixLine">
            <xsl:with-param name="prePrefix" select="$precountPrefix"/>
            <xsl:with-param name="StartLineString" select="'boolean'"/>
            <xsl:with-param name="PrefixLetters" select="$PrefixLetter_Generated"/>
            <xsl:with-param name="FISC_Constant_Boolean" select="child::*[name() = 'Boolean']/@fiscConstantPrefix"/>
            <xsl:with-param name="FISC_Constant_SearchWithinResults" select="child::*[name() = 'SearchWithinResults']/@fiscConstantPrefix"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$SecurityExists='true'">
          <xsl:value-of select="concat('boolean=',$FISC_SECURITY_PREFIX)"/>  
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="spaced_boolean">
      <xsl:value-of select="concat(' ', $booleanNumber, ' ')"/>
    </xsl:variable>

    <xsl:variable name="Index_for_Hierarchy">
      <xsl:choose>
        <xsl:when test="Hierarchical">
          <xsl:call-template name="make_index_line_for_hierarchy">
            <xsl:with-param name="Prefix" select="$precountPrefix"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="date_range_field">
      <xsl:apply-templates select="DateRangeField">
        <xsl:with-param name="parentKey" select="$parentKey"/>
        <xsl:with-param name="ref_name" select="$ref_name"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="sort">
      <xsl:apply-templates select="Sort"/>
    </xsl:variable>

    <xsl:variable name="all_vars_combined">
      <xsl:choose>
        <xsl:when test="contains($date_range_field, 'valuepacked')">
          <xsl:value-of select="concat($date_range_field, $spaced_boolean, $Index_for_Hierarchy)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of
            select="concat($date_range_field, $sort, $spaced_boolean, $Index_for_Hierarchy)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$all_vars_combined"/>

  </xsl:template>

  <xsl:template name="make_linked_to_final_index">
    <xsl:param name="LineName"/>
    <xsl:variable name="Linked_added_to_final_index_var">
      <xsl:choose>
        <xsl:when test="ancestor::Linked[1]/@by = $LineName">
          <xsl:value-of select="concat('link=', ancestor::Linked[1]/@by)"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>


    <xsl:attribute name="Linked_added_to_final_index">
      <xsl:value-of select="$Linked_added_to_final_index_var"/>
    </xsl:attribute>

    <xsl:attribute name="is_linked_index_needed">
      <xsl:choose>
        <xsl:when test="string-length($Linked_added_to_final_index_var) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="make_filter">
    <xsl:param name="LineName"/>

    <xsl:variable name="childOfLink">
      <xsl:choose>
        <xsl:when test="not(ancestor::Link)">
          <xsl:value-of select="'NO'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'YES'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="precedingPrefix">
      <xsl:call-template name="calculatePrefixNumber"/>
    </xsl:variable>

    <xsl:variable name="filter_var">
      <xsl:apply-templates select="Filter | Security">
        <xsl:with-param name="ref_name" select="$LineName"/>
        <xsl:with-param name="childOfLink" select="$childOfLink"/>
        <xsl:with-param name="precountPrefix" select="$precedingPrefix"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:attribute name="filter">
      <xsl:value-of select="$filter_var"/>
    </xsl:attribute>

    <xsl:attribute name="is_filter_needed">
      <xsl:choose>
        <xsl:when test="string-length($filter_var) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="make_indexed">
    <xsl:variable name="indexed_var">
      <xsl:call-template name="makeIndexingLine">
        <xsl:with-param name="parentKey" select="ancestor::Linked[1]/@by"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:attribute name="indexed">
      <xsl:value-of select="$indexed_var"/>
    </xsl:attribute>

    <xsl:attribute name="add_as_indexed">
      <xsl:choose>
        <xsl:when test="string-length($indexed_var) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="make_dedup">
    <xsl:variable name="dedup">
      <xsl:choose>
        <xsl:when test="@removeduplicates = 'true'">
          <xsl:value-of select="',dedup '"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:attribute name="dedup">
      <xsl:value-of select="$dedup"/>
    </xsl:attribute>

    <xsl:attribute name="is_dedup">
      <xsl:choose>
        <xsl:when test="string-length($dedup) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="make_split_string">
    <xsl:variable name="split_string">
      <xsl:if test="@split != ''">
        <xsl:value-of select="@split"/>
      </xsl:if>
    </xsl:variable>

    <xsl:attribute name="split_string">
      <xsl:value-of select="$split_string"/>
    </xsl:attribute>

    <xsl:attribute name="is_split_string">
      <xsl:choose>
        <xsl:when test="string-length($split_string) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="make_parent_key">
    <xsl:variable name="parentKey">
      <xsl:if test="ancestor::Linked[1]/@by != ''">
        <xsl:value-of select="ancestor::Linked[1]/@by"/>
      </xsl:if>
    </xsl:variable>

    <xsl:attribute name="parentKey">
      <xsl:value-of select="$parentKey"/>
    </xsl:attribute>

    <xsl:attribute name="is_parent_key">
      <xsl:choose>
        <xsl:when test="string-length($parentKey) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="make_Line_Name">
    <xsl:param name="LineName"/>


    <xsl:attribute name="LineName">
      <xsl:value-of select="$LineName"/>
    </xsl:attribute>

    <xsl:attribute name="is_Line_Name_present">
      <xsl:choose>
        <xsl:when test="string-length($LineName) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>


  </xsl:template>

  <xsl:template name="make_parsedate">
    <xsl:variable name="parsedate_var">
      <xsl:choose>
        <xsl:when test="@type = 'date'">
          <xsl:value-of select="concat('parsedate=', $double_quotes, @dateFormat, $double_quotes)"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:attribute name="parsedate">
      <xsl:value-of select="$parsedate_var"/>
    </xsl:attribute>

    <xsl:attribute name="is_parsedate_present">
      <xsl:choose>
        <xsl:when test="string-length($parsedate_var) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="make_unique_flag">

    <xsl:variable name="unique">
      <xsl:choose>
        <xsl:when test="@key = 'true'">
          <xsl:value-of select="'unique=Q'"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:attribute name="unique">
      <xsl:value-of select="$unique"/>
    </xsl:attribute>

    <xsl:attribute name="is_unique">
      <xsl:choose>
        <xsl:when test="string-length($unique) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:variable name="add_to_unique">
      <xsl:choose>
        <xsl:when test="@key = 'true'">
          <xsl:value-of select="'boolean=Q'"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:attribute name="add_to_unique">
      <xsl:value-of select="$add_to_unique"/>
    </xsl:attribute>

    <xsl:attribute name="is_add_to_unique_flag">
      <xsl:choose>
        <xsl:when test="string-length($add_to_unique) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>
  
  
  <xsl:template match="DataItem" mode="Alias_Eic">
    <xsl:if test="@does_alias_twice_exist='true'">
      <xsl:value-of select="concat('~',@name,'~')"/>
    </xsl:if>
      
  </xsl:template>

  <xsl:template name="make_display_fileshare_ref">
    <xsl:variable name="fileshare_field_ref">
      <xsl:call-template name="file_ref_mapping">
        <xsl:with-param name="LineNamePre" select="@name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:attribute name="does_alias_twice_exist">
      <xsl:choose>
        <xsl:when test="string-length($fileshare_field_ref) &gt; 0 and string-length(@alias) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>  
    </xsl:attribute>
    
    <xsl:variable name="field_to_use_as_alias">	
      <xsl:choose>
        <xsl:when test="@typefield != ''">
          <xsl:value-of select="$Typefield_String"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@alias"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="childOfLink">
      <xsl:choose>
        <xsl:when test="not(ancestor::Link)">
          <xsl:value-of select="'NO'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'YES'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="parentKey">
      <xsl:if test="ancestor::Linked[1]/@by != ''">
        <xsl:value-of select="ancestor::Linked[1]/@by"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:variable name="alias_ref_field">
      <xsl:choose>
        <xsl:when test="string-length($fileshare_field_ref) &gt; 0 and string-length(@alias) &gt; 0">
          <xsl:choose>
            <xsl:when test="$childOfLink = 'YES'">
              <xsl:choose>
                <xsl:when test="$parentKey != ''">
                  <xsl:value-of select="concat($field_to_use_as_alias, '{', $parentKey, '}')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$field_to_use_as_alias"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$field_to_use_as_alias"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:attribute name="display_fileshare_ref">
      <xsl:if test="string-length($alias_ref_field) &gt; 0">
        <xsl:choose>
          <xsl:when test="$alias_ref_field = 'field'">
            <xsl:value-of select="'#field#'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('field=', $alias_ref_field)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:attribute>
        
    
  </xsl:template>

  <xsl:template name="get_display_alias_ref">
    <xsl:param name="Icon"/>
      
      <xsl:variable name="delimited_display_file_ref">
        <xsl:for-each select="key('DataItems_By_Name', @name)">
          <xsl:if test="@does_alias_twice_exist='true'">
            <xsl:if test="@is_field_needed = 'true' or $Icon='true'">
              <xsl:value-of select="concat(' ', normalize-space(@display_fileshare_ref), ' **MULTI_SEP**')"/>
            </xsl:if>		
          </xsl:if>         
        </xsl:for-each>
      </xsl:variable>
      
      <xsl:variable name="display">
        <xsl:if test="string-length($delimited_display_file_ref) &gt; 0">
          <xsl:value-of select="substring-before($delimited_display_file_ref, '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:variable>
      
      <xsl:value-of select="$display"/>
      
  </xsl:template>
  
  <xsl:template match="Icon" mode="Icon_items">
    <xsl:if test="@make_icon_field_for_indexing='true'">
      <xsl:value-of select="concat('~',@field_name_attrib,'~')"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="make_display">
    <xsl:variable name="add_as_field">
      <xsl:choose>
        <xsl:when
          test="count(descendant::*[name() = 'Display' or name() = 'Preview' or name() = 'ExportToReport']) &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="@logged = 'true'">
              <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="false()"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:attribute name="is_field_needed">
      <xsl:value-of select="$add_as_field"/>
    </xsl:attribute>

    <xsl:variable name="field_alias">
      <xsl:call-template name="calculate_field_for_alias"/>
    </xsl:variable>

    <xsl:attribute name="display">
      <xsl:if test="$add_as_field = 'true'">
        <xsl:choose>
          <xsl:when test="$field_alias = 'field'">
            <xsl:value-of select="'#field#'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('field=', $field_alias)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="Remove_Dup_Massage_String">
    <xsl:param name="Unique_String_1"/>
    <xsl:param name="Unique_String_2"/>
    <xsl:param name="LineName"/>
    <xsl:param name="LineDescPost2"/>
    <xsl:param name="unique"/>

    <xsl:variable name="Unique_Index_FirstLine">
      <xsl:if test="$Unique_String_1 != ''">
        <xsl:choose>
          <xsl:when test="name(parent::*) = 'Link'">
            <xsl:value-of select="concat(' link=', $LineName, ' ', $Unique_String_1, ' ')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$Unique_String_1"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="FixTheString">
      <xsl:choose>
        <xsl:when test="name(parent::*) = 'Linked'">
          <xsl:value-of
            select="concat($LineName, ': ', ' link=', ancestor::Linked[1]/@by, ' ', $LineDescPost2, ' ')"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($LineName, ': ', $LineDescPost2, ' ')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="Dup_Remove_1">
      <xsl:call-template name="distinct-values-from-list">
        <xsl:with-param name="list" select="$Unique_Index_FirstLine"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Dup_Remove_2">
      <xsl:call-template name="distinct-values-from-list">
        <xsl:with-param name="list" select="$Unique_String_2"/>
      </xsl:call-template>
    </xsl:variable>


    <!-- have distinct descriptions and remove duplicates -->
    <xsl:variable name="Dup_Remove">
      <xsl:call-template name="distinct-values-from-list">
        <xsl:with-param name="list" select="$FixTheString"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- Find unique or check if it is key = true
        if yes,split single line into 2 lines
        with has unique line followed by field line 
        $LineName is starting word $unique-->
    <xsl:variable name="Final_Index_Output">
      <xsl:choose>
        <xsl:when test="$unique != ''">
          <xsl:value-of select="concat($LineName, ':', ' ', $Dup_Remove_1)"/>
          <xsl:if test="string-length($Dup_Remove_2) &gt; 0">
            <xsl:value-of select="concat($newLine, $LineName, ':', ' ', $Dup_Remove_2)"/>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$Dup_Remove"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$Final_Index_Output"/>

  </xsl:template>

  <xsl:template match="Sort">
    
    <xsl:variable name="SlotTobeUsed">      
      <xsl:call-template name="get_slot_number_constants"/>
    </xsl:variable>

    <xsl:variable name="Slot_Type">
      <xsl:choose>
        <xsl:when test="ancestor::DataItem[1]/@type = 'number'">
          <xsl:value-of select="'valuenumeric='"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'value='"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:if test="$SlotTobeUsed != ''">
      <xsl:choose>
        <xsl:when test="not(ancestor::Link)">
          <xsl:value-of select="concat(' ', $Slot_Type, $SlotTobeUsed, ' ')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="makeIndexingLine">
    <xsl:param name="parentKey"/>

    <xsl:variable name="truncate_value">
      <xsl:choose>
        <xsl:when test="@truncateField != ''">
          <xsl:value-of select="@truncateField"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="@name = 'fisc_extracted_text'">
              <xsl:value-of select="'500'"/>
            </xsl:when>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <xsl:variable name="truncate_line">
      <xsl:choose>
        <xsl:when test="$truncate_value != ''">
          <xsl:value-of select="concat(' truncate=', $truncate_value, ' ')"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant">
      <xsl:value-of select="@fiscConstantPrefix"/>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant_value">
      <xsl:if test="string-length($fisc_constant) &gt; 0">
        <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$fisc_constant]"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($fisc_constant) &gt; 0">
        <xsl:if test="string-length($fisc_constant_value) &lt; 1">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(@name,' has constant which is not defined',$newLine)"/>
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
    
    <xsl:choose>
      <xsl:when test="@index = 'true'">
        <xsl:choose>
          <xsl:when test="$parentKey != ''">
            <xsl:value-of select="concat('index=',$fisc_constant_value, '{', $parentKey, '}', $truncate_line)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="string-length($fisc_constant_value) &gt; 0">
                <xsl:value-of select="concat('index=',$fisc_constant_value,$truncate_line)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('index ', $truncate_line)"/>    
              </xsl:otherwise>
            </xsl:choose>            
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="@metadata = 'true'">
            <xsl:call-template name="make_index_for_metadata">
              <xsl:with-param name="truncate_line" select="$truncate_line"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$truncate_line"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="make_index_line_for_hierarchy">
    <xsl:param name="Prefix"/>
    
    <xsl:variable name="truncate_value">
      <xsl:choose>
        <xsl:when test="@truncateField != ''">
          <xsl:value-of select="@truncateField"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="@name = 'fisc_extracted_text'">
              <xsl:value-of select="'500'"/>
            </xsl:when>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="truncate_line">
      <xsl:choose>
        <xsl:when test="$truncate_value != ''">
          <xsl:value-of select="concat(' truncate=', $truncate_value, ' ')"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="prefixValue">
      <xsl:call-template name="calculatePrefixLine">
        <xsl:with-param name="prePrefix" select="$Prefix"/>
        <xsl:with-param name="StartLineString" select="''"/>
        <xsl:with-param name="PrefixLetters" select="$PrefixLetter_Generated"/>
        <xsl:with-param name="FISC_Constant_Hierarchical" select="child::*[name() = 'Hierarchical']/@fiscConstantPrefix"/>        
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="separator">
      <xsl:choose>
        <xsl:when test="descendant::Hierarchical[1]/@separator != ''">
          <xsl:value-of select="descendant::Hierarchical[1]/@separator"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="':'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="Initial_index_line">
      <xsl:value-of
        select="concat('path=', $prefixValue, ',', $separator, ',', descendant::Hierarchical[1]/@leading, ',', descendant::Hierarchical[1]/@trailing)"
      />
    </xsl:variable>
    
    <!-- strip ,, if both leading and trailing are null -->
    <xsl:variable name="Final_Index_line">
      <xsl:choose>
        <xsl:when
          test="substring($Initial_index_line, string-length($Initial_index_line) - 1) = ',,'">
          <xsl:value-of
            select="substring($Initial_index_line, 1, string-length($Initial_index_line) - 2)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$Initial_index_line"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="concat($Final_Index_line,$truncate_line)"/>
  </xsl:template>

  <xsl:template name="calculate_field_for_alias">
    <xsl:choose>
      <xsl:when
        test="count(descendant::*[name() = 'Display']) &gt; 0 or count(descendant::*[name() = 'Preview']) &gt; 0 or count(descendant::*[name() = 'ExportToReport']) &gt; 0">
        <xsl:call-template name="make_field_alias_for_indexing"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="@logged = 'true'">
          <xsl:call-template name="make_field_alias_for_indexing"/>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="make_field_alias_for_indexing">

    <xsl:variable name="fileshare_field_ref">
      <xsl:call-template name="file_ref_mapping">
        <xsl:with-param name="LineNamePre" select="@name"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="LineName">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="@name"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="field_to_use_as_alias">
      <xsl:choose>
        <xsl:when test="$fileshare_field_ref != ''">
          <xsl:value-of select="$fileshare_field_ref"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="@typefield != ''">
              <xsl:value-of select="$Typefield_String"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="string-length(@alias) &gt; 0">
                  <xsl:value-of select="@alias"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="$LineName != @name">
                      <xsl:value-of select="$LineName"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'field'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="childOfLink">
      <xsl:choose>
        <xsl:when test="not(ancestor::Link)">
          <xsl:value-of select="'NO'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'YES'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="parentKey">
      <xsl:if test="ancestor::Linked[1]/@by != ''">
        <xsl:value-of select="ancestor::Linked[1]/@by"/>
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="field_alias_link">
      <xsl:choose>
        <xsl:when test="$childOfLink = 'YES'">
          <xsl:choose>
            <xsl:when test="$field_to_use_as_alias = 'field'">
              <xsl:value-of select="$LineName"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$field_to_use_as_alias"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="field_alias">
      <xsl:choose>
        <xsl:when test="$childOfLink = 'YES'">
          <xsl:choose>
            <xsl:when test="$parentKey != ''">
              <xsl:value-of select="concat($field_alias_link, '{', $parentKey, '}')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$field_to_use_as_alias"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$field_to_use_as_alias"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$field_alias"/>

  </xsl:template>
  
  <xsl:template name="make_field_alias_for_icon">
    <xsl:param name="dataitem_name"/>
    <xsl:param name="typefield"/>
    <xsl:param name="alias"/>
    <xsl:param name="childOfLink"/>
    <xsl:param name="parentKey"/>
    
    <xsl:variable name="fileshare_field_ref">
      <xsl:call-template name="file_ref_mapping">
        <xsl:with-param name="LineNamePre" select="$dataitem_name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="LineName">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="$dataitem_name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="field_to_use_as_alias">
      <xsl:choose>
        <xsl:when test="$fileshare_field_ref != ''">
          <xsl:value-of select="$fileshare_field_ref"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$typefield != ''">
              <xsl:value-of select="$Typefield_String"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="string-length($alias) &gt; 0">
                  <xsl:value-of select="$alias"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="$LineName != $dataitem_name">
                      <xsl:value-of select="$LineName"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="'#field#'"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="field_alias_link">
      <xsl:choose>
        <xsl:when test="$childOfLink = 'YES'">
          <xsl:choose>
            <xsl:when test="$field_to_use_as_alias = '#field#'">
              <xsl:value-of select="$LineName"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$field_to_use_as_alias"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="field_alias">
      <xsl:choose>
        <xsl:when test="$childOfLink = 'YES'">
          <xsl:choose>
            <xsl:when test="$parentKey != ''">
              <xsl:value-of select="concat($field_alias_link, '{', $parentKey, '}')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$field_to_use_as_alias"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$field_to_use_as_alias"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($field_alias) &gt; 0">
        <xsl:choose>
          <xsl:when test="$field_alias='#field#'">
            <xsl:value-of select="$field_alias"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="contains($field_alias,'field=')">
                <xsl:value-of select="$field_alias"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat('field=',$field_alias)"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="make_index_for_metadata">
    <xsl:param name="truncate_line"/>

    <xsl:variable name="MetadataInit">
      <xsl:number format="1" count="DataItem[@metadata = 'true']" level="any" from="ConnectionType"
      />
    </xsl:variable>

    <xsl:variable name="MetadataNum">
      <xsl:choose>
        <xsl:when test="number($MetadataInit)">
          <xsl:value-of select="$MetadataInit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="IncNumber">
      <xsl:value-of select="$NumberNode + $MetadataNum - 1"/>
    </xsl:variable>

    <xsl:variable name="NodeLetters">
      <xsl:number value="$IncNumber" format="A"/>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant">
      <xsl:value-of select="@fiscConstantPrefix"/>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant_value">
      <xsl:if test="string-length($fisc_constant) &gt; 0">
        <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$fisc_constant]"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($fisc_constant) &gt; 0">
        <xsl:if test="string-length($fisc_constant_value) &lt; 1">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(@name,' has constant which is not defined',$newLine)"/>
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
    
    <xsl:choose>
      <xsl:when test="string-length($fisc_constant_value) &gt; 0">
        <xsl:value-of select="concat(' index=',$fisc_constant_value,' ',$truncate_line)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat(' index=', 'XM', $NodeLetters, ' ', $truncate_line)"/>    
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="get_filter">

    <xsl:variable name="delimited_filter">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_filter_needed = 'true'">
          <xsl:value-of select="concat(' ', normalize-space(@filter), ' **MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="filter">
      <xsl:if test="string-length($delimited_filter) &gt; 0">
        <xsl:value-of select="substring-before($delimited_filter, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$filter"/>

  </xsl:template>

  <xsl:template name="get_linked_index">
    <xsl:variable name="delimited_linked_index">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_linked_index_needed = 'true'">
          <xsl:value-of
            select="concat(normalize-space(@Linked_added_to_final_index), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="linked_index">
      <xsl:if test="string-length($delimited_linked_index) &gt; 0">
        <xsl:value-of select="substring-before($delimited_linked_index, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$linked_index"/>

  </xsl:template>

  <xsl:template name="get_parse_date">

    <xsl:variable name="delimited_parse_date">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_parsedate_present = 'true'">
          <xsl:value-of select="concat(normalize-space(@parsedate), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="parse_date">
      <xsl:if test="string-length($delimited_parse_date) &gt; 0">
        <xsl:value-of select="substring-before($delimited_parse_date, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$parse_date"/>

  </xsl:template>

  <xsl:template name="get_indexed">

    <xsl:variable name="delimited_indexed">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@add_as_indexed = 'true'">
          <xsl:value-of select="concat(normalize-space(@indexed), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="indexed">
      <xsl:if test="string-length($delimited_indexed) &gt; 0">
        <xsl:value-of select="substring-before($delimited_indexed, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$indexed"/>
  </xsl:template>

  <xsl:template name="get_add_to_unique">
    <xsl:variable name="delimited_add_to_unique">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_add_to_unique_flag = 'true'">
          <xsl:value-of select="concat(normalize-space(@add_to_unique), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="add_to_unique">
      <xsl:if test="string-length($delimited_add_to_unique) &gt; 0">
        <xsl:value-of select="substring-before($delimited_add_to_unique, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$add_to_unique"/>
  </xsl:template>

  <xsl:template name="get_line_name">
    <xsl:variable name="delimited_line_name">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_Line_Name_present = 'true'">
          <xsl:value-of select="concat(normalize-space(@LineName), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="LineName_var">
      <xsl:if test="string-length($delimited_line_name) &gt; 0">
        <xsl:value-of select="substring-before($delimited_line_name, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$LineName_var"/>
  </xsl:template>

  <xsl:template name="get_unique">
    <xsl:variable name="delimited_unique">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_unique = 'true'">
          <xsl:value-of select="concat(normalize-space(@unique), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="unique">
      <xsl:if test="string-length($delimited_unique) &gt; 0">
        <xsl:value-of select="substring-before($delimited_unique, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$unique"/>
  </xsl:template>

  <xsl:template name="get_dedup">
    <xsl:variable name="delimited_dedup">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_dedup = 'true'">
          <xsl:value-of select="concat(normalize-space(@dedup), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="dedup">
      <xsl:if test="string-length($delimited_dedup) &gt; 0">
        <xsl:value-of select="substring-before($delimited_dedup, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$dedup"/>
  </xsl:template>

  <xsl:template name="get_split_string">
    <xsl:variable name="delimited_split_string">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_split_string = 'true'">
          <xsl:value-of select="concat(@split_string, '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="split_string">
      <xsl:if test="string-length($delimited_split_string) &gt; 0">
        <xsl:value-of select="substring-before($delimited_split_string, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:variable name="curated_split_string">
      <xsl:choose>
        <xsl:when test="$split_string='&quot;'">
          <xsl:value-of select="'\x22'"/>
        </xsl:when>
        <xsl:when test="$split_string='\n'">
          <xsl:value-of select="'\x0A'"/>
        </xsl:when>
        <xsl:when test="$split_string='\t'">
          <xsl:value-of select="'\x09'"/>
        </xsl:when>
        <xsl:when test="$split_string='\'">
          <xsl:value-of select="concat('\',$split_string)"/>
        </xsl:when>
        <xsl:when test="$split_string='\r'">
          <xsl:value-of select="'\x0D'"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:value-of select="$split_string"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:if test="string-length($curated_split_string) &gt; 0">
      <xsl:value-of select="concat(' split=&quot;', $curated_split_string,'&quot; ')"/>
    </xsl:if>

  </xsl:template>

  <xsl:template name="get_display">
    <xsl:variable name="delimited_display">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@is_field_needed = 'true'">
          <xsl:value-of select="concat(' ', normalize-space(@display), ' **MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="display">
      <xsl:if test="string-length($delimited_display) &gt; 0">
        <xsl:value-of select="substring-before($delimited_display, '**MULTI_SEP**')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$display"/>

  </xsl:template>

  <!-- Template to process dateRangeField in DataItem Tag -->
  <xsl:template match="DateRangeField">
    <xsl:param name="parentKey"/>
    <xsl:param name="ref_name"/>
    
    <xsl:variable name="SlotTobeUsed">      
      <xsl:call-template name="get_slot_number_constants"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="not(ancestor::Link)">
        <xsl:value-of select="concat(' valuepacked=', $SlotTobeUsed)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!-- Template to process caption in Display Tag -->
  <xsl:template match="Caption">

    <!-- temporary code,will change -->
    <xsl:variable name="Grandparent_Caption">
      <xsl:choose>
        <xsl:when test="ancestor::Linked[1]/@by != ''">
          <xsl:value-of select="ancestor::Linked[1]/@by"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="parent_Caption">
      <xsl:choose>
        <xsl:when test="ancestor::DataItem[1]/@fisc_mapped != ''">
          <xsl:value-of select="ancestor::DataItem[1]/@fisc_mapped"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$Grandparent_Caption != ''">
        <xsl:value-of select="concat(' field=', $parent_Caption, '{', $Grandparent_Caption, '}')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'#field#'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="URL">
    <xsl:param name="parent"/>

    <xsl:choose>
      <xsl:when test="$parent != ''">
        <xsl:value-of select="concat(' field=eic_url', '{', $parent, '}')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="' field=eic_url '"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
      
  <xsl:template name="validate_index_output">
    <xsl:param name="output"/>
    
    <xsl:variable name="Num_Fields">
      <xsl:call-template name="GetNoOfOccurance">
        <xsl:with-param name="String" select="$output"/>
        <xsl:with-param name="SubString" select="' field '"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$Num_Fields &gt; 1">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('Error in output line as no of fields more than 1',$newLine)"/>
      </xsl:message>
    </xsl:if>
    
    <xsl:variable name="Num_Fields_Alias">
      <xsl:call-template name="GetNoOfOccurance">
        <xsl:with-param name="String" select="$output"/>
        <xsl:with-param name="SubString" select="'field='"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$Num_Fields_Alias &gt; 1">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('Error in output line as no of fields alias more than 1',$newLine)"/>
      </xsl:message>
    </xsl:if>
    
    <xsl:variable name="Num_Link">
      <xsl:call-template name="GetNoOfOccurance">
        <xsl:with-param name="String" select="$output"/>
        <xsl:with-param name="SubString" select="'link='"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$Num_Link &gt; 1">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('Error in output line as no of link more than 1',$newLine)"/>
      </xsl:message>
    </xsl:if>
   
    <xsl:variable name="Num_Separator">
      <xsl:call-template name="GetNoOfOccurance">
        <xsl:with-param name="String" select="$output"/>
        <xsl:with-param name="SubString" select="'*MULTI_SEP*'"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="$Num_Separator &gt; 0">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('Error in output line as field separator present',$newLine)"/>
      </xsl:message>
    </xsl:if>
    
  </xsl:template>

  <xsl:template name="make_child_of_link_and_parent_key">    
    <xsl:attribute name="childOfLink">
      <xsl:choose>
        <xsl:when test="not(ancestor::Link)">
          <xsl:value-of select="'NO'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'YES'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    
    <xsl:attribute name="parentKey">
      <xsl:if test="ancestor::Linked[1]/@by != ''">
        <xsl:value-of select="ancestor::Linked[1]/@by"/>
      </xsl:if>
    </xsl:attribute>  
  </xsl:template>

</xsl:stylesheet>
