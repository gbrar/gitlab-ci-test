<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:ext="http://exslt.org/common">
  <xsl:import href="frisk_xml_parser_utils.xsl"/>
  <xsl:import href="frisk_constants.xsl"/>
  
  <!-- Identity template : copy all text nodes, elements and attributes -->
  <xsl:template match="node() | @*" mode="preprocess_ui">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="DataSet" mode="preprocess_ui">
    <xsl:copy>
      <xsl:if test="count(child::Icon) != 1">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat('DataSet ',@name,' should have 1 mandatory Icon Tag',$newLine)"/>  
        </xsl:message>
      </xsl:if>      
      <xsl:apply-templates select="node() | @*" mode="preprocess_ui"/>
    </xsl:copy>  
  </xsl:template>
  
  <xsl:template match="DataItem" mode="preprocess_ui">
    <!-- Mark if it is First Node of this name or not -->
    <xsl:copy>
      <!-- Set Filter position (if Filter present) at DataItem level (except display="false")-->
      <xsl:choose>
        <xsl:when test="Filter/Boolean[not(@display) or @display='' or @display='true']">
          <xsl:if test="Filter/Boolean[not(number(@position))]">
            <xsl:message terminate="yes">
              <xsl:value-of
                select="concat('DataItem ', @name, ' has Boolean Node without numeric position', $newLine)"
              />
            </xsl:message>
          </xsl:if>          
          <xsl:attribute name="filterPosition">
            <xsl:value-of select="Filter/Boolean/@position"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:when test="Filter/Hierarchical[not(@display) or @display='' or @display='true']">
          <xsl:if test="Filter/Hierarchical[not(number(@position))]">
            <xsl:message terminate="yes">
              <xsl:value-of
                select="concat('DataItem ', @name, ' has Hierarchical Node without numeric position', $newLine)"
              />
            </xsl:message>
          </xsl:if>
          <xsl:attribute name="filterPosition">
            <xsl:value-of select="Filter/Hierarchical/@position"/>
          </xsl:attribute>
        </xsl:when>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not(@name = preceding::DataItem/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not(@name = following::DataItem/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not(ancestor::Link)">
          <xsl:attribute name="IsItMain">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsItMain">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:variable name="eic_converted">
        <xsl:call-template name="fisc_to_eic_mapping">
          <xsl:with-param name="InputField" select="@name"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="fisc_mapped_name">
        <xsl:call-template name="fisc_to_metadata_mapping">
          <xsl:with-param name="LineNamePre" select="$eic_converted"/>
        </xsl:call-template>
      </xsl:variable>
      
      <xsl:variable name="final_name_to_use_in_ui">
        <xsl:choose>
          <xsl:when test="string-length(@typefield) &gt; 0">
            <xsl:value-of select="'subdoc_typefield'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$fisc_mapped_name"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:attribute name="final_ui_label">
        <xsl:choose>
          <xsl:when test="string-length(@alias) &gt; 0">
            <xsl:value-of select="@alias"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$final_name_to_use_in_ui"/>    
          </xsl:otherwise>
        </xsl:choose>        
      </xsl:attribute>
      
      <xsl:attribute name="num_labels">
        <xsl:value-of select="count(child::*[name() = 'Filter']/Hierarchical/Labels/Label)"/>  
      </xsl:attribute>
      
      <xsl:choose>
        <xsl:when test="not(@type) or @type = ''">
          <xsl:attribute name="type">
            <xsl:value-of select="'string'"/>
          </xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="not(@type) or @type = ''">
          <xsl:attribute name="type_for_ui_json">
            <xsl:value-of select="'string'"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:when test="@type = 'date'">
          <xsl:attribute name="type_for_ui_json">
            <xsl:value-of select="'string'"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:when test="@type = 'string'">
          <xsl:attribute name="type_for_ui_json">
            <xsl:value-of select="'string'"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:when test="@type = 'number'">
          <xsl:attribute name="type_for_ui_json">
            <xsl:value-of select="'number'"/>
          </xsl:attribute>
        </xsl:when>

      </xsl:choose>      
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>

  </xsl:template>
  
  <xsl:template match="ExportToReport" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>
    
    <xsl:if test="not(number(@position))">
      <xsl:message terminate="yes">
        <xsl:value-of
          select="concat('DataItem ', $Current_dataItem, ' has ExportToReport Node without numeric position', $newLine)"
        />
      </xsl:message>
    </xsl:if>
    <xsl:copy>
      
      <xsl:call-template name="buildExportToReport">
        <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
      </xsl:call-template>

      <xsl:apply-templates select="node() | @*" mode="preprocess_ui"/>

    </xsl:copy>
  </xsl:template>

  <xsl:template match="Link" mode="preprocess_ui">
    <xsl:copy>
      <xsl:variable name="prefix_val">
        <xsl:value-of select="./descendant::DataItem/@typefield[1]"/>
      </xsl:variable>

      <xsl:for-each
        select="./descendant::*[name() = 'DataItem'][@name = following-sibling::DataItem/@name]">
        <xsl:message terminate="yes">
          <xsl:value-of
            select="concat('DataItem-typefield ', @name, '-', $prefix_val, ' has duplicate name', $newLine)"
          />
        </xsl:message>
      </xsl:for-each>
      <xsl:variable name="count_Icon" select="count(./descendant::DataItem/Icon)"/>
      <xsl:if test="$count_Icon &gt; 1">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat($prefix_val,' link has more than 1 DataItem/Icon.',$newLine)"/>  
        </xsl:message>        
      </xsl:if>      
      <xsl:apply-templates select="node() | @*" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Metadata" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>


    <xsl:if test="not(number(@position))">
      <xsl:message terminate="yes">
        <xsl:value-of
          select="concat('DataItem ', $Current_dataItem, ' has Metadata Node without numeric position', $newLine)"
        />
      </xsl:message>
    </xsl:if>

    <xsl:copy>

      <xsl:choose>
        <xsl:when test="name(parent::*) = 'Display'">
          <xsl:call-template name="buildDisplayMetadata">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="name(parent::*) = 'Preview'">
          <xsl:call-template name="buildPreviewMetadata">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Details" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>

    <xsl:if test="not(number(@position))">
      <xsl:message terminate="yes">
        <xsl:value-of
          select="concat('DataItem ', $Current_dataItem, ' has Details Node without numeric position', $newLine)"
        />
      </xsl:message>
    </xsl:if>

    <xsl:copy>

      <xsl:choose>
        <xsl:when test="name(parent::*) = 'Display'">
          <xsl:call-template name="buildDisplayDetails">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="name(parent::*) = 'Preview'">
          <xsl:call-template name="buildPreviewDetails">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>

      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Sort" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="@display='false'"/>
      <xsl:otherwise>
        <xsl:if test="not(number(@position))">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', $Current_dataItem, ' has Sort Node without numeric position', $newLine)"
            />
          </xsl:message>
        </xsl:if>    
      </xsl:otherwise>
    </xsl:choose>


    <xsl:copy>
      
      <xsl:variable name="combinedSlot">      
        <xsl:call-template name="get_slot_number_constants"/>
      </xsl:variable>
      
      <xsl:attribute name="slot">
        <xsl:value-of select="$combinedSlot"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = preceding::DataItem[Filter/Sort]/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = following::DataItem[Filter/Sort]/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
          <xsl:call-template name="buildSortLastItem">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template match="DateRangeField" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="@display='false'"/>
      <xsl:otherwise>
        <xsl:if test="not(number(@position))">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', $Current_dataItem, ' has DateRangeField Node without numeric position', $newLine)"
            />
          </xsl:message>
        </xsl:if>    
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:copy>
      
      <xsl:variable name="combinedSlot">      
        <xsl:call-template name="get_slot_number_constants"/>
      </xsl:variable>

      <xsl:attribute name="slot">
        <xsl:value-of select="$combinedSlot"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = preceding::DataItem[Filter/DateRangeField]/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = following::DataItem[Filter/DateRangeField]/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
          <xsl:call-template name="buildDateRangeLastItem">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template match="SearchWithinResults" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="@display='false'"/>
      <xsl:otherwise>
        <xsl:if test="not(number(@position))">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', $Current_dataItem, ' has SearchWithinResults Node without numeric position', $newLine)"
            />
          </xsl:message>
        </xsl:if>    
      </xsl:otherwise>
    </xsl:choose>

    
    <xsl:copy>
      
      <xsl:variable name="PrefixNumber">
        <xsl:call-template name="calculatePrefixNumber"/>
      </xsl:variable>

      <xsl:variable name="PrefixBoolean">
        <xsl:call-template name="calculateFullPrefixNumber">
          <xsl:with-param name="preCount" select="$PrefixNumber"/>
          <xsl:with-param name="FISC_Constant_SearchWithinResults" select="@fiscConstantPrefix"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:attribute name="termPrefix">
        <xsl:value-of select="$PrefixBoolean"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when
          test="not($Current_dataItem = preceding::DataItem[Filter/SearchWithinResults]/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when
          test="not($Current_dataItem = following::DataItem[Filter/SearchWithinResults]/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
          <xsl:call-template name="buildSearchWithinLastItem">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Boolean | Hierarchical" mode="preprocess_ui">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>
        
    <xsl:choose>
      <xsl:when test="@display='false'"/>
      <xsl:otherwise>
        <xsl:if test="not(number(@position))">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', $Current_dataItem, ' has Boolean or Hierarchical Node without numeric position', $newLine)"
            />
          </xsl:message>
        </xsl:if>    
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:copy>
      <xsl:variable name="PrefixNumber">
        <xsl:call-template name="calculatePrefixNumber"/>
      </xsl:variable>
      <xsl:variable name="Boolean_constant">        
        <xsl:if test="local-name()='Boolean'">
          <xsl:value-of select="@fiscConstantPrefix"/>
        </xsl:if>        
      </xsl:variable>
      <xsl:variable name="hierarchical_constant">        
        <xsl:if test="local-name()='Hierarchical'">
          <xsl:value-of select="@fiscConstantPrefix"/>
        </xsl:if>        
      </xsl:variable>

      <xsl:variable name="PrefixBoolean">
        <xsl:call-template name="calculateFullPrefixNumber">
          <xsl:with-param name="preCount" select="$PrefixNumber"/>
          <xsl:with-param name="FISC_Constant_Boolean" select="$Boolean_constant"/>
          <xsl:with-param name="FISC_Constant_Hierarchical" select="$hierarchical_constant"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:attribute name="termPrefix">
        <xsl:value-of select="$PrefixBoolean"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when
          test="not($Current_dataItem = preceding::DataItem[Filter/Boolean or Filter/Hierarchical]/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when
          test="not($Current_dataItem = following::DataItem[Filter/Boolean or Filter/Hierarchical]/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
          <xsl:call-template name="buildBooleanLastItem">
            <xsl:with-param name="Boolean_or_Hierarchical" select="local-name()"/>
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="Linked" mode="preprocess_ui">
    <xsl:if test="count(descendant::Link) > 0">
      <xsl:if test="count(ancestor::DataItem[@key='true']/Filter/SearchWithinResults[@searchWithin='true'][not(@display) or @display='' or @display='true']) != 1 ">
        <xsl:message terminate="yes">
          <xsl:value-of select="'There should be exactly 1 parent DataItem of a Linked element which has a SearchWithinResults filter with the searchWithin=&quot;true&quot; attribute and display is true'"/>
        </xsl:message>  
      </xsl:if>            
    </xsl:if>
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="URL" mode="preprocess_ui">
    <xsl:copy>
      <xsl:attribute name="Type_label">
        <xsl:choose>
          <xsl:when test="ancestor::DataItem[1]/@fileSource='iodl'">
            <xsl:value-of select="'ImageViewer'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'URL'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:attribute name="name_of_dataitem">
        <xsl:value-of select="ancestor::DataItem[1]/@name"/>
      </xsl:attribute>      
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="Label" mode="All_Label">
      <xsl:value-of select="concat($double_quotes,@title,$double_quotes,',')"/>
  </xsl:template>
  
  <xsl:template match="Label" mode="preprocess_ui">
    <xsl:copy>      
      <xsl:choose>
        <xsl:when test="not(@title) or @title=''">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(ancestor::DataItem[1]/@name,' is missing mandatory title on Label Node.',$newLine)"/>
          </xsl:message>
        </xsl:when>
      </xsl:choose>  
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="Transform" mode="preprocess_ui">
    <xsl:copy>
      <xsl:attribute name="parent_dataitem">
        <xsl:value-of select="ancestor::DataItem[1]/@name"/>
      </xsl:attribute>      
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="Action" mode="OnBlur">
    <xsl:value-of select="concat('~',@type,'~')"/>
  </xsl:template>
  
  <xsl:template match="Action" mode="OnChange">
    <xsl:value-of select="concat('~',@type,'~')"/>
  </xsl:template>
  
  <xsl:template match="OnBlur" mode="preprocess_ui">
    <xsl:copy>
      <xsl:attribute name="parent_dataitem">
        <xsl:value-of select="ancestor::DataItem[1]/@name"/>
      </xsl:attribute>
      <xsl:attribute name="type_list">
        <xsl:apply-templates select="./descendant::*[name() = 'Action']" mode="OnBlur"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="OnChange" mode="preprocess_ui">
    <xsl:copy>
      <xsl:attribute name="parent_dataitem">
        <xsl:value-of select="ancestor::DataItem[1]/@name"/>
      </xsl:attribute>
      <xsl:attribute name="type_list">
        <xsl:apply-templates select="./descendant::*[name() = 'Action']" mode="OnChange"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="Match" mode="preprocess_ui">
    <xsl:copy>
      <xsl:attribute name="parent_dataitem">
        <xsl:value-of select="ancestor::DataItem[1]/@name"/>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="Labels" mode="preprocess_ui">
    <xsl:copy>
      <xsl:variable name="All_labels">
        <xsl:apply-templates select="./descendant::*[name() = 'Label']" mode="All_Label"/>       
      </xsl:variable>
      
      <xsl:attribute name="Stripped_All_labels">
        <xsl:choose>
          <xsl:when test="string-length($All_labels) &gt; 0">
            <xsl:call-template name="strip-end-characters">
              <xsl:with-param name="text" select="$All_labels"/>
              <xsl:with-param name="strip-count" select="1"/>
            </xsl:call-template>    
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$All_labels"/>
          </xsl:otherwise>
        </xsl:choose>        
      </xsl:attribute>
      
      <xsl:attribute name="DataItem_name">
        <xsl:choose>
          <xsl:when test="string-length(ancestor::DataItem[1]/@name) &gt; 0">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>  
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template name="validate_Icon_MainLevel">
    <xsl:if test="string-length(@name) &lt; 1">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has Icon without name attribute.',$newLine)"/>
      </xsl:message>
    </xsl:if>
    <xsl:if test="string-length(@style) &lt; 1">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has Icon without style attribute.',$newLine)"/>
      </xsl:message>
    </xsl:if>
    
    <xsl:choose>
      <xsl:when test="string-length(@parse) &gt; 0">        
        <xsl:if test="count(key('DataItems_By_Name', @parse)) &lt; 1">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has invalid parse attribute.',$newLine)"/>
          </xsl:message>  
        </xsl:if>  
      </xsl:when>
    </xsl:choose>
                
  </xsl:template>
  
  <xsl:template match="Icon" mode="preprocess_ui">
    <xsl:copy>
      
      <!-- parse="true" is valid only for child of DataItem -->
      <xsl:choose>
        <xsl:when test="@parse='true'">
          <xsl:choose>
            <xsl:when test="name(parent::*)='DataItem'"/>
            <xsl:when test="name(parent::*)='Display'"/>
            <xsl:when test="name(parent::*)='Preview'"/>
            <xsl:otherwise>
              <xsl:message terminate="yes">
                <xsl:value-of select="concat('parse=true for parent ',../@name,$newLine)"/>
              </xsl:message>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="string-length(@parse) &gt; 0">
              <xsl:if test="count(key('DataItems_By_Name', @parse)) &lt; 1">
                <xsl:message terminate="yes">
                  <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has invalid parse attribute.',$newLine)"/>
                </xsl:message>  
              </xsl:if>  
            </xsl:when>
            <xsl:otherwise>
              <xsl:if test="string-length(@name) &lt; 1 or string-length(@style) &lt; 1">
                <xsl:message terminate="yes">
                  <xsl:value-of select="concat('Both name and style should be present for parent ',../@name,$newLine)"/>
                </xsl:message>  
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose> 
        </xsl:otherwise>
      </xsl:choose>
      
      <xsl:choose>
        <xsl:when test="string-length()"></xsl:when>
      </xsl:choose>
      
      <xsl:variable name="Icon_Ancestor">
        <xsl:choose>
          <xsl:when test="name(parent::*)='DataSet'">
            <xsl:value-of select="ancestor::DataSet[1]/@name"/>
            <xsl:call-template name="validate_Icon_MainLevel"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='DataItem'">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Linked'">
            <xsl:value-of select="ancestor::Linked[1]/@by"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Link'">
            <xsl:value-of select="ancestor::Linked[1]/@by"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Display'">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Preview'">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:value-of select="concat('Icon ',@name,' should only have DataSet, DataItem, Linked or Link as parent.',$newLine)"/>
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:attribute name="parent_Node_Icon">
        <xsl:choose>
          <xsl:when test="name(parent::*)='DataSet'">
            <xsl:value-of select="'DATASET'"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='DataItem'">
            <xsl:value-of select="'DATAITEM'"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Linked'">
            <xsl:value-of select="'LINKED'"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Link'">
            <xsl:value-of select="'LINK'"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Display'">
            <xsl:value-of select="'DISPLAY'"/>
          </xsl:when>
          <xsl:when test="name(parent::*)='Preview'">
            <xsl:value-of select="'PREVIEW'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:value-of select="concat('Icon ',@name,' should only have DataSet, DataItem, Linked or Link as parent.',$newLine)"/>
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      
      <xsl:attribute name="Icon_level">
        <xsl:choose>
          <xsl:when test="count(ancestor::Link) &gt; 0">
            <xsl:value-of select="'LINK_LEVEL'"/>
          </xsl:when>
          <xsl:when test="count(ancestor::Linked) &gt; 0 and count(ancestor::Link) &lt; 1">
            <xsl:value-of select="'LINKED_LEVEL'"/>
          </xsl:when>
          <xsl:when test="count(ancestor::DataSet) &gt; 0 and count(ancestor::Linked) &lt; 1">
            <xsl:value-of select="'MAIN_LEVEL'"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      
      <xsl:if test="string-length(@style) &gt; 0">
        <xsl:choose>
          <xsl:when test="contains($icon_list_styles,@style)"/>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:value-of select="concat($Icon_Ancestor,' has invalid style ',@style,$newLine)"/>  
            </xsl:message>
          </xsl:otherwise>        
        </xsl:choose>  
      </xsl:if>
      
      <xsl:variable name="style_code">
        <xsl:call-template name="get_code_from_style">
          <xsl:with-param name="style" select="@style"/>
        </xsl:call-template>
      </xsl:variable>
      
      <xsl:variable name="code_with_name">
        <xsl:choose>
          <xsl:when test="string-length(@name) &gt; 0">
            <xsl:choose>
              <xsl:when test="substring(@name,1,3)=$PREFIX_ICON_NAME">
                <xsl:value-of select="@name"/>    
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat($PREFIX_ICON_NAME,@name)"/>
              </xsl:otherwise>
            </xsl:choose>            
          </xsl:when>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:attribute name="full_name_with_style_code">
        <xsl:choose>
          <xsl:when test="string-length($style_code) &gt; 0">
            <xsl:value-of select="concat($style_code,' ',$code_with_name)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$code_with_name"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template name="buildExportToReport">
    <xsl:param name="Current_dataItem"/>
    
    <xsl:variable name="count_valid_exportToReport">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][ExportToReport])"
      />
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$count_valid_exportToReport &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:attribute name="name_to_use">      
      <xsl:choose>
        <xsl:when test="string-length(ancestor::DataItem[1]/@alias) > 0">
          <xsl:value-of select="ancestor::DataItem[1]/@alias"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="use_subdoc_typefield"/>    
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
       
    
    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when test="@title != ''">
          <xsl:value-of select="@title"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@title!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@title"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="use_subdoc_typefield"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    
  </xsl:template>

  <xsl:template name="buildSearchWithinLastItem">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_searchwithinresults">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_searchwithinresults &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/Filter/SearchWithinResults/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/Filter/SearchWithinResults/@title"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/@title"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/Filter/SearchWithinResults/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/Filter/SearchWithinResults/@desc"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/@desc"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="parent_flag_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/SearchWithinResults[@display = 'true' or not(@display)]][1]/Filter/SearchWithinResults/@searchWithin != ''">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="buildBooleanLastItem">
    <xsl:param name="Boolean_or_Hierarchical"/>
    <xsl:param name="Current_dataItem"/>

    <xsl:choose>
      <xsl:when test="$Boolean_or_Hierarchical = 'Boolean'">
        <xsl:call-template name="make_attribute_boolean">
          <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$Boolean_or_Hierarchical = 'Hierarchical'">
        <xsl:call-template name="make_attribute_hierarchical">
          <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="make_attribute_boolean">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_boolean">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_boolean &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/Filter/Boolean/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/Filter/Boolean/@title"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/@title"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/Filter/Boolean/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/Filter/Boolean/@desc"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/@desc"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="length_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/Filter/Boolean/@length != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Boolean[@display = 'true' or not(@display)]][1]/Filter/Boolean/@length"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="subdoc_flag_to_use">
      <xsl:choose>
        <xsl:when test="ancestor::*[name() = 'Link']">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="buildSortLastItem">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_sort">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_sort &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/Filter/Sort/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/Filter/Sort/@title"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/@title"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/Filter/Sort/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/Filter/Sort/@desc"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/@desc"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="order_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/Filter/Sort/@default != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Sort[@display = 'true' or not(@display)]][1]/Filter/Sort/@default"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'desc'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>


  </xsl:template>

  <xsl:template name="buildDateRangeLastItem">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_daterange">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_daterange &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/Filter/DateRangeField/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/Filter/DateRangeField/@title"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/@title"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/Filter/DateRangeField/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/Filter/DateRangeField/@desc"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/DateRangeField[@display = 'true' or not(@display)]][1]/@desc"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="buildDisplayMetadata">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_metadata">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Display/Metadata])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_metadata &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">      
        <xsl:choose>
          <xsl:when test="@title != ''">
            <xsl:value-of select="@title"/>
          </xsl:when>        
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="ancestor::DataItem[1]/@title!=''">
                <xsl:value-of select="ancestor::DataItem[1]/@title"/>    
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="''"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>            
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">      
        <xsl:choose>
          <xsl:when test="@desc != ''">
            <xsl:value-of select="@desc"/>
          </xsl:when>        
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="ancestor::DataItem[1]/@desc!=''">
                <xsl:value-of select="ancestor::DataItem[1]/@desc"/>    
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="''"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>            
    </xsl:attribute>

    <xsl:attribute name="span_to_use">
      <xsl:choose>
        <xsl:when test="@span != ''">
          <xsl:value-of select="@span"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:value-of select="''"/>          
        </xsl:otherwise>
      </xsl:choose>     
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="buildDisplayDetails">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_details">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Display/Details])"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_details &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when test="@title != ''">
          <xsl:value-of select="@title"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@title!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@title"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="''"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when test="@desc != ''">
          <xsl:value-of select="@desc"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@desc!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@desc"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="''"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

    <xsl:attribute name="prefix_to_use">
      <xsl:choose>
        <xsl:when test="@prefix != ''">
          <xsl:value-of select="@prefix"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:value-of select="''"/>          
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="buildPreviewDetails">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_preview">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Preview/Details])"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_preview &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when test="@title != ''">
          <xsl:value-of select="@title"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@title!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@title"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="''"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when test="@desc != ''">
          <xsl:value-of select="@desc"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@desc!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@desc"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="''"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

    <xsl:attribute name="prefix_to_use">
      <xsl:choose>
        <xsl:when test="@prefix != ''">
          <xsl:value-of select="@prefix"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:value-of select="''"/>          
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="buildPreviewMetadata">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_metadata">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Preview/Metadata])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_metadata &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when test="@title != ''">
          <xsl:value-of select="@title"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@title!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@title"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="''"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when test="@desc != ''">
          <xsl:value-of select="@desc"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@desc!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@desc"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="''"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

    <xsl:attribute name="span_to_use">
      <xsl:choose>
        <xsl:when test="@span != ''">
          <xsl:value-of select="@span"/>
        </xsl:when>        
        <xsl:otherwise>
          <xsl:value-of select="''"/>          
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:attribute>

  </xsl:template>

  <xsl:template name="make_attribute_hierarchical">
    <xsl:param name="Current_dataItem"/>

    <xsl:variable name="count_valid_hierarchical">
      <xsl:value-of
        select="count(ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_valid_hierarchical &gt; 0">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:attribute name="title_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/Filter/Hierarchical/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/Filter/Hierarchical/@title"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/@title != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/@title"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="desc_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/Filter/Hierarchical/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/Filter/Hierarchical/@desc"
          />
        </xsl:when>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/@desc != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/@desc"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>


    <xsl:attribute name="subdoc_flag_to_use">
      <xsl:choose>
        <xsl:when test="ancestor::*[name() = 'Link']">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:attribute name="separator_to_use">
      <xsl:choose>
        <xsl:when
          test="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/Filter/Hierarchical/@separator != ''">
          <xsl:value-of
            select="ancestor::DataSet[1]//DataItem[@name = $Current_dataItem][Filter/Hierarchical[@display = 'true' or not(@display)]][1]/Filter/Hierarchical/@separator"
          />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>
  
</xsl:stylesheet>
