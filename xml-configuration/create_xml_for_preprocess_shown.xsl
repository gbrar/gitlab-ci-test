<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:ext="http://exslt.org/common">
  <xsl:include href="frisk_shown_fields_utils.xsl"/>
  
  <!-- Identity template : copy all text nodes, elements and attributes -->
  <xsl:template match="node() | @*" mode="preprocess_shown">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="DataSet" mode="preprocess_shown">
    <xsl:copy>

      <xsl:call-template name="Make_Include_Filter_Delimiter"/>

      <xsl:call-template name="Make_Exclude_Filter_Delimiter"/>

      <xsl:apply-templates select="@* | node()" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="Make_Exclude_Filter_Delimiter">
    <xsl:variable name="All_exclude_values">
      <xsl:apply-templates select="./descendant::*[name() = 'Exclude']" mode="All_Exclude_Values"/>
    </xsl:variable>

    <!-- Make sure Include values dont have both | and # -->
    <xsl:choose>
      <xsl:when test="contains($All_exclude_values, '|') and contains($All_exclude_values, '#')">
        <xsl:message terminate="yes">
          <xsl:value-of
            select="concat('It is not allowed for Exclude Nodes in this Dataset to have both # and I', $newLine)"
          />
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:attribute name="Exclude_Delimiter">
      <xsl:choose>
        <xsl:when test="contains($All_exclude_values, '|')">
          <xsl:value-of select="'#'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'|'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="Make_Include_Filter_Delimiter">
    <xsl:variable name="All_include_values">
      <xsl:apply-templates select="./descendant::*[name() = 'Include']" mode="All_Include_Values"/>
    </xsl:variable>

    <!-- Make sure Include values dont have both | and # -->
    <xsl:choose>
      <xsl:when test="contains($All_include_values, '|') and contains($All_include_values, '#')">
        <xsl:message terminate="yes">
          <xsl:value-of
            select="concat('It is not allowed for Include Nodes in this Dataset to have both # and I', $newLine)"
          />
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:attribute name="Include_Delimiter">
      <xsl:choose>
        <xsl:when test="contains($All_include_values, '|')">
          <xsl:value-of select="'#'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'|'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="Include" mode="All_Include_Values">
    <xsl:value-of select="@value"/>
  </xsl:template>

  <xsl:template match="Exclude" mode="All_Exclude_Values">
    <xsl:value-of select="@value"/>
  </xsl:template>

  <xsl:template match="DataItem" mode="preprocess_shown">
    <xsl:copy>
      <xsl:choose>
        <xsl:when test="count(child::ExportToReport) &gt; 0 and count(child::Security) &lt; 1">
          <xsl:attribute name="exportReportFinal">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="exportReportFinal">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:variable name="eic_converted">
        <xsl:call-template name="fisc_to_eic_mapping">
          <xsl:with-param name="InputField" select="@name"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="final_name_to_use_shown">
        <xsl:call-template name="fisc_to_metadata_mapping">
          <xsl:with-param name="LineNamePre" select="$eic_converted"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="final_using_subdoc_typefield">
        <xsl:choose>
          <xsl:when test="string-length(@typefield) > 0">
            <xsl:value-of select="concat('$opt{subdoc_type_field:', ancestor::DataSet[1]/@db, '}')"
            />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$final_name_to_use_shown"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:attribute name="final_shown_label">
        <xsl:choose>
          <xsl:when test="string-length(@alias) > 0">
            <xsl:value-of select="@alias"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$final_using_subdoc_typefield"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="not(@type) or @type = ''">
          <xsl:attribute name="type">
            <xsl:value-of select="'string'"/>
          </xsl:attribute>
        </xsl:when>
      </xsl:choose>
      
      <xsl:variable name="count_internal">
        <xsl:value-of select="count(child::*[name()='Icon']) + count(Display/Icon) + count(Preview/Icon)"/>
      </xsl:variable>
      
      <xsl:variable name="Icon_flag_Internal">
        <xsl:choose>
          <xsl:when test="$count_internal &gt; 0">
            <xsl:value-of select="true()"/>        
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:variable name="Icon_flag_External">
        <xsl:choose>
          <xsl:when test="count(key('Icon_Tags_By_name', @name)) &gt; 0">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:attribute name="Icon_Flag">
        <xsl:choose>
          <xsl:when test="$Icon_flag_Internal='true' or $Icon_flag_External='true'">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      
      <xsl:apply-templates select="@* | node()" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Sample" mode="preprocess_shown">
    <xsl:copy>
      <xsl:variable name="preview_or_display">
        <xsl:choose>
          <xsl:when test="name(parent::*) = 'Preview'">
            <xsl:value-of select="'preview'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'display'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:choose>
        <xsl:when test="not(@length) or @length = ''">
          <xsl:if test="$preview_or_display = 'display'">
            <xsl:attribute name="length">
              <xsl:value-of select="'200'"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:when>
      </xsl:choose>
      <xsl:apply-templates select="node() | @*" mode="preprocess_shown"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template match="URL" mode="preprocess_shown">
    <xsl:choose>
      <xsl:when test="ancestor::DataItem[1]/@type = 'number'">
        <xsl:message terminate="yes">
          <xsl:value-of
            select="concat(ancestor::DataItem[1]/@name, ' has URL Node and type is numeric.It can be only string ', $newLine)"
          />
        </xsl:message>
      </xsl:when>
      <xsl:when test="ancestor::DataItem[1]/@type = 'date'">
        <xsl:message terminate="yes">
          <xsl:value-of
            select="concat(ancestor::DataItem[1]/@name, ' has URL Node and type is date.It can be only string ', $newLine)"
          />
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="string-length(ancestor::DataItem[1]/@split) &gt; 0">
        <xsl:message terminate="yes">
          <xsl:value-of
            select="concat(ancestor::DataItem[1]/@name, ' has URL Node and split attribute is present.It cannot have split attribute ', $newLine)"
          />
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="MandatoryOnSearch" mode="preprocess_shown">
    <xsl:copy>
      <xsl:attribute name="BooleanFilter">
        <xsl:call-template name="getBooleanNumber"/>
      </xsl:attribute>
      <xsl:apply-templates select="node() | @*" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Include" mode="preprocess_shown">
    <xsl:copy>
      <xsl:attribute name="Add_to_filter">
        <xsl:choose>
          <xsl:when test="string-length(@value) &gt; 0">
            <xsl:call-template name="calculate_suffix_filter">
              <xsl:with-param name="value_to_calculate" select="@value"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates select="node() | @*" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Exclude" mode="preprocess_shown">
    <xsl:copy>
      <xsl:attribute name="Add_to_filter">
        <xsl:choose>
          <xsl:when test="string-length(@value) &gt; 0">
            <xsl:call-template name="calculate_suffix_filter">
              <xsl:with-param name="value_to_calculate" select="@value"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates select="node() | @*" mode="preprocess_shown"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Icon" mode="preprocess_shown">
    <xsl:copy>
      <xsl:variable name="field_name">
        <xsl:choose>
          <xsl:when test="name(parent::*)='DataItem' or name(parent::*)='Display' or name(parent::*)='Preview'">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
          </xsl:when>
          <xsl:when test="string-length(@parse) &gt; 0">
            <xsl:value-of select="@parse"/>
          </xsl:when>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:if test="string-length($field_name) &gt; 0">
        <xsl:for-each select="key('DataItems_By_Name', $field_name)">
          <xsl:choose>
            <xsl:when test="string-length(@split) &gt; 0">
              <xsl:message terminate="yes">
                <xsl:value-of
                  select="concat('Split cannot be present on DataItem which has parse.Offending dataitem is ', $field_name, $newLine)"
                />
              </xsl:message>
            </xsl:when>
          </xsl:choose>         
        </xsl:for-each>  
      </xsl:if>
       
      <xsl:attribute name="field_name_attrib">
        <xsl:value-of select="$field_name"/>
      </xsl:attribute>
      
      <xsl:attribute name="make_icon_field_for_indexing">
        <xsl:choose>
          <xsl:when test="string-length($field_name) &gt; 0">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates select="@* | node()" mode="preprocess_index"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="calculate_suffix_filter">
    <xsl:param name="value_to_calculate"/>

    <xsl:variable name="first_letter">
      <xsl:choose>
        <xsl:when test="string-length($value_to_calculate) &gt; 0">
          <xsl:value-of select="substring($value_to_calculate, 1, 1)"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="suffix_filter">
      <xsl:if test="string-length($first_letter) &gt; 0">
        <xsl:choose>
          <xsl:when test="contains('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $first_letter)">
            <xsl:value-of select="concat(':', $value_to_calculate)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$value_to_calculate"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:variable>

    <xsl:value-of select="$suffix_filter"/>

  </xsl:template>

  <xsl:template name="getBooleanNumber">
    <xsl:variable name="does_boolean_exist">
      <xsl:choose>
        <xsl:when test="count(ancestor::DataItem[1]/Filter/Boolean) &gt; 0">
          <xsl:value-of select="'true'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'false'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="does_searchwithinresults_exist">
      <xsl:choose>
        <xsl:when test="count(ancestor::DataItem[1]/Filter/SearchWithinResults) &gt; 0">
          <xsl:value-of select="'true'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'false'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="does_security_exist">
      <xsl:choose>
        <xsl:when test="count(ancestor::DataItem[1]/Security) &gt; 0">
          <xsl:value-of select="'true'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'false'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:call-template name="calculate_boolean_using_flags">
      <xsl:with-param name="boolean_flag" select="$does_boolean_exist"/>
      <xsl:with-param name="searchwithin_flag" select="$does_searchwithinresults_exist"/>
      <xsl:with-param name="security_flag" select="$does_security_exist"/>
    </xsl:call-template>
  </xsl:template>
</xsl:stylesheet>
