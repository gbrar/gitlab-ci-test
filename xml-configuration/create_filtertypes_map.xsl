<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" omit-xml-declaration="yes"/>
    <xsl:include href="frisk_xml_parser_utils.xsl"/>

    <xsl:template match="DataSetConfig">
        <xsl:apply-templates select="DataSet"/>
    </xsl:template>

    <xsl:template match="DataSet">
        <xsl:apply-templates select="ConnectionType"/>
    </xsl:template>

    <xsl:template match="ConnectionType">
        <xsl:variable name="final_Result">
            <xsl:apply-templates
                select="/descendant::*[name() = 'DataItem'][Filter/Boolean or Filter/Hierarchical or Filter/SearchWithinResults]"
            />
        </xsl:variable>
        <xsl:variable name="Strip_last_comma">
            <xsl:call-template name="strip-end-comma">
                <xsl:with-param name="text" select="$final_Result"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$Strip_last_comma != ''">
                <xsl:value-of select="concat('$setmap{filtertypes,', $Strip_last_comma, '}')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat('$setmap{filtertypes_', ',,}')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="DataItem">
        <xsl:apply-templates select="Filter"/>
    </xsl:template>

    <xsl:template match="Filter">
        <xsl:apply-templates select="Boolean | Hierarchical | SearchWithinResults"/>
    </xsl:template>

    <!-- Only calculate prefixes for Boolean or Hierarchical or SearchWithinResults Tag inside Filter -->
    <xsl:template match="Boolean | SearchWithinResults | Hierarchical">
        <xsl:variable name="Current_dataItem">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
        </xsl:variable>

        <xsl:choose>
            <xsl:when
                test="not($Current_dataItem = preceding::DataItem[Filter/Boolean or Filter/Hierarchical or Filter/SearchWithinResults]/@name)">
                <xsl:variable name="precedingBoolean">
                    <xsl:number format="1"
                        count="child::*[Boolean or SearchWithinResults or Hierarchical]" level="any"
                        from="ConnectionType"/>
                </xsl:variable>
                <xsl:variable name="IncNumber">
                    <xsl:value-of select="$NumberNode + $precedingBoolean - 1"/>
                </xsl:variable>
                <xsl:variable name="NodeLetters">
                    <xsl:number value="$IncNumber" format="A"/>
                </xsl:variable>
                <xsl:variable name="booleanNumber">
                    <xsl:value-of select="concat('X', $NodeLetters)"/>
                </xsl:variable>
                <xsl:variable name="fisc_constant">
                    <xsl:value-of select="@fiscConstantPrefix"/>
                </xsl:variable>
                
                <xsl:variable name="fisc_constant_value">
                    <xsl:if test="string-length($fisc_constant) &gt; 0">
                        <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$fisc_constant]"/>
                    </xsl:if>
                </xsl:variable>
                
                <xsl:choose>
                    <xsl:when test="string-length($fisc_constant) &gt; 0">
                        <xsl:if test="string-length($fisc_constant_value) &lt; 1">
                            <xsl:message terminate="yes">
                                <xsl:value-of select="concat(ancestor::DataItem[1]/@name,' has constant which is not defined',$newLine)"/>
                            </xsl:message>
                        </xsl:if>
                    </xsl:when>
                </xsl:choose>
                <xsl:variable name="variable_to_use">
                    <xsl:choose>
                        <xsl:when test="string-length($fisc_constant_value) &gt; 0">
                            <xsl:value-of select="$fisc_constant_value"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$booleanNumber"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="subdoc_or_doc">
                    <xsl:choose>
                        <xsl:when test="not(ancestor::Link)">
                            <xsl:value-of select="'doc'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'subdoc'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:if test="string-length($variable_to_use) &gt; 0">
                    <xsl:value-of select="$variable_to_use"/>
                    <xsl:text>,</xsl:text>
                    <xsl:value-of select="$subdoc_or_doc"/>
                    <xsl:text>,</xsl:text>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
