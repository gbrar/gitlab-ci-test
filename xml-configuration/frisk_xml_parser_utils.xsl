<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="frisk_constants.xsl"/>
    
  <xsl:key name="DataItems_By_Name" match="DataItem" use="@name"/>
  
  <xsl:key name="Icon_Tags_By_name_attrib" match="Icon" use="@field_name_attrib"/>
  
  <xsl:key name="Icon_Tags_By_name" match="Icon" use="@parse"/>
  
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
  <xsl:variable name="double_quotes">"</xsl:variable>
  <xsl:variable name="spaces16">
    <xsl:text>                </xsl:text>
  </xsl:variable>
  <xsl:variable name="spaces14">
    <xsl:text>              </xsl:text>
  </xsl:variable>

  <xsl:variable name="newLine">
    <xsl:text>&#xa;</xsl:text>
  </xsl:variable>
  
  <xsl:variable name="FISC_SECURITY_PREFIX">
    <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key='FISC_SECURITY_PREFIX']"/>
  </xsl:variable>
  
  <xsl:variable name="FISC_EIC_AUDIT_STATUS">
    <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key='FISC_EIC_AUDIT_STATUS']"/>
  </xsl:variable>
  
  <xsl:variable name="FISC_EMBEDDED_FILE">
    <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key='FISC_EMBEDDED_FILE']"/>
  </xsl:variable>

  <xsl:variable name="HASH" select="'hash'"/>

  <!-- Three Letters from where prefixes(boolean, path and metadata terms) Start -->
  <xsl:variable name="NumberNode">
    <xsl:call-template name="string-to-num">
      <xsl:with-param name="string">BAA</xsl:with-param>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="Max_len_desc" select="50"/>
  <xsl:variable name="PrefixLetter_Generated" select="'X'"/>
  <xsl:variable name="SecuritySlot" select="11"/>
  <xsl:variable name="Typefield_String" select="'subdoc_typefield'"/>
  <xsl:variable name="subdoc_variable_name" select="'subdoc_type_field'"/>

  <xsl:variable name="DateRange_tag">
    <xsl:value-of select="concat($double_quotes, 'dateRange', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="SearchWithin_tag">
    <xsl:value-of select="concat($double_quotes, 'searchWithinField', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="Filter_tag">
    <xsl:value-of select="concat($double_quotes, 'filter', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="Sort_tag">
    <xsl:value-of select="concat($double_quotes, 'sort', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="Metadata_tag">
    <xsl:value-of select="concat($double_quotes, 'metadata', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="MetadataInfo_tag">
    <xsl:value-of select="concat($double_quotes, 'metadataInfo', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="exportReport_tag">
    <xsl:value-of select="concat($double_quotes, 'exportToReport', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="Details_tag">
    <xsl:value-of select="concat($double_quotes, 'details', $double_quotes, ' :[')"/>
  </xsl:variable>

  <xsl:variable name="Icon_tag">
    <xsl:value-of select="concat($double_quotes, 'icon', $double_quotes, ' :{')"/>
  </xsl:variable>

  <xsl:variable name="Highlight_tag">
    <xsl:value-of select="concat($double_quotes, 'highlight', $double_quotes, '  : ')"/>
  </xsl:variable>

  <xsl:variable name="Icon_Keyword">
    <xsl:value-of select="'parse'"/>
  </xsl:variable>

  <xsl:variable name="Preview_Pre">
    <xsl:value-of select="concat($double_quotes, 'preview', $double_quotes, ':{', $newLine)"/>
  </xsl:variable>

  <xsl:variable name="Preview_Post">
    <xsl:value-of select="concat($newLine, '}')"/>
  </xsl:variable>

  <xsl:variable name="Subdoc_Start_Line">
    <xsl:value-of select="concat(',', $double_quotes, 'subdocs', $double_quotes, ' :{', $newLine)"/>
  </xsl:variable>

  <xsl:variable name="Typefield_Line">
    <xsl:value-of
      select="concat($double_quotes, 'typefield', $double_quotes, ': ', $double_quotes, 'subdoc_typefield', $double_quotes, $newLine)"
    />
  </xsl:variable>

  <xsl:variable name="odbc_header_dataset" select="'odbc'"/>

  <xsl:variable name="export_header_dataset" select="'export'"/>

  <xsl:variable name="hdfs_share_header_dataset" select="'hdfs_share'"/>

  <xsl:variable name="file_share_header_dataset" select="'file_share'"/>

  <xsl:variable name="source_hdfs_type" select="'hdfs'"/>

  <xsl:variable name="source_iodl_type" select="'iodl'"/>

  <xsl:template name="strip-end-comma">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="not(substring($text, string-length($text)) = ',')">
        <xsl:value-of select="$text"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="substring($text, 1, string-length($text) - 1)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="appendAZ">
    <xsl:param name="Capital_Letters"/>
    <xsl:param name="value"/>

    <xsl:if test="string-length($Capital_Letters) &gt; 0">
      <xsl:value-of select="concat('X', $value, substring($Capital_Letters, 1, 1), ' ')"/>
      <xsl:call-template name="appendAZ">
        <xsl:with-param name="Capital_Letters" select="substring($Capital_Letters, 2)"/>
        <xsl:with-param name="value" select="$value"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:template>

  <xsl:template name="distinct-values-from-list">
    <xsl:param name="list"/>
    <xsl:param name="delimiter" select="' '"/>
    <xsl:param name="result"/>
    <xsl:choose>
      <xsl:when test="$list">
        <xsl:variable name="token" select="substring-before(concat($list, $delimiter), $delimiter)"/>
        <!-- recursive call -->
        <xsl:call-template name="distinct-values-from-list">
          <xsl:with-param name="list" select="substring-after($list, $delimiter)"/>
          <xsl:with-param name="result">
            <xsl:value-of select="$result"/>
            <!-- add token if this is its first occurrence -->
            <xsl:if
              test="not(contains(concat($delimiter, $result, $delimiter), concat($delimiter, $token, $delimiter)))">
              <xsl:value-of select="concat($delimiter, $token)"/>
            </xsl:if>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="substring($result, 2)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="replace-string">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="with"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text, $replace)"/>
        <xsl:value-of select="$with"/>
        <xsl:call-template name="replace-string">
          <xsl:with-param name="text" select="substring-after($text, $replace)"/>
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="with" select="$with"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="string-to-num">
    <xsl:param name="string"/>
    <xsl:param name="alpha" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    <xsl:param name="magnitude" select="1"/>
    <xsl:param name="carryover" select="0"/>
    <xsl:param name="bit" select="substring($string, string-length($string), 1)"/>
    <xsl:param name="bit-value" select="string-length(substring-before($alpha, $bit)) + 1"/>
    <xsl:variable name="return" select="$carryover + $bit-value * $magnitude"/>
    <xsl:choose>
      <xsl:when test="string-length($string) > 1">
        <xsl:call-template name="string-to-num">
          <xsl:with-param name="string" select="substring($string, 1, string-length($string) - 1)"/>
          <xsl:with-param name="magnitude" select="string-length($alpha) * $magnitude"/>
          <xsl:with-param name="carryover" select="$return"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$return"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="GetNoOfOccurance">
    <xsl:param name="String"/>
    <xsl:param name="SubString"/>
    <xsl:param name="Counter" select="0"/>

    <xsl:variable name="sa" select="substring-after($String, $SubString)"/>

    <xsl:choose>
      <xsl:when test="$sa != '' or contains($String, $SubString)">
        <xsl:call-template name="GetNoOfOccurance">
          <xsl:with-param name="String" select="$sa"/>
          <xsl:with-param name="SubString" select="$SubString"/>
          <xsl:with-param name="Counter" select="$Counter + 1"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$Counter"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="calculatePrefixLine">
    <xsl:param name="prePrefix"/>
    <xsl:param name="StartLineString"/>
    <xsl:param name="PrefixLetters"/>
    <xsl:param name="FISC_Constant_Boolean"/>
    <xsl:param name="FISC_Constant_SearchWithinResults"/>
    <xsl:param name="FISC_Constant_Hierarchical"/>
    <xsl:param name="dont_use_suffix"/>
    
    <xsl:choose>
      <xsl:when test="string-length($FISC_Constant_Boolean) &gt; 0 and string-length($FISC_Constant_SearchWithinResults) &gt; 0">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat(ancestor::DataItem[1],' has Boolean and SearchWithinResults on same Filter Node',$newLine)"/>
        </xsl:message>
      </xsl:when>
    </xsl:choose>
    
    <xsl:variable name="boolean_constants">
      <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$FISC_Constant_Boolean]"/>
    </xsl:variable>
    
    <xsl:variable name="search_within_results_constants">
      <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$FISC_Constant_SearchWithinResults]"/>
    </xsl:variable>
    
    <xsl:variable name="hierarchical_constants">
      <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$FISC_Constant_Hierarchical]"/>
    </xsl:variable>
    
    
    <xsl:variable name="Suffix">
      <xsl:choose>
        <xsl:when test="ancestor::Linked[1]/@by != ''">
          <xsl:value-of select="concat('{', ancestor::Linked[1]/@by, '}')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="IfStartLinePresent">
      <xsl:choose>
        <xsl:when test="$StartLineString != ''">
          <xsl:value-of select="concat($StartLineString, '=')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="IncNumber">
      <xsl:value-of select="$NumberNode + $prePrefix"/>
    </xsl:variable>

    <xsl:variable name="NodeLetters">
      <xsl:number value="$IncNumber" format="A"/>
    </xsl:variable>
    
    <xsl:variable name="constant_to_use">
      <xsl:choose>
        <xsl:when test="string-length($boolean_constants) &gt; 0">
          <xsl:value-of select="$boolean_constants"/>
        </xsl:when>
        <xsl:when test="string-length($search_within_results_constants) &gt; 0">
          <xsl:value-of select="$search_within_results_constants"/>
        </xsl:when>
        <xsl:when test="string-length($hierarchical_constants) &gt; 0">
          <xsl:value-of select="$hierarchical_constants"/>
        </xsl:when>        
      </xsl:choose>
    </xsl:variable>
    
    <xsl:if test="string-length($boolean_constants) &gt; 0 or string-length($search_within_results_constants) &gt; 0 or string-length($hierarchical_constants) &gt; 0">
      <xsl:if test="string-length($constant_to_use) &lt; 1">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat(ancestor::DataItem[1],' has constant on Filter Node but value is not defined',$newLine)"/>  
        </xsl:message>
      </xsl:if>
    </xsl:if>
    
    <xsl:choose>
      <xsl:when test="string-length($constant_to_use) &gt; 0">
        <xsl:choose>
          <xsl:when test="$dont_use_suffix='NO'">
            <xsl:value-of select="concat($IfStartLinePresent, $constant_to_use)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($IfStartLinePresent, $constant_to_use, $Suffix)"/>    
          </xsl:otherwise>
        </xsl:choose>            
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$dont_use_suffix='NO'">
            <xsl:value-of select="concat($IfStartLinePresent,$PrefixLetters,$NodeLetters)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($IfStartLinePresent,$PrefixLetters,$NodeLetters,$Suffix)"/>    
          </xsl:otherwise>
        </xsl:choose>          
      </xsl:otherwise>
    </xsl:choose>
        
  </xsl:template>

  <xsl:template name="fisc_to_metadata_mapping">
    <xsl:param name="LineNamePre"/>
    <xsl:variable name="LineName">
      <xsl:choose>
        <xsl:when test="$LineNamePre = 'fisc_author'">
          <xsl:value-of select="'extract_text_author'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_boolean_term'">
          <xsl:value-of select="'extract_text_boolean_term'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_created'">
          <xsl:value-of select="'extract_text_created'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_extracted_text'">
          <xsl:value-of select="'extract_text_extracted_text'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_keyword'">
          <xsl:value-of select="'extract_text_keyword'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_leaf_name'">
          <xsl:value-of select="'extract_text_leaf_name'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_md5'">
          <xsl:value-of select="'extract_text_md5'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_modtime'">
          <xsl:value-of select="'extract_text_modtime'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_ocr'">
          <xsl:value-of select="'extract_text_ocr'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_sample'">
          <xsl:value-of select="'extract_text_sample'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_spec'">
          <xsl:value-of select="'Stream_fileMsg_fileSpec'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_url'">
          <xsl:value-of select="'Stream_fileMsg_fileURL'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_last_modified'">
          <xsl:value-of select="'Stream_fileMsg_modified_ts'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_last_accessed'">
          <xsl:value-of select="'Stream_fileMsg_accessed_ts'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_size'">
          <xsl:value-of select="'Stream_fileMsg_size'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_desc'">
          <xsl:value-of select="'Stream_mimeMsg_mime_description'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_category'">
          <xsl:value-of select="'Stream_mimeMsg_mime_category'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_type'">
          <xsl:value-of select="'Stream_mimeMsg_mime_type'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_encoding'">
          <xsl:value-of select="'Stream_mimeMsg_mime_encoding'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_image_width'">
          <xsl:value-of select="'Stream_mimeMsg_mime_x'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_image_height'">
          <xsl:value-of select="'Stream_mimeMsg_mime_y'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_relative_path'">
          <xsl:value-of select="'Stream_fileMsg_relativePath'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$LineNamePre"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="$LineName"/>
  </xsl:template>

  <xsl:template name="fisc_to_eic_mapping">
    <xsl:param name="InputField"/>
    <xsl:variable name="LineName">
      <xsl:choose>
        <xsl:when test="$InputField = 'fisc_file_spec'">
          <xsl:value-of select="'eic_file_spec'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_file_url'">
          <xsl:value-of select="'eic_url'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_file_last_modified'">
          <xsl:value-of select="'eic_modified'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_file_last_accessed'">
          <xsl:value-of select="'eic_accessed'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_file_size'">
          <xsl:value-of select="'eic_file_size'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_mime_desc'">
          <xsl:value-of select="'eic_mime_description'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_mime_category'">
          <xsl:value-of select="'eic_mime_category'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_mime_type'">
          <xsl:value-of select="'eic_mime_type'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_mime_encoding'">
          <xsl:value-of select="'eic_mime_encoding'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_image_width'">
          <xsl:value-of select="'eic_mime_x'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_image_height'">
          <xsl:value-of select="'eic_mime_y'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_md5'">
          <xsl:value-of select="'extract_text_md5'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_ocr'">
          <xsl:value-of select="'extract_text_ocr'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_sample'">
          <xsl:value-of select="'extract_text_sample'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_author'">
          <xsl:value-of select="'extract_text_author'"/>
        </xsl:when>
        <xsl:when test="$InputField = 'fisc_keyword'">
          <xsl:value-of select="'extract_text_keyword'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$InputField"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="$LineName"/>
  </xsl:template>

  <xsl:template name="file_ref_mapping">
    <xsl:param name="LineNamePre"/>
    <xsl:variable name="LineName">
      <xsl:choose>
        <xsl:when test="$LineNamePre = 'fisc_file_spec'">
          <xsl:value-of select="'eic_file_spec'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_url'">
          <xsl:value-of select="'eic_url'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_last_modified'">
          <xsl:value-of select="'eic_modified'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_last_accessed'">
          <xsl:value-of select="'eic_accessed'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_file_size'">
          <xsl:value-of select="'eic_file_size'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_desc'">
          <xsl:value-of select="'eic_mime_description'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_category'">
          <xsl:value-of select="'eic_mime_category'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_type'">
          <xsl:value-of select="'eic_mime_type'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_mime_encoding'">
          <xsl:value-of select="'eic_mime_encoding'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_image_width'">
          <xsl:value-of select="'eic_mime_x'"/>
        </xsl:when>
        <xsl:when test="$LineNamePre = 'fisc_image_height'">
          <xsl:value-of select="'eic_mime_y'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="$LineName"/>
  </xsl:template>

  <xsl:template name="eic_to_fisc_mapping">
    <xsl:param name="InputField"/>
    <xsl:choose>
      <xsl:when test="$InputField = 'eic_file_spec'">
        <xsl:value-of select="'fisc_file_spec'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_url'">
        <xsl:value-of select="'fisc_file_url'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_modified'">
        <xsl:value-of select="'fisc_file_last_modified'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_accessed'">
        <xsl:value-of select="'fisc_file_last_accessed'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_file_size'">
        <xsl:value-of select="'fisc_file_size'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_mime_description'">
        <xsl:value-of select="'fisc_mime_desc'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_mime_category'">
        <xsl:value-of select="'fisc_mime_category'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_mime_type'">
        <xsl:value-of select="'fisc_mime_type'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_mime_encoding'">
        <xsl:value-of select="'fisc_mime_encoding'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_mime_x'">
        <xsl:value-of select="'fisc_image_width'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'eic_mime_y'">
        <xsl:value-of select="'fisc_image_height'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'extract_text_md5'">
        <xsl:value-of select="'fisc_md5'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'extract_text_ocr'">
        <xsl:value-of select="'fisc_ocr'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'extract_text_sample'">
        <xsl:value-of select="'fisc_sample'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'extract_text_author'">
        <xsl:value-of select="'fisc_author'"/>
      </xsl:when>
      <xsl:when test="$InputField = 'extract_text_keyword'">
        <xsl:value-of select="'fisc_keyword'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$InputField"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="by"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text, $replace)"/>
        <xsl:value-of select="$by"/>
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text" select="substring-after($text, $replace)"/>
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="strip-end-characters">
    <xsl:param name="text"/>
    <xsl:param name="strip-count"/>
    <xsl:if test="substring($text, string-length($text), 1) = ','"/>
    <xsl:value-of select="substring($text, 1, string-length($text) - $strip-count)"/>
  </xsl:template>

  <xsl:template name="calculateFullPrefixNumber">
    <xsl:param name="preCount"/>
    <xsl:param name="FISC_Constant_SearchWithinResults"/>
    <xsl:param name="FISC_Constant_Boolean" />
    <xsl:param name="FISC_Constant_Hierarchical"/>
    
    <xsl:choose>
      <xsl:when test="string-length($FISC_Constant_Boolean) &gt; 0 and string-length($FISC_Constant_SearchWithinResults) &gt; 0">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat(ancestor::DataItem[1],' has Boolean and SearchWithinResults on same Filter Node',$newLine)"/>
        </xsl:message>
      </xsl:when>
    </xsl:choose>
    
    <xsl:variable name="boolean_constants">
      <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$FISC_Constant_Boolean]"/>
    </xsl:variable>
    
    <xsl:variable name="search_within_results_constants">
      <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$FISC_Constant_SearchWithinResults]"/>
    </xsl:variable>
    
    <xsl:variable name="hierarchical_constants">
      <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$FISC_Constant_Hierarchical]"/>
    </xsl:variable>
    

    <xsl:variable name="IncNumber">
      <xsl:value-of select="$NumberNode + $preCount"/>
    </xsl:variable>
    <xsl:variable name="NodeLetters">
      <xsl:number value="$IncNumber" format="A"/>
    </xsl:variable>
    
    <xsl:variable name="constant_to_use">
      <xsl:choose>
        <xsl:when test="string-length($boolean_constants) &gt; 0">
          <xsl:value-of select="$boolean_constants"/>
        </xsl:when>
        <xsl:when test="string-length($search_within_results_constants) &gt; 0">
          <xsl:value-of select="$search_within_results_constants"/>
        </xsl:when>
        <xsl:when test="string-length($hierarchical_constants) &gt; 0">
          <xsl:value-of select="$hierarchical_constants"/>
        </xsl:when>        
      </xsl:choose>
    </xsl:variable>
    
    <xsl:if test="string-length($boolean_constants) &gt; 0 or string-length($search_within_results_constants) &gt; 0 or string-length($hierarchical_constants) &gt; 0">
      <xsl:if test="string-length($constant_to_use) &lt; 1">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat(ancestor::DataItem[1],' has constant on Filter Node but value is not defined',$newLine)"/>  
        </xsl:message>
      </xsl:if>
    </xsl:if>
    
    <xsl:choose>
      <xsl:when test="string-length($constant_to_use) &gt; 0">
        <xsl:value-of select="$constant_to_use"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('X', $NodeLetters)"/>  
      </xsl:otherwise>
    </xsl:choose>
    
    
  </xsl:template>
  
  <xsl:template name="get_slot_number_constants">    
    <xsl:variable name="fisc_constant_prefix">
      <xsl:value-of select="@fiscConstantPrefix"/>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant_value">
      <xsl:if test="string-length($fisc_constant_prefix) &gt; 0">
        <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$fisc_constant_prefix]"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($fisc_constant_prefix) &gt; 0">
        <xsl:if test="string-length($fisc_constant_value) &lt; 1">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(ancestor::DataItems[1]/@name,' has constant which is not defined',$newLine)"/>
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
    
    <xsl:variable name="combinedSlot">
      <xsl:call-template name="calculateSlotNumber">
        <xsl:with-param name="FISC_CONSTANT_SLOT" select="$fisc_constant_value"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="$combinedSlot"/>
  </xsl:template>

  <xsl:template name="calculatePrefixNumber">
    <!-- Prefixes are made only if child is either of these 3    -->

    <xsl:variable name="precedingBooleanInit">
      <xsl:number format="1"
        count="child::DataItem[Filter/Boolean or Filter/SearchWithinResults or Filter/Hierarchical]"
        level="any" from="DataSet"/>
    </xsl:variable>

    <xsl:variable name="precedingBoolean">
      <xsl:choose>
        <xsl:when test="number($precedingBooleanInit)">
          <xsl:value-of select="$precedingBooleanInit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="precedingSecurityInit">
      <xsl:number format="1" count="child::DataItem[Security]" level="any" from="DataSet"/>
    </xsl:variable>

    <xsl:variable name="precedingSecurity">
      <xsl:choose>
        <xsl:when test="number($precedingSecurityInit)">
          <xsl:value-of select="$precedingSecurityInit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$precedingBoolean + $precedingSecurity - 1"/>

  </xsl:template>

  <xsl:template name="Add_Field_if_need">
    <xsl:param name="FieldParam"/>

    <xsl:variable name="Link_Field">
      <xsl:choose>
        <xsl:when test="ancestor::Linked[1]/@by != ''">
          <xsl:value-of select="ancestor::Linked[1]/@by"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="DataItem_Name">
      <xsl:choose>
        <xsl:when test="string-length(ancestor-or-self::DataItem[1]/@alias) > 0">
          <xsl:value-of select="ancestor-or-self::DataItem[1]/@alias"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="ancestor-or-self::DataItem[1]/@fisc_mapped"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>



    <xsl:variable name="Masterfield">
      <xsl:choose>
        <xsl:when test="ancestor::Link and @typefield != ''">
          <xsl:value-of select="$Typefield_String"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="$FieldParam != ''">
              <xsl:value-of select="$FieldParam"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="ancestor::Linked">
                  <xsl:value-of select="$DataItem_Name"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:if test="string-length(ancestor-or-self::DataItem[1]/@alias) > 0">
                    <xsl:value-of select="ancestor-or-self::DataItem[1]/@alias"/>
                  </xsl:if>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="count(ancestor::Linked) &gt; 0">
        <xsl:value-of select="concat(' field=', $Masterfield, '{', $Link_Field, '}', ' ')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$Masterfield != ''">
            <xsl:value-of select="concat(' field=', $Masterfield, ' ')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'#field#'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="calculateSlotNumber">
    <xsl:param name="FISC_CONSTANT_SLOT"/>

    <xsl:variable name="precedingFInit">
      <xsl:number count="child::*[DateRangeField or Sort]" level="any" from="ConnectionType"/>
    </xsl:variable>

    <xsl:variable name="precedingLinked_Sort">
      <xsl:number count="child::*//Linked[.//Sort]" level="any" from="ConnectionType"/>
    </xsl:variable>
    <xsl:variable name="precedingLinked_Date">
      <xsl:number count="child::*//Linked[.//DateRangeField]" level="any" from="ConnectionType"/>
    </xsl:variable>

    <xsl:variable name="preceding_All">
      <xsl:choose>
        <xsl:when test="number($precedingFInit)">
          <xsl:value-of select="$precedingFInit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="precedingLink_Sort">
      <xsl:choose>
        <xsl:when test="number($precedingLinked_Sort)">
          <xsl:value-of select="$precedingLinked_Sort"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="precedingLink_Date">
      <xsl:choose>
        <xsl:when test="number($precedingLinked_Date)">
          <xsl:value-of select="$precedingLinked_Date"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="combinedSlot">
      <xsl:value-of
        select="$preceding_All + $SecuritySlot - $precedingLink_Sort - $precedingLink_Date - 1"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($FISC_CONSTANT_SLOT) &gt; 0">
        <xsl:value-of select="$FISC_CONSTANT_SLOT"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$combinedSlot"/>    
      </xsl:otherwise>
    </xsl:choose>
    

  </xsl:template>

  <xsl:template name="Make_idfield_ui_line">
    <xsl:param name="Linked_By"/>

    <xsl:choose>
      <xsl:when test="$Linked_By = ''">
        <xsl:message terminate="yes">
          <xsl:text>by attribute is mandatory on Linked Node.</xsl:text>
        </xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of
          select="concat(',', $double_quotes, 'idfield', $double_quotes, ' : ', $double_quotes, $Linked_By, $double_quotes, $newLine)"
        />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="validate_one_dataitem_at_main_level">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$Current_dataItem = preceding::DataItem[@IsItMain = 'true']/@name">
        <xsl:if test="ancestor::DataItem[1]/@IsItMain = 'true'">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItems at Main Level cannot have duplicate names ', $Current_dataItem)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="use_subdoc_typefield">
    <xsl:choose>
      <xsl:when test="string-length(ancestor::DataItem[1]/@typefield) > 0">
        <xsl:value-of select="'subdoc_typefield'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="ancestor::DataItem[1]/@name"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="substring-after-last">
    <xsl:param name="input"/>
    <xsl:param name="substr"/>

    <!-- Extract the string which comes after the first occurence -->
    <xsl:variable name="temp" select="substring-after($input, $substr)"/>

    <xsl:choose>
      <!-- If it still contains the search string the recursively process -->
      <xsl:when test="$substr and contains($temp, $substr)">
        <xsl:call-template name="substring-after-last">
          <xsl:with-param name="input" select="$temp"/>
          <xsl:with-param name="substr" select="$substr"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$temp"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="processUniqueValues">
    <xsl:param name="delimitedValues"/>
    <xsl:param name="CalledFromWithin"/>
    <xsl:param name="Delimiter"/>
    <xsl:variable name="firstOne">
      <!-- variable firstOne: the first value in the delimited list of-->
      <xsl:value-of select="substring-before($delimitedValues, '~')"/>
    </xsl:variable>
    <xsl:variable name="firstOneDelimited">
      <!-- variable firstOneDelimited: the first value in the delimitedof items with the tilde "~" delimiter -->
      <xsl:value-of select="concat('~', substring-before($delimitedValues, '~'), '~')"/>
    </xsl:variable>
    <xsl:variable name="theRest">
      <!-- variable theRest: the rest of the delimited list after theone is removed -->
      <xsl:value-of select="substring-after($delimitedValues, '~')"/>
    </xsl:variable>
    <xsl:variable name="first_of_rest">
      <xsl:value-of select="substring-before($theRest, '~')"/>
    </xsl:variable>
    <xsl:variable name="last_of_rest">
      <xsl:call-template name="substring-after-last">
        <xsl:with-param name="input" select="$theRest"/>
        <xsl:with-param name="substr" select="'~'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <!-- when the current one exists again in the remaining list ANDfirst one isn't empty, -->
      <xsl:when
        test="(contains($theRest, $firstOneDelimited) or ($firstOne = $first_of_rest) or ($firstOne = $last_of_rest)) and not($firstOne = '')">
        <xsl:call-template name="processUniqueValues">
          <xsl:with-param name="delimitedValues" select="$theRest"/>
          <xsl:with-param name="CalledFromWithin" select="'YES'"/>
          <xsl:with-param name="Delimiter" select="$Delimiter"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <!-- Only put space if it is not first one -->
        <xsl:if test="normalize-space($firstOne) != '' and $CalledFromWithin = 'YES'">
          <xsl:value-of select="$Delimiter"/>
        </xsl:if>
        <!-- otherwise this is the last occurence in the list, so returnitem with a delimiter tilde "~". -->
        <xsl:value-of select="normalize-space($firstOne)" xml:space="preserve"/>

        <xsl:if test="contains($theRest, '~')">
          <!-- when there are more left in the delimited list, call thewith the remaining items -->
          <xsl:call-template name="processUniqueValues">
            <xsl:with-param name="delimitedValues" select="$theRest"/>
            <xsl:with-param name="CalledFromWithin" select="'YES'"/>
            <xsl:with-param name="Delimiter" select="$Delimiter"/>
          </xsl:call-template>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="strip-beginning-comma">
    <xsl:param name="text"/>
    <xsl:choose>
      <xsl:when test="not(substring($text, 1, 1) = ',')">
        <xsl:value-of select="$text"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="substring($text, 2)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
