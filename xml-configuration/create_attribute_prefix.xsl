<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text"/>
    <xsl:include href="frisk_xml_parser_utils.xsl"/>
    <xsl:include href="frisk_constants.xsl"/>

    <xsl:template match="DataSetConfig">
        <xsl:apply-templates select="DataSet"/>
    </xsl:template>

    <xsl:template match="DataSet">
        <xsl:for-each select="./descendant::*[name() = 'DataItem']">
            <xsl:if test="Security">                
                <xsl:value-of select="$FISC_SECURITY_PREFIX"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
