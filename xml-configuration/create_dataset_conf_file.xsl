<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:ext="http://exslt.org/common">

  <xsl:output method="text"/>
  <xsl:include href="frisk_xml_parser_utils.xsl"/>


  <xsl:key name="hdfs_items" match="DataItem[@fileSource = 'hdfs']/@name" use="."/>
  <xsl:key name="iodl_items" match="DataItem[@fileSource = 'iodl']/@name" use="."/>
  
  <xsl:template match="DataSetConfig">
    <xsl:apply-templates select="DataSet"/>
  </xsl:template>

  <xsl:template match="DataSet">
    <!-- Validate that dataExportTarget should be either hbase or file -->
    <xsl:call-template name="make_dataExportTarget"/>
    <xsl:value-of select="$newLine"/>

    <xsl:apply-templates select="ConnectionType"/>
  </xsl:template>
  
  <xsl:template match="ConnectionType">

    <xsl:apply-templates select="ODBC"/>
    <xsl:apply-templates select="FileShare"/>
    <xsl:apply-templates select="HDFSFileShare"/>
    <xsl:apply-templates select="EMAIL_EXCHANGE"/>
  </xsl:template>

  <xsl:template match="ODBC">

    <xsl:value-of
      select="concat('[', $odbc_header_dataset, '_', ancestor::DataSet[1]/@name, ']', $newLine)"/>
    <xsl:value-of select="concat('type=odbc', $newLine)"/>

    <xsl:call-template name="Make_iodl_hdfs_keys"/>

    <xsl:value-of select="concat('index_db=', ancestor::DataSet[1]/@db, $newLine)"/>

    <xsl:choose>
      <xsl:when test="count(descendant::Link) + count(descendant::Join) > 0">
        <xsl:value-of select="concat('index_append=1', $newLine)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('index_append=0', $newLine)"/>
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:choose>
      <xsl:when test="string-length(ancestor::DataSet[1]/@dataExportTarget) &gt; 0">
        <xsl:value-of select="concat('export=1',$newLine)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('export=0',$newLine)"/>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template match="FileShare">
    <xsl:value-of
      select="concat('[', $file_share_header_dataset, '_', ancestor::DataSet[1]/@name, ']', $newLine)"/>
    <xsl:value-of select="concat('type=file', $newLine)"/>
    <xsl:if test="@location != ''">
      <xsl:value-of select="concat('share_path=', @location, $newLine)"/>
      <xsl:value-of select="concat('share_url=', @location, $newLine)"/>
    </xsl:if>

    <xsl:value-of select="concat('index_db=', ancestor::DataSet[1]/@db, $newLine)"/>
  </xsl:template>

  <xsl:template match="HDFSFileShare">
    <xsl:value-of
      select="concat('[', $hdfs_share_header_dataset, '_', ancestor::DataSet[1]/@name, ']', $newLine)"/>
    <xsl:value-of select="concat('type=hdfs', $newLine)"/>
    <xsl:if test="@location != ''">
      <xsl:value-of select="concat('hdfs_path=', @location, $newLine)"/>
    </xsl:if>

    <xsl:value-of select="concat('index_db=', ancestor::DataSet[1]/@db, $newLine)"/>

  </xsl:template>

  <xsl:template name="Make_iodl_hdfs_keys">

    <xsl:variable name="unique_hdfs_keys">
      <xsl:for-each
        select="./descendant::*[name() = 'DataItem'][@fileSource = $source_hdfs_type]/@name[generate-id() = generate-id(key('hdfs_items', .)[1])]">
        <xsl:value-of select="."/>
        <xsl:if test="position() != last()">
          <xsl:value-of select="','"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$unique_hdfs_keys != ''">
        <xsl:value-of select="concat('hdfs_keys=', $unique_hdfs_keys, $newLine)"/>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="unique_iodl_keys">
      <xsl:for-each
        select="./descendant::*[name() = 'DataItem'][@fileSource = $source_iodl_type]/@name[generate-id() = generate-id(key('iodl_items', .)[1])]">
        <xsl:value-of select="."/>
        <xsl:if test="position() != last()">
          <xsl:value-of select="','"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$unique_iodl_keys != ''">
        <xsl:value-of select="concat('iodl_keys=', $unique_iodl_keys, $newLine)"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>
  
  <xsl:template name="make_dataExportTarget">
    <xsl:choose>
      <xsl:when test="string-length(@dataExportTarget) &gt; 0">
        <xsl:choose>
          <xsl:when test="translate(@dataExportTarget, $lowercase, $uppercase) = 'HBASE'">
            <xsl:call-template name="make_hdfs_section"/>  
          </xsl:when>
          <xsl:when test="translate(@dataExportTarget, $lowercase, $uppercase) = 'FILE'">
            <xsl:call-template name="make_file_section"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:value-of select="concat('dataExportTarget attribute value ', @dataExportTarget, ' not valid on DataSet Node ', newLine)"
              />              
            </xsl:message>            
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="make_hdfs_section">
    <xsl:value-of
      select="concat('[', $export_header_dataset, '_', ancestor-or-self::DataSet[1]/@name, ']', $newLine)"/>
    <xsl:value-of select="concat('export_type=hbase', $newLine)"/>
    
    <!-- Validate that value on dataExport should be raw or encoded -->
    <xsl:apply-templates select="./descendant::*[name() = 'DataExport']" mode="validate_hbase_values"/>
    
    <xsl:variable name="hdfs_keys">
      <xsl:apply-templates
        select="./descendant::*[name() = 'DataExport'][@key = 'true']" mode="keys"/>
    </xsl:variable>
    
    <xsl:variable name="hdfs_keys_Strip_last_comma">      
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$hdfs_keys"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:value-of select="concat('hbase_keys=',$hdfs_keys_Strip_last_comma,$newLine)"/>
    
    <xsl:variable name="hdfs_values">
      <xsl:apply-templates
        select="./descendant::*[name() = 'DataExport'][not(@key) or @key !='true']" mode="hdfs_values"/>
    </xsl:variable>
    
    <xsl:variable name="unique_hdfs_values">
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$hdfs_values"/>
      </xsl:call-template>  
    </xsl:variable>
    
        
    <xsl:variable name="hdfs_values_from_key">
      <xsl:apply-templates select="./descendant::*[name() = 'DataExport'][@key='true']" mode="hdfs_keys_as_value"/>       
    </xsl:variable>
    
    <xsl:variable name="unique_hdfs_values_key">
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$hdfs_values_from_key"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="hdfs_values_including_key">
      <xsl:choose>
        <xsl:when test="string-length($unique_hdfs_values_key) &gt; 0 and string-length($unique_hdfs_values) &gt; 0">
          <xsl:value-of select="concat($unique_hdfs_values,',',$unique_hdfs_values_key)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$unique_hdfs_values"/>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:variable>
    
    <xsl:variable name="hdfs_values_Strip_last_comma">
      <xsl:call-template name="strip-end-comma">
        <xsl:with-param name="text" select="$hdfs_values_including_key"/>
      </xsl:call-template>  
    </xsl:variable>

    <xsl:value-of select="concat('hbase_values=',$hdfs_values_Strip_last_comma,$newLine)"/>        
  </xsl:template>
    
  <xsl:template name="make_file_section">
    <xsl:value-of
      select="concat('[', $export_header_dataset, '_', ancestor-or-self::DataSet[1]/@name, ']', $newLine)"/>
    <xsl:value-of select="concat('export_type=file', $newLine)"/>
    
    <xsl:variable name="file_keys">
      <xsl:apply-templates
        select="./descendant::*[name() = 'DataExport'][@key = 'true']" mode="keys"/>
    </xsl:variable>
    
    <xsl:variable name="unique_file_keys">
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$file_keys"/>
      </xsl:call-template>  
    </xsl:variable>
           
    <xsl:value-of select="concat('file_keys=',$unique_file_keys,$newLine)"/>
    
    <!-- Validate that value on dataExport should be raw or encoded -->
    <xsl:apply-templates select="./descendant::*[name() = 'DataExport']" mode="validate_file_values"/>
    
    <xsl:variable name="file_values_raw">
      <xsl:apply-templates
        select="./descendant::*[name() = 'DataExport'][@value = 'raw']" mode="file_values_raw"/>
    </xsl:variable>
    
    <xsl:variable name="unique_file_values_raw">
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$file_values_raw"/>
      </xsl:call-template>  
    </xsl:variable>
        
    <xsl:value-of select="concat('file_values_raw=',$unique_file_values_raw,$newLine)"/>
    
    <xsl:variable name="file_values_encoded">
      <xsl:apply-templates
        select="./descendant::*[name() = 'DataExport'][@value = 'encoded']" mode="file_values_encoded"/>
    </xsl:variable>
    
    <xsl:variable name="unique_file_values_encoded">
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$file_values_encoded"/>
      </xsl:call-template>  
    </xsl:variable>
        
    <xsl:value-of select="concat('file_values_encoded=',$unique_file_values_encoded,$newLine)"/>
    
    <xsl:variable name="file_values_digest">
      <xsl:apply-templates select="./descendant::*[name() = 'DataExport'][@digest = 'true']" mode="file_values_digest"/>       
    </xsl:variable>
    
    <xsl:variable name="unique_file_values_digest">
      <xsl:call-template name="make_unique_remove_commas">
        <xsl:with-param name="delimited_input" select="$file_values_digest"/>
      </xsl:call-template>  
    </xsl:variable>
    
    <xsl:value-of select="concat('file_values_digest=',$unique_file_values_digest,$newLine)"/>
  </xsl:template>
  
  <xsl:template match="DataExport" mode="keys">    
    <!-- Convert name for eg fisc_extracted_ext to extract_text_extracted_text -->    
    <xsl:variable name="key_converted">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="ancestor::DataItem[1]/@name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="string-length($key_converted) &gt; 0">
      <xsl:value-of select="concat($key_converted,'~')"/>
    </xsl:if>        
  </xsl:template>
  
  <xsl:template match="DataExport" mode="validate_hbase_values">
    <xsl:if test="string-length(@value) &gt; 0">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('value attribute is invalid for exportType  hbase. ',$newLine)"/>
      </xsl:message>  
    </xsl:if>
    <xsl:if test="@digest = 'true'">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('digest attribute cannot be true for exportType  hbase. ',$newLine)"/>
      </xsl:message>  
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="DataExport" mode="file_values_raw">
    <!-- Convert name for eg fisc_extracted_ext to extract_text_extracted_text -->
    <xsl:variable name="key_converted">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="ancestor::DataItem[1]/@name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="string-length($key_converted) &gt; 0">
      <xsl:value-of select="concat($key_converted,'~')"/>
    </xsl:if>    
  </xsl:template>
  
  <xsl:template match="DataExport" mode="file_values_digest">
    <!-- Convert name for eg fisc_extracted_ext to extract_text_extracted_text -->
    <xsl:variable name="key_converted">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="ancestor::DataItem[1]/@name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="string-length($key_converted) &gt; 0">
      <xsl:value-of select="concat($key_converted,'~')"/>
    </xsl:if>    
  </xsl:template>
  
  <xsl:template match="DataExport" mode="file_values_encoded">
    <!-- Convert name for eg fisc_extracted_ext to extract_text_extracted_text -->
    <xsl:variable name="key_converted">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="ancestor::DataItem[1]/@name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="string-length($key_converted) &gt; 0">
      <xsl:value-of select="concat($key_converted,'~')"/>
    </xsl:if>    
  </xsl:template>
  
  <xsl:template match="DataExport" mode="validate_file_values">
    
    <xsl:choose>
      <xsl:when test="@key='true'">
        <xsl:if test="string-length(@value) &gt; 0">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat('attribute value ',@value,' should not be present when key is true. ',$newLine)"/>
          </xsl:message>          
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="string-length(@value) &gt; 0">
          <xsl:choose>
            <xsl:when test="@value='raw' or @value='encoded'"/>
            <xsl:otherwise>
              <xsl:message terminate="yes">
                <xsl:value-of select="concat('attribute value ',@value,' is incorrect.It should be either raw or encoded. ',$newLine)"/>
              </xsl:message>  
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>       
      </xsl:otherwise>
    </xsl:choose>
    
    <xsl:if test="string-length(@columnGroup) &gt; 0">
      <xsl:message terminate="yes">
        <xsl:value-of select="concat('DataExport attribute columnGroup not valid for ExportType file for DataItem ',ancestor::DataItem[1]/@name,$newLine)"/>
      </xsl:message>
    </xsl:if>    
  </xsl:template>
  
  <xsl:template match="DataExport" mode="hdfs_values">
    <!-- Convert name for eg fisc_extracted_ext to extract_text_extracted_text -->
    <xsl:variable name="key_converted">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="ancestor::DataItem[1]/@name"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length(@columnGroup) &gt; 0">
        <xsl:value-of select="concat(@columnGroup,':',$key_converted,'~')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('extracted:',$key_converted,'~')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="DataExport" mode="hdfs_keys_as_value">
    <!-- Convert name for eg fisc_extracted_ext to extract_text_extracted_text -->
    <xsl:variable name="key_converted">
      <xsl:call-template name="fisc_to_metadata_mapping">
        <xsl:with-param name="LineNamePre" select="ancestor::DataItem[1]/@name"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="string-length(@columnGroup) &gt; 0">
        <xsl:value-of select="concat(@columnGroup,':',$key_converted,'~')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('extracted:',$key_converted,'~')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="make_unique_remove_commas">
    <xsl:param name="delimited_input"/>
    <xsl:variable name="Unique_values">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$delimited_input"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="','"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="values_without_begin_comma">
      <xsl:call-template name="strip-beginning-comma">
        <xsl:with-param name="text" select="$Unique_values"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="values_Strip_last_comma">
      <xsl:call-template name="strip-end-comma">
        <xsl:with-param name="text" select="$values_without_begin_comma"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:value-of select="$values_Strip_last_comma"/>
    
  </xsl:template> 

</xsl:stylesheet>
