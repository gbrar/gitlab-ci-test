<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" omit-xml-declaration="yes"/>
    <xsl:include href="frisk_xml_parser_utils.xsl"/>
   
   
    <xsl:template name="Where">
        <xsl:param name="nodePosition"/>
        <xsl:param name="bracket_begin"/>
        <xsl:param name="bracket_end"/>

        <xsl:variable name="alias_query">
            <xsl:if test="$nodePosition = 1">
                <xsl:choose>
                    <xsl:when test="ancestor::Query[1]/@alias != ''">
                        <xsl:value-of select="ancestor::Query[1]/@alias"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="ancestor::Query[1]/@table"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="$nodePosition > 1">
                <xsl:choose>
                    <xsl:when test="ancestor::Join[2]/@alias != ''">
                        <xsl:value-of select="ancestor::Join[2]/@alias"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="ancestor::Join[2]/@table"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </xsl:variable>


        <xsl:variable name="table_join">
            <xsl:choose>
                <xsl:when test="ancestor::Join[1]/@alias != ''">
                    <xsl:value-of select="ancestor::Join[1]/@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::Join[1]/@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="parent_alias_name">
            <xsl:choose>
                <xsl:when test="ancestor::DataItem[1]/@alias != ''">
                    <xsl:value-of select="ancestor::DataItem[1]/@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::DataItem[1]/@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fromClause">
            <xsl:value-of
                select="concat(' from ', ancestor::Query[1]/@table, ' ', ancestor::Query[1]/@alias, ',', ancestor::Join[1]/@table)"
            />
        </xsl:variable>

        <xsl:variable name="firsthalfsql">
            <xsl:choose>
                <xsl:when test="@parent != ''">
                    <xsl:value-of select="concat($alias_query, '.', @parent)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($table_join, '.', @child)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="condition_clause">
            <xsl:choose>
                <xsl:when test="@condition = 'eq'">
                    <xsl:text disable-output-escaping="yes">=</xsl:text>
                </xsl:when>
                <xsl:when test="@condition = 'gt'">
                    <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
                </xsl:when>
                <xsl:when test="@condition = 'lt'">
                    <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
                </xsl:when>
                <xsl:when test="@condition = 'le'">
                    <xsl:text disable-output-escaping="yes">&lt;</xsl:text>
                    <xsl:text>=</xsl:text>
                </xsl:when>
                <xsl:when test="@condition = 'ge'">
                    <xsl:text disable-output-escaping="yes">&gt;</xsl:text>
                    <xsl:text>=</xsl:text>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="ifwhereclausereq">
            <xsl:choose>
                <xsl:when test="$condition_clause != ''">
                    <xsl:value-of select="' where '"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="secondhalfsql">
            <xsl:choose>
                <xsl:when test="@absolute != ''">
                    <xsl:choose>
                        <xsl:when test="@data_type = 'string'">
                            <xsl:text>'</xsl:text>
                            <xsl:value-of select="@absolute"/>
                            <xsl:text>'</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@absolute"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($table_join, '.', @child)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>


        <xsl:variable name="which_cond">
            <xsl:choose>
                <xsl:when test="name(parent::*) = 'OR'">
                    <xsl:value-of select="' OR '"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="' AND '"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>


        <xsl:choose>
            <xsl:when test="$nodePosition = 1">
                <xsl:value-of
                    select="concat($ifwhereclausereq, $firsthalfsql, $condition_clause, $secondhalfsql)"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of
                    select="concat($which_cond, $bracket_begin, $firsthalfsql, $condition_clause, $secondhalfsql, $bracket_end)"
                />
            </xsl:otherwise>
        </xsl:choose>


    </xsl:template>

    <xsl:template match="DataItem" mode="JoinsOnly">
        <xsl:apply-templates select="Join" mode="NodeByNode"/>
    </xsl:template>

    <xsl:template match="DataItem/Join" mode="NodeByNode">
        <xsl:variable name="alias_parent_query">
            <xsl:choose>
                <xsl:when test="ancestor::Query[1]/@alias != ''">
                    <xsl:value-of
                        select="concat(ancestor::Query[1]/@alias, '.', ancestor::DataItem[1]/@name)"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::Query[1]/@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="table_parent_query">
            <xsl:choose>
                <xsl:when test="ancestor::Query[1]/@table != ''">
                    <xsl:value-of select="ancestor::Query[1]/@table"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="dataitem_alias">
            <xsl:choose>
                <xsl:when test="ancestor::DataItem[1]/@alias">
                    <xsl:value-of select="ancestor::DataItem[1]/@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::DataItem[1]/@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="Query_Alias">
            <xsl:choose>
                <xsl:when test="ancestor::Query[1]/@alias != ''">
                    <xsl:value-of
                        select="concat(ancestor::Query[1]/@table, ' ', ancestor::Query[1]/@alias)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::Query[1]/@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>


        <xsl:value-of select="concat('SELECT ', $alias_parent_query, ' as ', $dataitem_alias)"/>

        <xsl:for-each select="descendant-or-self::*[name() = 'DataItem']">
            <xsl:call-template name="GetDataItemVals"/>
        </xsl:for-each>

        <xsl:variable name="asAlias">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="concat(@table, ' ', @alias)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:value-of select="concat(' from ', $Query_Alias, ',', $asAlias)"/>
        <xsl:apply-templates select="Join" mode="Nested"/>


        <xsl:for-each select="child::*">

            <xsl:if test="name() = 'Join'">
                <xsl:call-template name="Joined"/>
            </xsl:if>


            <xsl:if test="name() = 'Where'">
                <xsl:call-template name="Where">
                    <xsl:with-param name="nodePosition">
                        <xsl:value-of select="count(ancestor::Join | ancestor::AND | ancestor::OR)"
                        />
                    </xsl:with-param>
                    <xsl:with-param name="bracket_begin">
                        <xsl:value-of select="''"/>
                    </xsl:with-param>
                    <xsl:with-param name="bracket_end">
                        <xsl:value-of select="''"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>

            <xsl:if test="name() = 'AND'">
                <xsl:call-template name="AND"/>
            </xsl:if>

            <xsl:if test="name() = 'OR'">
                <xsl:call-template name="OR"/>
            </xsl:if>

        </xsl:for-each>
        <xsl:value-of select="concat(';',$newLine)"/>
    </xsl:template>

    <xsl:template name="Joined">
        <xsl:for-each select="child::node()">
            <xsl:if test="name() = 'Where'">
                <xsl:call-template name="Where">
                    <xsl:with-param name="nodePosition">
                        <xsl:value-of select="count(ancestor::Join | ancestor::AND | ancestor::OR)"
                        />
                    </xsl:with-param>
                    <xsl:with-param name="bracket_begin">
                        <xsl:value-of select="''"/>
                    </xsl:with-param>
                    <xsl:with-param name="bracket_end">
                        <xsl:value-of select="''"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="name() = 'Join'">
                <xsl:call-template name="Joined"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="AND">
        <xsl:variable name="bracketval_begin">
            <xsl:choose>
                <xsl:when test="count(descendant::Where) > 1 and name(parent::*) = 'Join'">
                    <xsl:value-of select="'('"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="bracketval_end">
            <xsl:choose>
                <xsl:when
                    test="count(ancestor-or-self::OR | ancestor-or-self::AND) > 1 and (count(following-sibling::*) = 0)">
                    <xsl:value-of select="')'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:for-each select="child::*">
            <xsl:if test="name() = 'Where'">
                <xsl:call-template name="Where">
                    <xsl:with-param name="nodePosition">
                        <xsl:value-of select="count(ancestor::Join | ancestor::AND | ancestor::OR)"/>
                        <!-- <xsl:number  from="DataItem" count="Join/Where" level="single" /> -->
                    </xsl:with-param>
                    <xsl:with-param name="bracket_begin">
                        <xsl:value-of select="$bracketval_begin"/>
                    </xsl:with-param>
                    <xsl:with-param name="bracket_end">
                        <xsl:value-of select="$bracketval_end"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="name() = 'OR'">
                <xsl:call-template name="OR"/>
            </xsl:if>
            <xsl:if test="name() = 'AND'">
                <xsl:call-template name="OR"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="OR">
        <xsl:variable name="bracketval_begin">
            <xsl:choose>
                <xsl:when test="count(descendant::Where) > 1 and name(parent::*) = 'Join'">
                    <xsl:value-of select="'('"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="bracketval_end">
            <xsl:choose>
                <xsl:when
                    test="count(ancestor-or-self::OR | ancestor-or-self::AND) > 1 and (count(following-sibling::*) = 0)">
                    <xsl:value-of select="')'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:for-each select="child::*">
            <xsl:if test="name() = 'Where'">
                <xsl:call-template name="Where">
                    <xsl:with-param name="nodePosition">
                        <xsl:value-of select="count(ancestor::Join | ancestor::AND | ancestor::OR)"
                        />
                    </xsl:with-param>
                    <xsl:with-param name="bracket_begin">
                        <xsl:value-of select="$bracketval_begin"/>
                    </xsl:with-param>
                    <xsl:with-param name="bracket_end">
                        <xsl:value-of select="$bracketval_end"/>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="name() = 'OR'">
                <xsl:call-template name="OR"/>
            </xsl:if>
            <xsl:if test="name() = 'AND'">
                <xsl:call-template name="AND"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>



    <xsl:template match="Join" mode="Nested">
        <xsl:variable name="asAlias">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="concat(@table, ' ', @alias)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:value-of select="concat(',', $asAlias)"/>
        <xsl:apply-templates select="Join" mode="Nested"/>
    </xsl:template>

    <xsl:template name="GetDataItemVals">
        <xsl:variable name="join_table">
            <xsl:choose>
                <xsl:when test="ancestor::Join[1]/@table != ''">
                    <xsl:value-of select="ancestor::Join[1]/@table"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="join_alias">
            <xsl:choose>
                <xsl:when test="ancestor::Join[1]/@alias != ''">
                    <xsl:value-of select="ancestor::Join[1]/@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::Join[1]/@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="asAlias">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="concat('.', @name, ' as ', @alias)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('.', @name)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat(',', $join_alias, $asAlias)"/>
    </xsl:template>

    <xsl:template match="Link" mode="Linked">


        <xsl:variable name="parent_alias_name">
            <xsl:choose>
                <xsl:when test="../@alias">
                    <xsl:value-of select="../@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="../@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="useas">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="Link_column">
            <xsl:value-of select="concat($useas, '.', @key_name, ' as ', $parent_alias_name)"/>
        </xsl:variable>

        <xsl:variable name="AllColsUnderDataItem">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="DataItem" mode="AllDataItemsUnderQuery">
                <xsl:with-param name="tableAlias" select="$useas"/>
                <xsl:with-param name="table" select="@table"/>
            </xsl:apply-templates>
        </xsl:variable>
        <!-- remove last comma if it exists -->
        <xsl:variable name="AllColsStripComma">
            <xsl:call-template name="strip-end-characters">
                <xsl:with-param name="text" select="$AllColsUnderDataItem"/>
                <xsl:with-param name="strip-count" select="1"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of
            select="concat('SELECT ', $Link_column, $AllColsStripComma, ' FROM ', @table, ' ', @alias, ';&#10;')"
        />
    </xsl:template>

    <xsl:template match="DataItem" mode="OtherChilds">
        <xsl:param name="table_alias"/>

        <xsl:variable name="useas">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($table_alias, '.', @name, ' as ', $useas)"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="DataItem" mode="OtherChilds">
            <xsl:with-param name="table_alias" select="$table_alias"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="DataItem" mode="AllDataItemsUnderQuery">
        <xsl:param name="tableAlias"/>
        <xsl:param name="table"/>
        <xsl:variable name="useas">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="concat($tableAlias, '.', @name, ' as ', $useas)"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="DataItem" mode="OtherChilds">
            <xsl:with-param name="table_alias" select="$tableAlias"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="DataItem" mode="LinksOnly">
        <xsl:apply-templates select="Link" mode="Linked"/>
    </xsl:template>

    <xsl:template match="DataItem" mode="SiblingsOnly">

        <xsl:variable name="alias_parent">
            <xsl:choose>
                <xsl:when test="ancestor::Query[1]/@alias != ''">
                    <xsl:value-of select="ancestor::Query[1]/@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="ancestor::Query[1]/@table"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="table_parent">
            <xsl:choose>
                <xsl:when test="ancestor::Query[1]/@table != ''">
                    <xsl:value-of select="ancestor::Query[1]/@table"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="useas">
            <xsl:choose>
                <xsl:when test="@alias != ''">
                    <xsl:value-of select="@alias"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="position() = 1">
            <xsl:text>SELECT </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat($alias_parent, '.', @name, ' as ', $useas)"/>
        <xsl:if test="position() != last()">
            <xsl:text>,</xsl:text>
            <xsl:value-of select="$newLine"/>
        </xsl:if>
        <xsl:if test="position() = last()">
            <xsl:value-of select="concat(' FROM ', $table_parent, ' ', $alias_parent, ';&#10;')"/>
        </xsl:if>
    </xsl:template>


    <xsl:template match="Query">
        <xsl:apply-templates select="DataItem" mode="SiblingsOnly"/>

        <!-- Only process if it has Link Column-->
        <xsl:apply-templates select="DataItem" mode="LinksOnly"/>

        <xsl:apply-templates select="DataItem" mode="JoinsOnly"/>
    </xsl:template>
    
    <xsl:template match="FileShare">
        <xsl:apply-templates select="Query"/>
    </xsl:template>
    
    <xsl:template match="Sql">
        <xsl:apply-templates select="Query"/>
    </xsl:template> 
    
    <xsl:template match="ConnectionType">
        <xsl:apply-templates select="ODBC"/>
        <xsl:apply-templates select="FileShare"/>
        <xsl:apply-templates select="Sql"/>
    </xsl:template>

    <xsl:template match="ODBC">
        <xsl:apply-templates select="Query"/>
    </xsl:template>

    <xsl:template match="DataSetConfig/DataSet">
        <xsl:apply-templates select="ConnectionType"/>
    </xsl:template>

</xsl:stylesheet>
