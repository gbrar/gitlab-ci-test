<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
  <xsl:import href="frisk_xml_parser_utils.xsl"/>
  <xsl:import href="frisk_constants.xsl"/>
  
  <xsl:template name="ProcessSubdocMultilines_Preview">        
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem'][Preview]" mode="Shown_Label_Space">
      <xsl:with-param name="IsItMain" select="false()"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template name="ProcessSubdocMultilines">        
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="Shown_Label_Space">
      <xsl:with-param name="IsItMain" select="false()"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template name="ProcessSubdocURL">        
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="URL">
      <xsl:with-param name="IsItMain" select="false()"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template name="ProcessSubdocImageViewer">        
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="ImageViewer">
      <xsl:with-param name="IsItMain" select="false()"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="metadata_prefixes">
    <xsl:variable name="MetadataInit">
      <xsl:number format="1" count="DataItem[@metadata = 'true']" level="any" from="ConnectionType"
      />
    </xsl:variable>
    
    <xsl:variable name="MetadataNum">
      <xsl:choose>
        <xsl:when test="number($MetadataInit)">
          <xsl:value-of select="$MetadataInit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="IncNumber">
      <xsl:value-of select="$NumberNode + $MetadataNum - 1"/>
    </xsl:variable>
    
    <xsl:variable name="NodeLetters">
      <xsl:number value="$IncNumber" format="A"/>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant">
      <xsl:value-of select="@fiscConstantPrefix"/>
    </xsl:variable>
    
    <xsl:variable name="fisc_constant_value">
      <xsl:if test="string-length($fisc_constant) &gt; 0">
        <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key=$fisc_constant]"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($fisc_constant) &gt; 0">
        <xsl:if test="string-length($fisc_constant_value) &lt; 1">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(@name,' has constant which is not defined',$newLine)"/>
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
    
    <xsl:choose>
      <xsl:when test="string-length($fisc_constant_value) &gt; 0">
        <xsl:value-of select="concat('~',$fisc_constant_value,'~')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('~', 'XM', $NodeLetters, '~')"/>    
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="ProcessNormalLink">
    <xsl:if test="./child::*[name() = 'Display'] or @Icon_Flag='true'">
      <xsl:if test="count(Display/Sample) &lt; 1 and @name != 'fisc_file_size'">                    
        <xsl:call-template name="check_sub_fields_to_include"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="ProcessPreviewNormalLink">
    <xsl:if test="./child::*[name() = 'Preview'] or @Icon_Flag='true'">
      <xsl:if test="@name != 'fisc_file_size'">
        <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="ProcessNormalLink">
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem' and string-length(@split) &lt; 1 and @type != 'date']" mode="ProcessNormalLink"/> 
  </xsl:template>
  
  <xsl:template name="ProcessPreviewNormalLink">
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem' and string-length(@split) &lt; 1 and @type != 'date']" mode="ProcessPreviewNormalLink"/>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="ProcessImageViewerLink">
    <xsl:variable name="count_Link_URL" >
      <xsl:call-template name="count_URL_Link"/>
    </xsl:variable>
    <xsl:if test="$count_Link_URL &gt; 0 and @fileSource = 'iodl'">        
      <xsl:value-of select="concat('~',@final_shown_label, '~')"/>        
    </xsl:if>
  </xsl:template>
  
  
  <xsl:template name="ProcessImageViewerLink">
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="ProcessImageViewerLink"/>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="ProcessURLLink">
    <xsl:variable name="count_Link_URL" >
      <xsl:call-template name="count_URL_Link"/>
    </xsl:variable>
    <xsl:variable name="IsitImageViewer">
      <xsl:choose>
        <xsl:when test="@fileSource='iodl'">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="$count_Link_URL &gt; 0 and $IsitImageViewer='false'">        
      <xsl:value-of select="concat('~',@final_shown_label, '~')"/>        
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="ProcessURLLink">
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="ProcessURLLink"/>
  </xsl:template>
  
  <xsl:template name="make_subdoc_normal_fields">
    <xsl:param name="db"/>
    <xsl:param name="Count_Links"/>
    <xsl:param name="Search_Type"/>
    
    <xsl:if test="$Count_Links &gt; 0">
      <xsl:for-each select="./descendant::*[name() = 'Link']">
        <xsl:variable name="prefix_val">
          <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
        </xsl:variable>
        <xsl:variable name="Fields_for_link">
          <xsl:choose>
            <xsl:when test="$Search_Type = 'Search'">
              <xsl:call-template name="ProcessNormalLink"/>
            </xsl:when>
            <xsl:when test="$Search_Type = 'Preview'">
              <xsl:call-template name="ProcessPreviewNormalLink"/>
            </xsl:when>
            <xsl:when test="$Search_Type = 'ImageViewer'">
              <xsl:call-template name="ProcessImageViewerLink"/>
            </xsl:when>
            <xsl:when test="$Search_Type = 'URL'">
              <xsl:call-template name="ProcessURLLink"/>
            </xsl:when>
          </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="Stripped_fields_link">
          <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$Fields_for_link"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>
        
        
        <xsl:variable name="Num_subdoc_variables">
          <xsl:call-template name="GetNoOfOccurance">
            <xsl:with-param name="String" select="$Stripped_fields_link"/>
            <xsl:with-param name="SubString" select="concat('$opt{', $subdoc_variable_name, ':', $db,'}')"/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:choose>
          <xsl:when test="$Search_Type = 'ImageViewer' or $Search_Type = 'URL'">
            <xsl:if test="normalize-space($Stripped_fields_link) != ''">
              <xsl:value-of
                select="concat('$set{', $Search_Type, ':', $prefix_val, '-Normal', ':', $db, ',$split{', normalize-space($Stripped_fields_link), '}}', $newLine)"
              />
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="normalize-space($Stripped_fields_link) != ''">
              <xsl:choose>
                <xsl:when test="$Num_subdoc_variables &gt; 0">
                  <xsl:value-of
                    select="concat('$set{', $Search_Type, ':', $prefix_val, '-Normal', ':', $db, ',$split{', normalize-space($Stripped_fields_link), '}}', $newLine)"
                  />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of
                    select="concat('$set{', $Search_Type, ':', $prefix_val, '-Normal', ':', $db, ',$split{', normalize-space($Stripped_fields_link), ' $opt{', $subdoc_variable_name, ':', $db, '}}}', $newLine)"
                  />        
                </xsl:otherwise>
              </xsl:choose>          
            </xsl:if>    
          </xsl:otherwise>
        </xsl:choose>       
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="make_subdoc_multilines">
    <xsl:param name="db"/>
    <xsl:param name="Count_Links"/>
    <xsl:param name="Search_Type"/>
    
    <xsl:if test="$Count_Links > 0">
      <xsl:for-each select="./descendant::*[name() = 'Link']">
        <xsl:variable name="prefix_val">
          <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
        </xsl:variable>
        <xsl:variable name="Fields_for_link">
          <xsl:choose>
            <xsl:when test="$Search_Type = 'Search'">
              <xsl:call-template name="ProcessSubdocMultilines"/>
            </xsl:when>
            <xsl:when test="$Search_Type = 'Preview'">
              <xsl:call-template name="ProcessSubdocMultilines_Preview"/>
            </xsl:when>            
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="Stripped_fields_link">
          <xsl:call-template name="processUniqueValues" xml:space="preserve">
            <xsl:with-param name="delimitedValues" select="$Fields_for_link"/>
            <xsl:with-param name="CalledFromWithin" select="'NO'"/>
            <xsl:with-param name="Delimiter" select="' '"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:if test="normalize-space($Stripped_fields_link) != ''">
          <xsl:value-of
            select="concat('$set{', $Search_Type, ':', $prefix_val, '-Multi', ':', $db, ',$split{', normalize-space($Stripped_fields_link), '}}', $newLine)"
          />
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="main_multilines_search">
    <xsl:param name="db"/>
    
    <xsl:variable name="Fields_normal_multilines">
      <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="Shown_Label_Space">
        <xsl:with-param name="IsItMain" select="true()"/>
      </xsl:apply-templates>      
    </xsl:variable>
    <xsl:variable name="Stripped_fields_link">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$Fields_normal_multilines"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="' '"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="normalize-space($Stripped_fields_link) != ''">
      <xsl:value-of
        select="concat('$set{Search:Multi:', $db, ',$split{', normalize-space($Stripped_fields_link), '}}', $newLine)"
      />
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="main_imageviewer_ImageViewer">
    <xsl:param name="db"/>
    
    <xsl:variable name="Fields_ImageViewer_main">
      <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="ImageViewer">
        <xsl:with-param name="IsItMain" select="true()"/>
      </xsl:apply-templates>
    </xsl:variable>
    <xsl:variable name="Stripped_ImageViewer_main">
      <xsl:call-template name="strip-end-characters">
        <xsl:with-param name="text" select="$Fields_ImageViewer_main"/>
        <xsl:with-param name="strip-count" select="1"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="normalize-space($Stripped_ImageViewer_main) != ''">
      <xsl:value-of
        select="concat('$set{ImageViewer:Multi:', $db, ',$split{', $Stripped_ImageViewer_main, '}}', $newLine)"
      />
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="main_multilines_preview">
    <xsl:param name="db"/>
    <xsl:variable name="Fields_preview_multilines">            
      <xsl:apply-templates select="./descendant::*[name() = 'DataItem'][Preview]" mode="Shown_Label_Space">
        <xsl:with-param name="IsItMain" select="true()"/>
      </xsl:apply-templates>
    </xsl:variable>
    
    <xsl:variable name="Stripped_preview_link">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$Fields_preview_multilines"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="' '"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="normalize-space($Stripped_preview_link) != ''">
      <xsl:value-of
        select="concat('$set{Preview:Multi:', $db, ',$split{', normalize-space($Stripped_preview_link), '}}', $newLine)"
      />
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="main_url_searchtype">
    <xsl:param name="db"/>
    
    <xsl:variable name="Fields_URL_multilines">
      <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="URL">
        <xsl:with-param name="IsItMain" select="true()"/>
      </xsl:apply-templates>
    </xsl:variable>
    <xsl:variable name="Stripped_URL_multilines">
      <xsl:call-template name="strip-end-characters">
        <xsl:with-param name="text" select="$Fields_URL_multilines"/>
        <xsl:with-param name="strip-count" select="1"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="normalize-space($Stripped_URL_multilines) != ''">
      <xsl:value-of
        select="concat('$set{URL:Multi:', $db, ',$split{', $Stripped_URL_multilines, '}}', $newLine)"
      />
    </xsl:if>
    
  </xsl:template>
  
  <xsl:template match="DataItem" mode="ImageViewer">
    <xsl:param name="IsItMain"/>
    
    <xsl:variable name="count_Main_URL" >
      <xsl:call-template name="count_URL_Main"/>
    </xsl:variable>
    
    <xsl:variable name="IsitImageViewer">
      <xsl:choose>
        <xsl:when test="@fileSource='iodl' and $count_Main_URL &gt; 0">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$IsItMain='true'">
        <xsl:if test="not(ancestor::Link)">
          <xsl:if test="$IsitImageViewer='true'">
            <xsl:value-of select="concat(@final_shown_label, ' ')"/>
          </xsl:if>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="ancestor::Link">
          <xsl:if test="$IsitImageViewer='true'">
            <xsl:value-of select="concat(@final_shown_label, ' ')"/>
          </xsl:if>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template name="count_URL_Main">
    <xsl:variable name="count_Main_Display_URL">
      <xsl:value-of select="count(Display/URL[not(ancestor::*[name() = 'Link'])])"/>
    </xsl:variable>
    
    <xsl:variable name="count_Main_Preview_URL">
      <xsl:value-of select="count(Preview/URL[not(ancestor::*[name() = 'Link'])])"/>
    </xsl:variable>
    
    <xsl:variable name="count_Main_URL" select="$count_Main_Display_URL + $count_Main_Preview_URL"/>
    
    <xsl:value-of select="$count_Main_URL"/>
  </xsl:template>
  
  <xsl:template name="count_URL_Link">
    <xsl:variable name="count_Link_Display_URL">
      <xsl:value-of select="count(Display/URL[ancestor::*[name() = 'Link']])"/>
    </xsl:variable>
    
    <xsl:variable name="count_Link_Preview_URL">
      <xsl:value-of select="count(Preview/URL[ancestor::*[name() = 'Link']])"/>
    </xsl:variable>
    
    <xsl:variable name="count_Link_URL" select="$count_Link_Display_URL + $count_Link_Preview_URL"/>
    
    <xsl:value-of select="$count_Link_URL"/>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="URL">
    <xsl:param name="IsItMain"/>
        
    <xsl:variable name="count_Main_URL" >
      <xsl:call-template name="count_URL_Main"/>
    </xsl:variable>
    
    <xsl:variable name="IsIt_iodl_source">
      <xsl:choose>
        <xsl:when test="@fileSource='iodl'">
          <xsl:value-of select="true()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="false()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$IsItMain='true'">
        <xsl:if test="not(ancestor::Link)">
          <xsl:if test="$count_Main_URL &gt; 0 and $IsIt_iodl_source='false'">
            <xsl:value-of select="concat(@final_shown_label, ' ')"/>
          </xsl:if>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="ancestor::Link">
          <xsl:if test="$count_Main_URL &gt; 0 and $IsIt_iodl_source='false'">
            <xsl:value-of select="concat(@final_shown_label, ' ')"/>
          </xsl:if>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template match="DataItem" mode="Shown_Label_Space">   
    <xsl:param name="IsItMain"/>
    <xsl:choose>
      <xsl:when test="$IsItMain='true'">
        <xsl:if test="not(ancestor::Link)">
          <xsl:if test="string-length(@split) > 0">
            <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
          </xsl:if>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="ancestor::Link">
          <xsl:if test="string-length(@split) > 0">
            <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
          </xsl:if>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template name="make_all_subdoc_normal">
    <xsl:param name="db"/>
    <xsl:param name="Count_Links"/>
    
    <xsl:call-template name="make_subdoc_normal_fields">
      <xsl:with-param name="db" select="$db"/>
      <xsl:with-param name="Count_Links" select="$Count_Links"/>
      <xsl:with-param name="Search_Type" select="'Search'"/>
    </xsl:call-template>
    
    <xsl:call-template name="make_subdoc_normal_fields">
      <xsl:with-param name="db" select="$db"/>
      <xsl:with-param name="Count_Links" select="$Count_Links"/>
      <xsl:with-param name="Search_Type" select="'Preview'"/>
    </xsl:call-template>
    
    <xsl:call-template name="make_subdoc_normal_fields">
      <xsl:with-param name="db" select="$db"/>
      <xsl:with-param name="Count_Links" select="$Count_Links"/>
      <xsl:with-param name="Search_Type" select="'ImageViewer'"/>
    </xsl:call-template>
    
    <xsl:call-template name="make_subdoc_normal_fields">
      <xsl:with-param name="db" select="$db"/>
      <xsl:with-param name="Count_Links" select="$Count_Links"/>
      <xsl:with-param name="Search_Type" select="'URL'"/>
    </xsl:call-template>
    
  </xsl:template>
  
  <xsl:template name="make_all_multilines_subdoc">
    <xsl:param name="db"/>
    <xsl:param name="Count_Links"/>    
    
    <xsl:call-template name="make_subdoc_multilines">
      <xsl:with-param name="db" select="$db"/>
      <xsl:with-param name="Count_Links" select="$Count_Links"/>
      <xsl:with-param name="Search_Type" select="'Search'"/>
    </xsl:call-template>
    
    <xsl:call-template name="make_subdoc_multilines">
      <xsl:with-param name="db" select="$db"/>
      <xsl:with-param name="Count_Links" select="$Count_Links"/>
      <xsl:with-param name="Search_Type" select="'Preview'"/>
    </xsl:call-template>   
    
  </xsl:template>
  
  <!-- Common Function to check for sub fields
    Presence of these sub fields will make this DataItem to be included
    in list of shown fields by UI-->    
  <xsl:template name="check_sub_fields_to_include">
    <xsl:choose>
      <xsl:when test="count(descendant::Caption) &gt; 0">
        <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
      </xsl:when>
      <xsl:when test="count(descendant::URL) &gt; 0">
        <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
      </xsl:when>      
      <xsl:when
        test="count(descendant::Metadata) &gt; 0 or count(descendant::Details) &gt; 0">
        <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
      </xsl:when>
      <xsl:when test="count(descendant::Highlight) &gt; 0">
        <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
      </xsl:when>
      <xsl:when test="@Icon_Flag='true'">
        <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="make_normal_Main_fields">
    <xsl:param name="db"/>
    
    <xsl:variable name="delimitedShownNonDate">
      <xsl:call-template name="Delimited_Shown_Main_Non_Date"/>
    </xsl:variable>
    <xsl:variable name="AllUniqShownNonDate">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$delimitedShownNonDate"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="' '"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$AllUniqShownNonDate != ''">
      <xsl:value-of
        select="concat('$set{Search:Normal:', $db, ',$split{', normalize-space($AllUniqShownNonDate), '}}', $newLine)"
      />
    </xsl:if>  
  </xsl:template>
  
  <xsl:template name="make_normal_URL_fields">
    <xsl:param name="db"/>
    
    <xsl:variable name="delimitedShownURL">
      <xsl:call-template name="Delimited_Shown_Main_URL"/>
    </xsl:variable>
    
    <xsl:variable name="AllUniqShownURL">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$delimitedShownURL"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="$AllUniqShownURL != ''">
      <xsl:value-of
        select="concat('$set{URL:Parent-Normal:', $db, ',$split{', normalize-space($AllUniqShownURL), '}}', $newLine)"
      />
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="make_normal_ImageViewer_fields">
    <xsl:param name="db"/>
    
    <xsl:variable name="delimitedShownImageViewer">
      <xsl:call-template name="Delimited_Shown_Main_ImageViewer"/>
    </xsl:variable>
    
    <xsl:variable name="AllUniqShownImageViewer">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$delimitedShownImageViewer"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="$AllUniqShownImageViewer != ''">
      <xsl:value-of
        select="concat('$set{ImageViewer:Parent-Normal:', $db, ',$split{', normalize-space($AllUniqShownImageViewer), '}}', $newLine)"
      />
    </xsl:if>
    
  </xsl:template>
  
  <xsl:template name="Delimited_Shown_Main_ImageViewer">
    <!-- variable will result in a delimited list of all Shown URL fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
    <xsl:for-each select="./descendant::*[name() = 'URL']">
      <!-- Dont include Results node under link -->
      <xsl:if test="not(ancestor::Link)">
        <xsl:if test="ancestor::DataItem[1]/@type != 'date'">                    
          <xsl:variable name="IsitImageViewer">
            <xsl:choose>
              <xsl:when test="ancestor::DataItem[1]/@fileSource='iodl'">
                <xsl:value-of select="true()"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="false()"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:choose>                            
            <xsl:when test="$IsitImageViewer='true'">
              <xsl:value-of
                select="concat(ancestor::DataItem[1]/@final_shown_label, '~')"/>
            </xsl:when>                                                     
          </xsl:choose>                    
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="Delimited_Shown_Main_Non_Date">
    <!-- variable will result in a delimited list of all Shown Non Date Fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
    <xsl:apply-templates select="./descendant-or-self::*[name() = 'DataItem']" mode="Main_Non_Date"/>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="Main_Non_Date">
    <!-- variable will result in a delimited list of all Shown Non Date Fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
    <xsl:if test="count(child::Display) &gt; 0 or @Icon_Flag='true'">
      <!-- Dont include Results node under link -->
      <xsl:if test="not(ancestor::Link)">
        <xsl:if test="@type != 'date'">
          <xsl:choose>
            <!-- Sample is processed in function make_Sample_Fields_Entry -->
            <xsl:when test="count(Display/Caption) &gt; 0 or count(Display/URL) &gt; 0">
              <xsl:value-of
                 select="concat(@final_shown_label, '~')"/>
            </xsl:when>
            <xsl:when test="count(Display/Metadata) &gt; 0 or count(Display/Details) &gt; 0 or count(Display/Highlight) &gt; 0">
              <xsl:value-of
                  select="concat(@final_shown_label, '~')"/>
            </xsl:when>
            <xsl:when test="@Icon_Flag='true'">
              <xsl:value-of select="concat(@final_shown_label,'~')"/>
            </xsl:when>
          </xsl:choose>          
        </xsl:if>
      </xsl:if>  
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="Delimited_Main_date_fields">
    <!-- variable will rmesult in a delimited list of all Shown Non Date Fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
    <xsl:apply-templates select="./descendant-or-self::*[name() = 'DataItem']" mode="Main_Date"/>
  </xsl:template>
  
  <xsl:template match="DataItem" mode="Main_Date">
    <!-- variable will rmesult in a delimited list of all Shown Non Date Fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
    <xsl:if test="count(child::Display) &gt; 0 or @Icon_Flag='true'">
      <!-- Dont include Results node under link -->
      <xsl:if test="not(ancestor::Link) and @type='date'">        
        <xsl:value-of select="concat(@final_shown_label,'~')"/>      
      </xsl:if>    
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="Delimited_Shown_Main_URL">
    <!-- variable will result in a delimited list of all Shown URL fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
    <xsl:for-each select="./descendant::*[name() = 'URL']">
      <!-- Dont include Results node under link -->
      <xsl:if test="not(ancestor::Link)">
        <xsl:if test="ancestor::DataItem[1]/@type != 'date'">                    
          <xsl:variable name="IsitImageViewer">
            <xsl:choose>
              <xsl:when test="ancestor::DataItem[1]/@fileSource='iodl'">
                <xsl:value-of select="true()"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="false()"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:choose>                            
            <xsl:when test="$IsitImageViewer='false'">
              <xsl:value-of
                select="concat(ancestor::DataItem[1]/@final_shown_label, '~')"/>
            </xsl:when>                                                     
          </xsl:choose>                    
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="calculate_boolean_using_flags">
    <xsl:param name="boolean_flag"/>
    <xsl:param name="searchwithin_flag"/>
    <xsl:param name="security_flag"/>
    
    <xsl:variable name="precedingPrefix">
      <xsl:call-template name="calculatePrefixNumber"/>
    </xsl:variable>
    
    <!--Only make prefix only if we have Filter/Boolean or Filter/SearchWithinResults
        or DataItems/Security-->
    <xsl:variable name="booleanNumber">
      <xsl:choose>
        <xsl:when test="$boolean_flag = 'true' or $searchwithin_flag = 'true'">
          <xsl:call-template name="calculatePrefixLine">
            <xsl:with-param name="prePrefix" select="$precedingPrefix"/>
            <xsl:with-param name="StartLineString" select="''"/>
            <xsl:with-param name="PrefixLetters" select="$PrefixLetter_Generated"/>
            <xsl:with-param name="FISC_Constant_Boolean" select="ancestor::DataItem[1]/Filter/Boolean/@fiscConstantPrefix"/>
            <xsl:with-param name="FISC_Constant_SearchWithinResults" select="ancestor::DataItem[1]/Filter/SearchWithinResults/@fiscConstantPrefix"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$security_flag='true'">
          <xsl:value-of select="$FISC_SECURITY_PREFIX"/>  
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:value-of select="$booleanNumber"/>
    
  </xsl:template>
  
</xsl:stylesheet>