<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:ext="http://exslt.org/common">
  <xsl:include href="frisk_xml_parser_utils.xsl"/>
  <xsl:include href="frisk_constants.xsl"/>

  <!-- Identity template : copy all text nodes, elements and attributes -->
  <xsl:template match="node() | @*" mode="preprocess_boolean_slots">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="preprocess_boolean_slots"/>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="Filter" mode="preprocess_boolean_slots">
    <xsl:copy>
      <xsl:variable name="DateRange_Constant">
        <xsl:value-of select="//DateRangeField/@fiscConstantPrefix"/>
      </xsl:variable>
      <xsl:variable name="Sort_Constant">
        <xsl:value-of select="//Sort/@fiscConstantPrefix"/>
      </xsl:variable>
      <xsl:if test="count(child::Sort) &gt; 0 and count(child::DateRangeField) &gt; 0">
        <xsl:if test="string-length(child::Sort/@fiscConstantPrefix) &gt; 0">
          <xsl:if test="$Sort_Constant != $DateRange_Constant">
            <xsl:message terminate="yes">
              <xsl:value-of
                select="concat(ancestor::DataItem[1]/@name, ' has different constants on Sort and DateRangeField', $newLine)"
              />
            </xsl:message>
          </xsl:if>
        </xsl:if>
        <xsl:if test="string-length(child::DateRangeField/@fiscConstantPrefix) &gt; 0">
          <xsl:if test="$DateRange_Constant != $Sort_Constant">
            <xsl:message terminate="yes">
              <xsl:value-of
                select="concat(ancestor::DataItem[1]/@name, ' has different constants on Sort and DateRangeField', $newLine)"
              />
            </xsl:message>
          </xsl:if>

        </xsl:if>
      </xsl:if>
      <xsl:apply-templates select="@* | node()" mode="preprocess_boolean_slots"/>
    </xsl:copy>
  </xsl:template>



  <xsl:template match="DataItem" mode="preprocess_boolean_slots">
    <xsl:copy>

      <xsl:attribute name="type_label">
        <xsl:choose>
          <xsl:when test="not(@type) or @type = ''">
            <xsl:value-of select="'string'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@type"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:variable name="eic_converted">
        <xsl:call-template name="fisc_to_eic_mapping">
          <xsl:with-param name="InputField" select="@name"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="fisc_mapped_name">
        <xsl:call-template name="fisc_to_metadata_mapping">
          <xsl:with-param name="LineNamePre" select="$eic_converted"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="final_name_to_use_in_ui">
        <xsl:choose>
          <xsl:when test="string-length(@typefield) > 0">
            <xsl:value-of select="'subdoc_typefield'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$fisc_mapped_name"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:attribute name="final_dataitem_label">
        <xsl:choose>
          <xsl:when test="string-length(@alias) > 0">
            <xsl:value-of select="@alias"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$final_name_to_use_in_ui"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:variable name="Slot_Count">
        <xsl:value-of select="count(Filter/Sort) + count(Filter/DateRangeField)"/>
      </xsl:variable>

      <xsl:attribute name="Is_Slot_present">
        <xsl:choose>
          <xsl:when test="$Slot_Count &gt; 0">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:variable name="All_Boolean_Count">
        <xsl:value-of
          select="count(Security) + count(Filter/Boolean) + count(Filter/SearchWithinResults) + count(Filter/Hierarchical)"
        />
      </xsl:variable>


      <xsl:variable name="Boolean_Count">
        <xsl:value-of
          select="count(Filter/Boolean) + count(Filter/SearchWithinResults) + count(Filter/Hierarchical)"
        />
      </xsl:variable>

      <xsl:attribute name="Is_Boolean_present">
        <xsl:choose>
          <xsl:when test="$All_Boolean_Count &gt; 0 or @metadata = 'true'">
            <xsl:value-of select="true()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="false()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <xsl:variable name="Boolean_number">
        <xsl:call-template name="calculatePrefixNumber"/>
      </xsl:variable>

      <xsl:variable name="IncNumber">
        <xsl:value-of select="$NumberNode + $Boolean_number"/>
      </xsl:variable>

      <xsl:variable name="NodeLetters">
        <xsl:number value="$IncNumber" format="A"/>
      </xsl:variable>

      <xsl:variable name="boolean_prefix_constants">
        <xsl:call-template name="calculatePrefixLine">
          <xsl:with-param name="prePrefix" select="$Boolean_number"/>
          <xsl:with-param name="StartLineString" select="''"/>
          <xsl:with-param name="PrefixLetters" select="$PrefixLetter_Generated"/>
          <xsl:with-param name="FISC_Constant_Boolean" select="Filter/Boolean/@fiscConstantPrefix"/>
          <xsl:with-param name="FISC_Constant_SearchWithinResults"
            select="Filter/SearchWithinResults/@fiscConstantPrefix"/>
          <xsl:with-param name="FISC_Constant_Hierarchical"
            select="Filter/Hierarchical/@fiscConstantPrefix"/>
          <xsl:with-param name="dont_use_suffix" select="'NO'"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="boolean_prefix_to_use">
        <xsl:choose>
          <xsl:when test="string-length($boolean_prefix_constants) &gt; 0">
            <xsl:value-of select="$boolean_prefix_constants"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat($PrefixLetter_Generated, $NodeLetters)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>


      <xsl:attribute name="Boolean_prefix">
        <xsl:choose>
          <xsl:when test="count(Security) &gt; 0">
            <xsl:value-of select="$FISC_SECURITY_PREFIX"/>
          </xsl:when>
          <xsl:when test="$Boolean_Count &gt; 0">
            <xsl:value-of select="$boolean_prefix_to_use"/>
          </xsl:when>
          <xsl:when test="@metadata = 'true'">
            <xsl:call-template name="make_prefixes_boolean"/>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="not(@name = preceding::DataItem/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not(@name = following::DataItem/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>


      <xsl:apply-templates select="@* | node()" mode="preprocess_boolean_slots"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template name="make_prefixes_boolean">
    <xsl:variable name="MetadataInit">
      <xsl:number format="1" count="DataItem[@metadata = 'true']" level="any" from="ConnectionType"
      />
    </xsl:variable>

    <xsl:variable name="MetadataNum">
      <xsl:choose>
        <xsl:when test="number($MetadataInit)">
          <xsl:value-of select="$MetadataInit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="0"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="IncNumber">
      <xsl:value-of select="$NumberNode + $MetadataNum - 1"/>
    </xsl:variable>

    <xsl:variable name="NodeLetters">
      <xsl:number value="$IncNumber" format="A"/>
    </xsl:variable>

    <xsl:variable name="fisc_constant">
      <xsl:value-of select="@fiscConstantPrefix"/>
    </xsl:variable>

    <xsl:variable name="fisc_constant_value">
      <xsl:if test="string-length($fisc_constant) &gt; 0">
        <xsl:value-of select="document('FISC_CONSTANTS.xml')/map/entry[@key = $fisc_constant]"/>
      </xsl:if>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="string-length($fisc_constant) &gt; 0">
        <xsl:if test="string-length($fisc_constant_value) &lt; 1">
          <xsl:message terminate="yes">
            <xsl:value-of select="concat(@name, ' has constant which is not defined', $newLine)"/>
          </xsl:message>
        </xsl:if>
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="string-length($fisc_constant_value) &gt; 0">
        <xsl:value-of select="$fisc_constant_value"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('XM', $NodeLetters)"/>
      </xsl:otherwise>
    </xsl:choose>



  </xsl:template>

  <xsl:template match="Sort" mode="preprocess_boolean_slots">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="@display = 'false'"/>
      <xsl:otherwise>
        <xsl:if test="not(number(@position))">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', $Current_dataItem, ' has Sort Node without numeric position', $newLine)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>


    <xsl:copy>

      <xsl:variable name="combinedSlot">
        <xsl:call-template name="get_slot_number_constants"/>
      </xsl:variable>

      <xsl:attribute name="slot">
        <xsl:value-of select="$combinedSlot"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = preceding::DataItem[Filter/Sort]/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = following::DataItem[Filter/Sort]/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
          <xsl:call-template name="buildSortItem">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template match="DateRangeField" mode="preprocess_boolean_slots">
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="@display = 'false'"/>
      <xsl:otherwise>
        <xsl:if test="not(number(@position))">
          <xsl:message terminate="yes">
            <xsl:value-of
              select="concat('DataItem ', $Current_dataItem, ' has DateRangeField Node without numeric position', $newLine)"
            />
          </xsl:message>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:copy>

      <xsl:variable name="combinedSlot">
        <xsl:call-template name="get_slot_number_constants"/>
      </xsl:variable>

      <xsl:attribute name="slot">
        <xsl:value-of select="$combinedSlot"/>
      </xsl:attribute>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = preceding::DataItem[Filter/DateRangeField]/@name)">
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitFirstNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="not($Current_dataItem = following::DataItem[Filter/DateRangeField]/@name)">
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="true()"/>
          </xsl:attribute>
          <xsl:call-template name="buildDateRangeItem">
            <xsl:with-param name="Current_dataItem" select="$Current_dataItem"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="IsitLastNode">
            <xsl:value-of select="false()"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@* | node()" mode="preprocess_ui"/>
    </xsl:copy>

  </xsl:template>

  <xsl:template name="buildSortItem">
    <xsl:param name="Current_dataItem"/>

    <xsl:choose>
      <xsl:when test="not(ancestor::Link)">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="buildDateRangeItem">
    <xsl:param name="Current_dataItem"/>

    <xsl:choose>
      <xsl:when test="not(ancestor::Link)">
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="true()"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="makeEntryUI">
          <xsl:value-of select="false()"/>
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

</xsl:stylesheet>
