<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:ext="http://exslt.org/common">
    <xsl:import href="create_xml_for_preprocess_shown.xsl"/>
    <xsl:output method="text" indent="no"/>

    <xsl:key name="SearchTypes" match="DataItem" use="API/SearchType/@name"/>

    <xsl:key name="DateTypes" match="DataItem" use="@name"/>
    
    <xsl:template match="/">
        <xsl:variable name="preProcessXMLForShownFields">
            <xsl:apply-templates mode="preprocess_shown"/>
        </xsl:variable>

        <xsl:apply-templates select="ext:node-set($preProcessXMLForShownFields)/*"/>
    </xsl:template>

    <xsl:template match="DataSet">
        <xsl:variable name="DataSet_Alias">
            <xsl:value-of select="@name"/>
        </xsl:variable>

        <xsl:variable name="dbname">
            <xsl:value-of select="@db"/>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$dbname != ''">
                <xsl:call-template name="AllDataItems">
                    <xsl:with-param name="DataSetName" select="$DataSet_Alias"/>
                    <xsl:with-param name="db" select="$dbname"/>
                </xsl:call-template>                
            </xsl:when>
            <xsl:otherwise>
                <xsl:message terminate="yes">
                    <xsl:text>db attribute is mandatory on DataSet Node.</xsl:text>
                </xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="DataSetConfig">
        <xsl:apply-templates select="DataSet"/>
    </xsl:template>

    <xsl:template name="Search_by_api_type">
        <xsl:param name="api_type"/>
        <xsl:param name="data_set"/>
        <xsl:value-of select="concat('$set{', $api_type, ':', $data_set, ',$split{')"/>
        <xsl:for-each select="key('SearchTypes', $api_type)/@name">
            <xsl:choose>
                <xsl:when test="position() = last()">
                    <xsl:value-of select="."/>
                    <xsl:value-of select="'}}'"/>
                </xsl:when>
                <xsl:when test="position() != last()">
                    <xsl:value-of select="."/>
                    <xsl:value-of select="' '"/>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="tokenize_and_Process_Api_Type">
        <!--passed template parameter -->
        <xsl:param name="DataSetName"/>
        <xsl:param name="list"/>
        <xsl:param name="delimiter"/>
        <xsl:choose>
            <xsl:when test="contains($list, $delimiter)">
                <!-- get everything in front of the first delimiter -->
                <xsl:variable name="token_init">
                    <xsl:value-of select="substring-before($list, $delimiter)"/>
                </xsl:variable>

                <xsl:choose>
                    <xsl:when test="$token_init = $delimiter or $token_init = ''"> </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="Search_by_api_type">
                            <xsl:with-param name="data_set" select="$DataSetName"/>
                            <xsl:with-param name="api_type" select="$token_init"/>
                        </xsl:call-template>
                        <xsl:value-of select="$newLine"/>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:call-template name="tokenize_and_Process_Api_Type">
                    <xsl:with-param name="DataSetName" select="$DataSetName"/>
                    <xsl:with-param name="list" select="substring-after($list, $delimiter)"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$list = ''">
                        <xsl:text/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="token_later">
                            <xsl:value-of select="$list"/>
                        </xsl:variable>
                        <xsl:call-template name="Search_by_api_type">
                            <xsl:with-param name="data_set" select="$DataSetName"/>
                            <xsl:with-param name="api_type" select="$token_later"/>
                        </xsl:call-template>
                        <xsl:value-of select="$newLine"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="makeSubdocPrefixes">
        <xsl:param name="SubdocPrefixes"/>
        <xsl:param name="db"/>
        <xsl:if test="string-length($SubdocPrefixes) &gt; 0">
            <xsl:value-of select="concat('${ Define subdoc prefixes }', $newLine)"/>
            <xsl:value-of
                select="concat('$set{subdoc-prefixes:', $db, ',$split{', $SubdocPrefixes, '}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template name="calculateDateFieldNames">
        <xsl:param name="db"/>
        <xsl:param name="dateFormat"/>
        <xsl:choose>
            <xsl:when test="string-length(@typefield) &gt; 0">
                <xsl:value-of select="concat('$opt{subdoc_type_field:', $db, '},')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat(@final_shown_label, ',', @displayFormat, ',')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="ProcessFileSizeLink">
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type != 'date']">
            <xsl:if test="./child::*[name() = 'Display'] or @Icon_Flag='true'">
                <xsl:if test="count(Display/Sample) &lt; 1 and @name = 'fisc_file_size'">
                    <xsl:call-template name="check_sub_fields_to_include"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessPreviewFileSizeLink">
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type != 'date']">
            <xsl:if test="./child::*[name() = 'Preview'] or @Icon_Flag='true'">
                <xsl:if test="@name = 'fisc_file_size'">
                    <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessSampleLink">
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type != 'date']">
            <xsl:if test="./child::*[name() = 'Display'] or @Icon_Flag='true'">
                <xsl:if test="count(Display/Sample) &gt; 0">          
                    <xsl:value-of select="concat(@final_shown_label, ' ')"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessDateLink">
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type = 'date']">
            <xsl:if test="./child::*[name() = 'Display'] or @Icon_Flag='true'">
                <xsl:if test="count(Display/Sample) &lt; 1">
                    <xsl:call-template name="check_sub_fields_to_include"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessPreviewDateLink">
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type = 'date']">
            <xsl:if test="./child::*[name() = 'Preview'] or @Icon_Flag='true'">
                <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessSubDocDateLink">
        <xsl:param name="db"/>
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type = 'date']">
            <xsl:if test="./child::*[name() = 'Display']">
                <xsl:if test="count(descendant::Metadata) &gt; 0 or count(descendant::Details) &gt; 0 or @Icon_Flag='true'">
                    <xsl:call-template name="calculateDateFieldNames">
                        <xsl:with-param name="db" select="$db"/>
                        <xsl:with-param name="dateFormat" select="@displayFormat"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessSubDocDatePreviewLink">
        <xsl:param name="db"/>
        <xsl:for-each select="./descendant::*[name() = 'DataItem' and @type = 'date']">
            <xsl:if test="./child::*[name() = 'Preview'] or @Icon_Flag='true'">
                <xsl:call-template name="calculateDateFieldNames">
                    <xsl:with-param name="db" select="$db"/>
                    <xsl:with-param name="dateFormat" select="@displayFormat"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="ProcessSubSampleLink">
        <xsl:for-each select="./descendant::*[name() = 'DataItem']">
            <xsl:if test="./child::*[name() = 'Display'] or @Icon_Flag='true'">
                <xsl:if test="count(descendant::Sample) &gt; 0">
                    <xsl:value-of select="descendant::Sample[1]/@length"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="makeMetaPrefixesLine">
        <xsl:param name="MetaPrefixes"/>
        <xsl:param name="db"/>
        <xsl:if test="$MetaPrefixes != ''">
            <xsl:value-of
                select="concat('$set{metadata-prefixes:', $db, ',$split{$if{$cgi{meta},', normalize-space($MetaPrefixes), '}}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_Sample_Fields_Entry">
        <xsl:param name="db"/>
        <!-- Count Sample at Main Level-->
        <xsl:variable name="SampleCount_Main">
            <xsl:value-of
                select="count(descendant-or-self::*[name() = 'DataItem']//Display/Sample[not(ancestor::*[name() = 'Link'])][not(ancestor::*[name() = 'Preview'])])"
            />
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$SampleCount_Main &gt; 1">
                <xsl:message terminate="yes">
                    <xsl:text>There cannot be more than 1 Sample at main level (not under Link) and (not under Preview)</xsl:text>
                </xsl:message>
            </xsl:when>
        </xsl:choose>

        <xsl:variable name="Sample_Main_Value">
            <xsl:choose>
                <xsl:when
                    test="descendant-or-self::*[name() = 'DataItem'][./Display/Sample][not(ancestor::*[name() = 'Link'])][not(ancestor::*[name() = 'Preview'])]/@final_shown_label != ''">
                    <xsl:value-of
                        select="descendant-or-self::*[name() = 'DataItem'][./Display/Sample][not(ancestor::*[name() = 'Link'])][not(ancestor::*[name() = 'Preview'])]/@final_shown_label"
                    />
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="Sample_Main_Length">
            <xsl:choose>
                <xsl:when
                    test="descendant-or-self::*[name() = 'DataItem']/Display/Sample[not(ancestor::*[name() = 'Link'])][not(ancestor::*[name() = 'Preview'])]/@length != ''">
                    <xsl:value-of
                        select="descendant-or-self::*[name() = 'DataItem']/Display/Sample[not(ancestor::*[name() = 'Link'])][not(ancestor::*[name() = 'Preview'])]/@length"
                    />
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$Sample_Main_Value != '' and $Sample_Main_Length != ''">
                <xsl:value-of
                    select="concat('$set{Search:SampleField:', $db, ',$split{', $Sample_Main_Value, '}}', $newLine)"/>
                <xsl:value-of
                    select="concat('$setmap{Search:SampleField_Length:', $db, ',', $Sample_Main_Value, ',', $Sample_Main_Length, '}', $newLine)"
                />
            </xsl:when>
        </xsl:choose>

    </xsl:template>

     <xsl:template name="make_subdoc_typefield">
        <xsl:param name="db"/>
        <xsl:value-of
            select="concat('$set{', $subdoc_variable_name, ':', $db, ',', $Typefield_String, '}', $newLine)"
        />
    </xsl:template>

    <xsl:template name="make_subdoc_id_field">
        <xsl:param name="db"/>

        <xsl:variable name="subdoc_id_field">
            <xsl:choose>
                <xsl:when test="descendant::Linked[1]/@by != ''">
                    <xsl:value-of select="descendant::Linked[1]/@by"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$subdoc_id_field != ''">
                <xsl:value-of
                    select="concat('$set{subdoc_id_field:', $db, ',', $subdoc_id_field, '}', $newLine)"
                />
            </xsl:when>
        </xsl:choose>

    </xsl:template>

    <xsl:template name="AllDataItems" xml:space="default">
        <xsl:param name="DataSetName"/>
        <xsl:param name="db"/>

        <xsl:call-template name="make_subdoc_typefield">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_subdoc_id_field">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:variable name="metadata_prefixes_list">
            <xsl:apply-templates select="./descendant-or-self::*[name()='DataItem'][@metadata = 'true']" mode="metadata_prefixes"/>            
        </xsl:variable>
        
        <xsl:variable name="Unique_meta_prefixes_list">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$metadata_prefixes_list"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:call-template name="makeMetaPrefixesLine">
            <xsl:with-param name="MetaPrefixes" select="$Unique_meta_prefixes_list"/>
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:variable name="LinkCount">
            <xsl:value-of select="count(./descendant::*[name() = 'Link'])"/>
        </xsl:variable>

        <xsl:variable name="Sample_Entry">
            <xsl:call-template name="make_Sample_Fields_Entry">
                <xsl:with-param name="db" select="$db"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$Sample_Entry != ''">
                <xsl:value-of select="$Sample_Entry"/>
            </xsl:when>
        </xsl:choose>

        <xsl:call-template name="make_mainlevel_multilines">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_all_multilines_subdoc">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
        </xsl:call-template>

        <xsl:call-template name="make_all_subdoc_normal">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
        </xsl:call-template>

        <xsl:call-template name="make_subdoc_size_fields">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
            <xsl:with-param name="Search_Or_Preview" select="'Search'"/>
        </xsl:call-template>

        <xsl:call-template name="make_subdoc_date_fields">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
            <xsl:with-param name="Search_Or_Preview" select="'Search'"/>
        </xsl:call-template>

        <xsl:call-template name="make_subdoc_sample_fields">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
        </xsl:call-template>

        <xsl:variable name="ApiTypes">
            <xsl:for-each select="./descendant::*[name() = 'DataItem']">
                <xsl:if test="./descendant::*[name() = 'API']">
                    <xsl:for-each select="./descendant::*[name() = 'SearchType']">
                        <xsl:value-of select="concat(@name, '~')"/>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="UniqueApiTypes">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$ApiTypes"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="'~'"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:call-template name="tokenize_and_Process_Api_Type">
            <xsl:with-param name="DataSetName" select="concat('_', $DataSetName)"/>
            <xsl:with-param name="list" select="$UniqueApiTypes"/>
            <xsl:with-param name="delimiter" select="'~'"/>
        </xsl:call-template>

        <xsl:variable name="StdShownMaps">
            <xsl:call-template name="makeStdShownMap">
                <xsl:with-param name="delimitedValues" select="$UniqueApiTypes"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="'~'"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:call-template name="make_main_date_shown">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_main_nondate_shown">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_main_nondate_export">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_main_date_exportReport">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="DelimitedNonDate_subdoc_ExportReport">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
        </xsl:call-template>

        <xsl:call-template name="DelimitedDate_subdoc_ExportReport">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
        </xsl:call-template>

        <xsl:call-template name="make_common_maps">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_preview_fields_main_normal">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_preview_fields_main_date">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_preview_fields_size">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_subdoc_date_fields">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
            <xsl:with-param name="Search_Or_Preview" select="'Preview'"/>
        </xsl:call-template>

        <xsl:call-template name="make_subdoc_size_fields">
            <xsl:with-param name="db" select="$db"/>
            <xsl:with-param name="Count_Links" select="$LinkCount"/>
            <xsl:with-param name="Search_Or_Preview" select="'Preview'"/>
        </xsl:call-template>

        <xsl:call-template name="Make_list_logged_fields">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="write_security_prefixes">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="write_include_filters">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="write_exclude_filters">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="make_main_date_shown">
        <xsl:param name="db"/>

        <xsl:variable name="delimitedShownDate">
            <xsl:call-template name="Delimited_Main_date_fields"/>
        </xsl:variable>
        <xsl:variable name="AllUniqShownDate">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$delimitedShownDate"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:if test="$AllUniqShownDate != ''">
            <xsl:value-of
                select="concat('$set{Search:dtfld:', $db, ',$split{', $AllUniqShownDate, '}}', $newLine)"/>
            <xsl:value-of select="concat('$setmap{Search:DateValueMap:', $db)"/>
            <xsl:call-template name="process_Date_Fields">
                <xsl:with-param name="list" select="$AllUniqShownDate"/>
            </xsl:call-template>
            <xsl:value-of select="concat('}', $newLine)"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_main_nondate_shown">
        <xsl:param name="db"/>

        <xsl:call-template name="make_normal_Main_fields">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_normal_URL_fields">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="make_normal_ImageViewer_fields">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="make_main_nondate_export">
        <xsl:param name="db"/>


        <xsl:variable name="delimitedShownNonDate_ExportReport">
            <xsl:call-template name="Delimited_Shown_Main_Export_Non_Date"/>
        </xsl:variable>
        <xsl:variable name="AllUniqShownNonDate_ExportReport">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$delimitedShownNonDate_ExportReport"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="$AllUniqShownNonDate_ExportReport != ''">
            <xsl:value-of
                select="concat('$set{ExportToReport:Normal:', $db, ',$split{', $AllUniqShownNonDate_ExportReport, '}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_main_date_exportReport">
        <xsl:param name="db"/>
        <xsl:variable name="delimitedShownDate_ExportReport">
            <xsl:call-template name="Delimited_Shown_Main_Export_Date"/>
        </xsl:variable>
        <xsl:variable name="AllUniqShownDate_ExportReport">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$delimitedShownDate_ExportReport"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="$AllUniqShownDate_ExportReport != ''">
            <xsl:value-of
                select="concat('$set{ExportToReport:dtfld:', $db, ',$split{', $AllUniqShownDate_ExportReport, '}}', $newLine)"/>
            <xsl:value-of select="concat('$setmap{ExportToReport:DateValueMap:', $db)"/>
            <xsl:call-template name="process_Date_Fields">
                <xsl:with-param name="list" select="$AllUniqShownDate_ExportReport"/>
            </xsl:call-template>
            <xsl:value-of select="concat('}', $newLine)"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_common_maps">
        <xsl:param name="db"/>
        <xsl:value-of
            select="concat('$setmap{Search:Date_Map,', $db, ',$opt{Search:dtfld:', $db, '}}', $newLine)"/>
        <xsl:value-of
            select="concat('$setmap{Search:NoDate_Map,', $db, ',$opt{Search:Normal:', $db, '}}', $newLine)"/>

        <xsl:value-of
            select="concat('$setmap{ExportToReport:Date_Map,', $db, ',$opt{ExportToReport:dtfld:', $db, '}}', $newLine)"/>
        <xsl:value-of
            select="concat('$setmap{ExportToReport:NoDate_Map,', $db, ',$opt{ExportToReport:Normal:', $db, '}}', $newLine)"
        />
    </xsl:template>

    <xsl:template match="DataItem" mode="Preview_Main_Size">
        <xsl:if test="count(child::Preview) &gt; 0 or @Icon_Flag='true'">
            <xsl:if test="not(ancestor::Link)">
                <xsl:if test="@name = 'fisc_file_size'">
                    <xsl:for-each select="child::*">
                        <xsl:value-of
                            select="concat(ancestor::DataItem[1]/@final_shown_label, ' ')"/>
                    </xsl:for-each>
                </xsl:if>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_preview_fields_size">
        <xsl:param name="db"/>

        <xsl:variable name="Delimited_preview_main_size">
            <xsl:apply-templates select="./descendant-or-self::*[name() = 'DataItem']" mode="Preview_Main_Size"/>
        </xsl:variable>

        <xsl:if test="normalize-space($Delimited_preview_main_size)">
            <xsl:value-of
                select="concat('$set{Preview:Parent-Size:', $db, ',$split{', normalize-space($Delimited_preview_main_size), '}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="DataItem" mode="Preview_Main_Date">
        <xsl:if test="count(child::Preview) &gt; 0 or @Icon_Flag='true'">
            <xsl:if test="not(ancestor::Link) and @type='date'">                           
                <xsl:value-of select="concat(@final_shown_label, ' ')"/>                       
            </xsl:if>    
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="DataItem" mode="Preview_Main_Date_Map">
        <xsl:if test="count(child::Preview) &gt; 0 or @Icon_Flag='true'">
            <xsl:if test="not(ancestor::Link) and @type='date'">                             
                <xsl:value-of select="concat(@final_shown_label, ',', @displayFormat, ',')"/>                       
            </xsl:if>    
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_preview_fields_main_date">
        <xsl:param name="db"/>

        <xsl:variable name="Delimited_preview_main_date">
            <xsl:apply-templates select="./descendant-or-self::*[name()='DataItem']" mode="Preview_Main_Date"/>
        </xsl:variable>

        <xsl:variable name="Delimited_preview_main_date_map">
            <xsl:apply-templates select="./descendant-or-self::*[name() = 'DataItem']" mode="Preview_Main_Date_Map"/>
        </xsl:variable>

        <xsl:variable name="Stripped_preview_date_fields_link">
            <xsl:call-template name="strip-end-characters">
                <xsl:with-param name="text" select="$Delimited_preview_main_date_map"/>
                <xsl:with-param name="strip-count" select="1"/>
            </xsl:call-template>
        </xsl:variable>


        <xsl:if test="normalize-space($Delimited_preview_main_date) != ''">
            <xsl:value-of
                select="concat('$set{Preview:Parent-Date:', $db, ',$split{', normalize-space($Delimited_preview_main_date), '}}', $newLine)"/>
            <xsl:value-of
                select="concat('$setmap{Preview:Parent-DateMap:', $db, ',', $Stripped_preview_date_fields_link, '}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template match="DataItem" mode="Preview_Main">        
        <xsl:if test="count(child::Preview) &gt; 0 or @Icon_Flag='true'">
            <xsl:if test="not(ancestor::Link)">
                <xsl:if test="@type != 'date' and @name != 'fisc_file_size'">
                    <xsl:value-of select="concat(@final_shown_label, ' ~')"/>                    
                </xsl:if>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_preview_fields_main_normal">
        <xsl:param name="db"/>

        <xsl:variable name="Delimited_preview_main_normal">
            <xsl:apply-templates select="./descendant-or-self::*[name() = 'DataItem']" mode="Preview_Main"/>
        </xsl:variable>
        <xsl:variable name="AllUniq_Preview_Main_Normal">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$Delimited_preview_main_normal"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="normalize-space($AllUniq_Preview_Main_Normal) != ''">
            <xsl:value-of
                select="concat('$set{Preview:Parent-Normal:', $db, ',$split{', normalize-space($AllUniq_Preview_Main_Normal), '}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template name="Delimited_Shown_Main_Export_Non_Date">
        <!-- variable will result in a delimited list of all Shown Fields. 
            will include duplicates and is delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
        <xsl:apply-templates
            select="/descendant::*[name() = 'DataItem' and not(ancestor::*[name() = 'Link']) and @exportReportFinal = 'true' and @type != 'date']"
            mode="ExportToReport"/>
    </xsl:template>

    <xsl:template name="Delimited_Shown_Main_Export_Date">
        <xsl:apply-templates
            select="/descendant::*[name() = 'DataItem' and not(ancestor::*[name() = 'Link']) and @exportReportFinal = 'true' and @type = 'date']"
            mode="ExportToReport"/>
    </xsl:template>

    <xsl:template name="DelimitedNonDate_subdoc_ExportReport">
        <xsl:param name="db"/>
        <xsl:param name="Count_Links"/>

        <xsl:if test="$Count_Links > 0">
            <xsl:for-each select="./descendant::*[name() = 'Link']">
                <xsl:variable name="prefix_val">
                    <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
                </xsl:variable>
                
                <xsl:variable name="Fields_for_link">
                    <xsl:apply-templates
                        select="./descendant::*[name() = 'DataItem'][@exportReportFinal = 'true'][@type != 'date']"
                        mode="ExportToReport"/>
                </xsl:variable>
                <!-- strip last char from Fields_for_link and we are done by using normalize-space to get rid of last space -->
                <xsl:variable name="AllUniqSubdocNonDate">
                    <xsl:call-template name="processUniqueValues" xml:space="preserve">
                        <xsl:with-param name="delimitedValues" select="$Fields_for_link"/>
                        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                        <xsl:with-param name="Delimiter" select="' '"/>
                    </xsl:call-template>                    
                </xsl:variable>                
                <xsl:if test="normalize-space($AllUniqSubdocNonDate) != ''">
                    <xsl:value-of
                        select="concat('$set{ExportToReport:', $prefix_val, '-Normal:', $db, ',$split{', normalize-space($AllUniqSubdocNonDate), '}}', $newLine)"
                    />
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="DelimitedDate_subdoc_ExportReport">
        <xsl:param name="db"/>
        <xsl:param name="Count_Links"/>

        <xsl:if test="$Count_Links > 0">
            <xsl:for-each select="./descendant::*[name() = 'Link']">
                <xsl:variable name="prefix_val">
                    <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
                </xsl:variable>
                <xsl:variable name="Fields_for_link">
                    <xsl:apply-templates
                        select="./descendant::*[name() = 'DataItem'][@exportReportFinal = 'true'][@type = 'date']"
                        mode="ExportToReport"/>
                </xsl:variable>
                <xsl:variable name="AllUniqSubdocDate">
                    <xsl:call-template name="processUniqueValues" xml:space="preserve">
                        <xsl:with-param name="delimitedValues" select="$Fields_for_link"/>
                        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                        <xsl:with-param name="Delimiter" select="' '"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:if test="normalize-space($AllUniqSubdocDate) != ''">
                    <xsl:value-of
                        select="concat('$set{ExportToReport:', $prefix_val, '-Date:', $db, ',$split{', $AllUniqSubdocDate, '}}', $newLine)"/>
                    <xsl:value-of
                        select="concat('$setmap{ExportToReport:', $prefix_val, '-DateMap:', $db)"/>
                    <xsl:call-template name="process_Date_Fields">
                        <xsl:with-param name="list" select="$AllUniqSubdocDate"/>
                    </xsl:call-template>
                    <xsl:value-of select="concat('}', $newLine)"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template match="DataItem" mode="ExportToReport">
        <xsl:value-of select="concat(@final_shown_label, '~')"/>
    </xsl:template>

    <xsl:template name="make_subdoc_date_fields">
        <xsl:param name="db"/>
        <xsl:param name="Count_Links"/>
        <xsl:param name="Search_Or_Preview"/>

        <xsl:if test="$Count_Links > 0">
            <xsl:for-each select="./descendant::*[name() = 'Link']">
                <xsl:variable name="prefix_val">
                    <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
                </xsl:variable>
                <xsl:variable name="Fields_for_link">
                    <xsl:choose>
                        <xsl:when test="$Search_Or_Preview = 'Search'">
                            <xsl:call-template name="ProcessDateLink"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="ProcessPreviewDateLink"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="Stripped_fields_link">
                    <xsl:call-template name="processUniqueValues" xml:space="preserve">
                        <xsl:with-param name="delimitedValues" select="$Fields_for_link"/>
                        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                        <xsl:with-param name="Delimiter" select="' '"/>
                    </xsl:call-template>
                </xsl:variable>

                <xsl:if test="normalize-space($Stripped_fields_link) != ''">
                    <xsl:value-of
                        select="concat('$set{', $Search_Or_Preview, ':', $prefix_val, '-Date', ':', $db, ',$split{', normalize-space($Stripped_fields_link), '}}', $newLine)"
                    />
                </xsl:if>
                <xsl:variable name="SubDocDateFields">
                    <xsl:choose>
                        <xsl:when test="$Search_Or_Preview = 'Search'">
                            <xsl:call-template name="ProcessSubDocDateLink">
                                <xsl:with-param name="db" select="$db"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="ProcessSubDocDatePreviewLink">
                                <xsl:with-param name="db" select="$db"/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="Stripped_date_fields_link">
                    <xsl:call-template name="strip-end-characters">
                        <xsl:with-param name="text" select="$SubDocDateFields"/>
                        <xsl:with-param name="strip-count" select="1"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:if test="normalize-space($Stripped_date_fields_link) != ''">
                    <xsl:value-of
                        select="concat('$setmap{', $Search_Or_Preview, ':', $prefix_val, '-DateMap', ':', $db, ',', $Stripped_date_fields_link, '}', $newLine)"
                    />
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_subdoc_sample_fields">
        <xsl:param name="db"/>
        <xsl:param name="Count_Links"/>
        <xsl:if test="$Count_Links > 0">
            <xsl:for-each select="./descendant::*[name() = 'Link']">
                <xsl:variable name="prefix_val">
                    <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
                </xsl:variable>
                <xsl:variable name="Fields_for_link">
                    <xsl:call-template name="ProcessSampleLink"/>
                </xsl:variable>
                <xsl:variable name="Stripped_fields_link">
                    <xsl:call-template name="strip-end-characters">
                        <xsl:with-param name="text" select="$Fields_for_link"/>
                        <xsl:with-param name="strip-count" select="1"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:if test="normalize-space($Stripped_fields_link) != ''">
                    <xsl:value-of
                        select="concat('$set{Search:', $prefix_val, '-Sample', ':', $db, ',$split{', $Stripped_fields_link, '}}', $newLine)"
                    />
                </xsl:if>

                <xsl:variable name="SubSample">
                    <xsl:call-template name="ProcessSubSampleLink"/>
                </xsl:variable>
                <xsl:if test="normalize-space($SubSample) != ''">
                    <xsl:value-of
                        select="concat('$set{Search:', $prefix_val, '-SampleSize', ':', $db, ',', $SubSample, '}', $newLine)"
                    />
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_subdoc_size_fields">
        <xsl:param name="db"/>
        <xsl:param name="Count_Links"/>
        <xsl:param name="Search_Or_Preview"/>

        <xsl:if test="$Count_Links > 0">
            <xsl:for-each select="./descendant::*[name() = 'Link']">
                <xsl:variable name="prefix_val">
                    <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
                </xsl:variable>
                <xsl:variable name="Fields_for_link">
                    <xsl:choose>
                        <xsl:when test="$Search_Or_Preview = 'Search'">
                            <xsl:call-template name="ProcessFileSizeLink"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="ProcessPreviewFileSizeLink"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="Stripped_fields_link">
                    <xsl:call-template name="processUniqueValues" xml:space="preserve">
                        <xsl:with-param name="delimitedValues" select="$Fields_for_link"/>
                        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                        <xsl:with-param name="Delimiter" select="' '"/>
                     </xsl:call-template>
                </xsl:variable>
                <xsl:if test="normalize-space($Stripped_fields_link) != ''">
                    <xsl:value-of
                        select="concat('$set{', $Search_Or_Preview, ':', $prefix_val, '-Size', ':', $db, ',$split{', normalize-space($Stripped_fields_link), '}}', $newLine)"
                    />
                </xsl:if>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make_mainlevel_multilines">
        <xsl:param name="db"/>

        <xsl:call-template name="main_multilines_search">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

        <xsl:call-template name="main_multilines_preview">
            <xsl:with-param name="db" select="$db"/>
        </xsl:call-template>

    </xsl:template>

    <xsl:template name="Make_list_logged_fields">
        <xsl:param name="db"/>

        <!-- Find all DataItems which have logged="true" and they are not under Link -->
        <xsl:variable name="LoggedItem_MainLevel">
            <!-- variable will result in a delimited list of all Fields which have logged="true". 
           Fields will be delimited with a tilde "~".
            xml produces this: ~id~CasePrdNm~CasePrdNm~ -->
            <xsl:for-each select="./descendant::*[name() = 'DataItem']">
                <!-- Dont include Results node under link -->
                <xsl:if test="not(ancestor::Link)">
                    <xsl:if test="@logged = 'true'">
                        <xsl:variable name="DataItem_tobe_Used" select="@final_shown_label"/>
                        <xsl:if test="$DataItem_tobe_Used != ''">
                            <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
                        </xsl:if>
                    </xsl:if>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>


        <xsl:variable name="AllUniqLogged_MainLevel">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$LoggedItem_MainLevel"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="normalize-space($AllUniqLogged_MainLevel) != ''">
            <xsl:value-of
                select="concat('$set{Logging:', $db, ',$split{', normalize-space($AllUniqLogged_MainLevel), '}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template name="write_security_prefixes">
        <xsl:param name="db"/>

        <xsl:variable name="Count_Security_Tags">
            <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Security)"/>
        </xsl:variable>

        <xsl:if test="$Count_Security_Tags &gt; 1">
            <xsl:message terminate="yes">
                <xsl:text> DataSet cannot have more than 1 DataItem with Security subNode </xsl:text>
            </xsl:message>
        </xsl:if>

        <xsl:variable name="All_Security_Prefixes">
            <xsl:apply-templates select="./descendant::*[name() = 'DataItem']"
                mode="SecurityPrefixes"/>
        </xsl:variable>
        
        <xsl:variable name="AllUniqSecurityPrefixes">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$All_Security_Prefixes"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="' '"/>
      </xsl:call-template>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="string-length($AllUniqSecurityPrefixes) &gt; 0">
                <xsl:value-of
                    select="concat('$set{security_prefixes', $db, ',', normalize-space($AllUniqSecurityPrefixes), ' }', $newLine)"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat('$set{security_prefixes_split-smsf, }', $newLine)"/>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:variable name="Security_ExportReport_Pref">
            <xsl:apply-templates select="./descendant::*[name() = 'DataItem']"
                mode="Security_ExportReport"/>
        </xsl:variable>

        <xsl:variable name="stripped_exportreport_pref">
            <xsl:choose>
                <xsl:when test="string-length($Security_ExportReport_Pref) &gt; 0">
                    <xsl:call-template name="processUniqueValues" xml:space="preserve">
                        <xsl:with-param name="delimitedValues" select="$Security_ExportReport_Pref"/>
                        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                        <xsl:with-param name="Delimiter" select="' '"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:value-of
            select="concat('$set{ExportToReport:Security:', $db, ',', normalize-space($stripped_exportreport_pref), '}', $newLine)"
        />
    </xsl:template>

    <xsl:template name="process_Date_Fields">
        <xsl:param name="list"/>
        <xsl:variable name="newlist" select="concat(normalize-space($list), ' ')"/>
        <xsl:variable name="first" select="substring-before($newlist, ' ')"/>
        <xsl:variable name="remaining" select="substring-after($newlist, ' ')"/>

        <xsl:if test="$first != ''">
            <xsl:variable name="Field_name_to_use">
                <xsl:call-template name="eic_to_fisc_mapping">
                    <xsl:with-param name="InputField" select="$first"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="concat(',', $first, ',')"/>
            <xsl:value-of select="key('DateTypes', $Field_name_to_use)/@displayFormat"/>
        </xsl:if>
        <xsl:if test="$remaining">
            <xsl:call-template name="process_Date_Fields">
                <xsl:with-param name="list" select="$remaining"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="makeStdShownMap">
        <xsl:param name="delimitedValues"/>
        <xsl:param name="CalledFromWithin"/>
        <xsl:param name="Delimiter"/>
        <xsl:variable name="firstOne">
            <!-- variable firstOne: the first value in the delimited list of-->
            <xsl:value-of select="substring-before($delimitedValues, '~')"/>
        </xsl:variable>
        <xsl:variable name="firstOneDelimited">
            <!-- variable firstOneDelimited: the first value in the delimitedof items with the tilde "~" delimiter -->
            <xsl:value-of select="substring-before($delimitedValues, '~')"/>~</xsl:variable>
        <xsl:variable name="theRest">
            <!-- variable theRest: the rest of the delimited list after theone is removed -->
            <xsl:value-of select="substring-after($delimitedValues, '~')"/>
        </xsl:variable>
        <xsl:choose>
            <!-- when the current one exists again in the remaining list ANDfirst one isn't empty, -->
            <xsl:when test="contains($theRest, $firstOneDelimited) and not($firstOne = '')">
                <xsl:call-template name="processUniqueValues">
                    <xsl:with-param name="delimitedValues" select="$theRest"/>
                    <xsl:with-param name="CalledFromWithin" select="'YES'"/>
                    <xsl:with-param name="Delimiter" select="$Delimiter"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <!-- Only put space if it is not first one -->
                <xsl:if test="normalize-space($firstOne) != '' and $CalledFromWithin = 'YES'">
                    <xsl:value-of select="$Delimiter"/>
                </xsl:if>
                <!-- otherwise this is the last occurence in the list, so returnitem with a delimiter tilde "~". -->
                <xsl:value-of select="normalize-space($firstOne)" xml:space="preserve"/>

                <xsl:if test="contains($theRest, '~')">
                    <!-- when there are more left in the delimited list, call thewith the remaining items -->
                    <xsl:call-template name="processUniqueValues">
                        <xsl:with-param name="delimitedValues" select="$theRest"/>
                        <xsl:with-param name="CalledFromWithin" select="'YES'"/>
                        <xsl:with-param name="Delimiter" select="$Delimiter"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="write_include_filters">
        <xsl:param name="db"/>
        <xsl:variable name="include_filters">
            <xsl:apply-templates select="./descendant::*[name() = 'MandatoryOnSearch'][Include]"
                mode="Include"/>
        </xsl:variable>
        <xsl:variable name="stripped_include_filters">
            <xsl:choose>
                <xsl:when test="string-length($include_filters) &gt; 0">
                    <xsl:call-template name="strip-end-characters">
                        <xsl:with-param name="text" select="$include_filters"/>
                        <xsl:with-param name="strip-count" select="1"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="string-length($stripped_include_filters) &gt; 0">
            <xsl:value-of
                select="concat('$set{IncludeFilters:', $db, ',$split{', ancestor-or-self::DataSet[1]/@Include_Delimiter, ',', $stripped_include_filters, '}}', $newLine)"
            />
        </xsl:if>

    </xsl:template>

    <xsl:template name="write_exclude_filters">
        <xsl:param name="db"/>
        <xsl:variable name="exclude_filters">
            <xsl:apply-templates select="./descendant::*[name() = 'MandatoryOnSearch'][Exclude]"
                mode="Exclude"/>
        </xsl:variable>
        <xsl:variable name="stripped_exclude_filters">
            <xsl:choose>
                <xsl:when test="string-length($exclude_filters) &gt; 0">
                    <xsl:call-template name="strip-end-characters">
                        <xsl:with-param name="text" select="$exclude_filters"/>
                        <xsl:with-param name="strip-count" select="1"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="string-length($stripped_exclude_filters) &gt; 0">
            <xsl:value-of
                select="concat('$set{ExcludeFilters:', $db, ',$split{', ancestor-or-self::DataSet[1]/@Exclude_Delimiter, ',', $stripped_exclude_filters, '}}', $newLine)"
            />
        </xsl:if>
    </xsl:template>

    <xsl:template match="Include" mode="Include">
        <xsl:value-of
            select="concat(ancestor-or-self::MandatoryOnSearch[1]/@BooleanFilter, @Add_to_filter, ancestor-or-self::DataSet[1]/@Include_Delimiter)"
        />
    </xsl:template>

    <xsl:template match="Exclude" mode="Exclude">
        <xsl:value-of
            select="concat(ancestor-or-self::MandatoryOnSearch[1]/@BooleanFilter, @Add_to_filter, ancestor-or-self::DataSet[1]/@Exclude_Delimiter)"
        />
    </xsl:template>

    <xsl:template match="DataItem" mode="SecurityPrefixes">
        <xsl:if test="Security">
            <xsl:value-of select="concat('~',$FISC_SECURITY_PREFIX, '~')"/>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(Filter/Boolean/MandatoryOnSearch) &gt; 0">
                <xsl:if test="Filter/Boolean/MandatoryOnSearch/@securityRelated = 'true'">
                    <xsl:value-of
                        select="concat('~',Filter/Boolean/MandatoryOnSearch/@BooleanFilter, '~')"/>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="DataItem" mode="Security_ExportReport">
        <xsl:if test="Security">
            <xsl:if test="ExportToReport">
                <xsl:value-of select="concat('~',@final_shown_label, '~')"/>
            </xsl:if>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
