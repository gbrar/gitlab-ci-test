<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text"/>
    <xsl:include href="frisk_xml_parser_utils.xsl"/>


    <xsl:template match="DataSetConfig">
        <xsl:apply-templates select="DataSet"/>
    </xsl:template>
    <xsl:template match="DataSet">

        <xsl:variable name="db_desc_given">
            <xsl:choose>
                <xsl:when test="@description != ''">
                    <xsl:value-of select="@description"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="db_desc">
            <xsl:choose>
                <!-- if description is greater than the Max_len_desc -->
                <xsl:when test="string-length($db_desc_given) &gt; $Max_len_desc">
                    <xsl:value-of select="substring($db_desc_given, 0, $Max_len_desc)"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- otherwise print out the whole, un-truncated string -->
                    <xsl:value-of select="$db_desc_given"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:value-of select="$db_desc"/>

    </xsl:template>

</xsl:stylesheet>
