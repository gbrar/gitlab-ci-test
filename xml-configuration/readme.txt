These xslt's will be run against xml having multiple sets provided by customer to generate
1.) config.json for UI in /srv/frisk/flexible/www.config_{dataset} file is also produced which is a intermediate file used for making config.json
2.) config_{dataset} files as part of omega-template files which will make it configurable to define and use shown fields
3.) SQL file for querying data source (if applicable)
4.) script file for scriptindex
5.) /etc/frisk/datasets-available/{db_name}/db_long_name file containing description of the dataset.
6.) filtertypes__<dataset-name> in omega-templates folder which is a file like $setmap{filtertypes,XBCE,subdoc,XBCI,doc}
7.) permissions.conf in /etc/frisk/datasets-available/{db_name}/permissions.conf file containing attributePrefix=<prefix of Security>

The way to generate output files from XML is as shown below:
Call the script /usr/bin/frisk-process-xml using sudo.
The arguments required by this script are 

i)  --etc-path="..."
        This is optional argument.It is path of some output files like indexing scripts (for each dataset) and some intermediate files(separate xmls,sqls for each dataset) . 
	Default is [/etc/frisk].

2)  --input_xml_path="..."
        This is mandatory argument.It is path where input xml having one or multiple datasets is present.

3)  --template-path="..."
        This is optional argument.It is path of output files like flexible.json and config_all and config_{dataset1}.json and config_{dataset2}.json etc. 
	Default is [/opt/frisk/omega-templates].

4)  --ui-path=...
        This is optional argument.It is path of some output files for gui like config.json.It also has intermediate files like config_{dataset1}.json and config_{dataset2}.json. 
        Default is [/srv/frisk/flexible/www].

5)  --xslt_path="..."
        This is optional argument.It is path where xslts which transform xml are placed.
        Default is [/etc/frisk/xml-configuration].

6)  --mode="..."
        Mode of running.Currently it is hardcoded as remove_existing.
        Later on other modes will be developed.They will be remove_existing|update_existing|skip_existing|prompt

Examples of running this script from frisk-xml-parser/test folder in frisk-build is as shown below:
 
sudo $current_dir/../frisk-process-xml --input-xml-path $current_dir/input_xml/file_share_plus_hdfs2.xml --xslt-path $current_dir/../xml-configuration --etc-path $current_dir/multi_dataset --ui-path $current_dir/multi_dataset --template-path $current_dir/multi_dataset --debug 2


Description of files present in xslt_path directory is as described below:

i) generate_json_for_ui.xsl to generate config.json for UI.

ii) generate_sql_from_xml.xsl to generate sql queries(for querying data source) from xml input file.

iii) create_shown_fields.xsl to generate config_{dataset} file containing shown fields to be used in flexible.json

iv) create_dataset_longname.xsl to generate db_long_name file in datasets available folder 

v) copy_sql_from_xml.xsl script to copy sql from xml file

vi) script_to_generate_index.xsl to generate index file script

vii) create_filtertypes_map.xsl to generate filtertypes__datasetname

viii) create_permission_conf.xsl to generare permissions.conf in datasets available folder
