<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" indent="no"/>
    <xsl:include href="frisk_xml_parser_utils.xsl"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="FileShare">
        <xsl:value-of select="concat('[', @name, ']', $newLine)"/>
        <xsl:value-of select="concat('sec', '=', @sec, $newLine)"/>
        <xsl:value-of select="concat('share', '=', @path, $newLine)"/>
        <xsl:value-of select="concat('url', '=', @url, $newLine)"/>
        <xsl:value-of select="concat('ver', '=', @ver, $newLine)"/>
    </xsl:template>

    <xsl:template match="HDFSFileShare">
        <xsl:value-of select="concat('[', @name, ']', $newLine)"/>
        <xsl:value-of select="concat('sec', '=', @sec, $newLine)"/>
        <xsl:value-of select="concat('share', '=', @path, $newLine)"/>
        <xsl:value-of select="concat('url', '=', @url, $newLine)"/>
        <xsl:value-of select="concat('ver', '=', @ver, $newLine)"/>
    </xsl:template>

    <xsl:template match="Sql">
        <xsl:value-of select="concat('[', @name, ']', $newLine)"/>
        <xsl:value-of select="concat('database=', @database, $newLine)"/>
        <xsl:value-of select="concat('delete=', @delete, $newLine)"/>
        <xsl:value-of select="concat('password=', @password, $newLine)"/>
        <xsl:value-of select="concat('port=', @port, $newLine)"/>
        <xsl:value-of select="concat('sql_server=', @sql_server, $newLine)"/>
        <xsl:value-of select="concat('type=', @sql_type, $newLine)"/>
        <xsl:value-of select="concat('username=', @username, $newLine)"/>
        <xsl:value-of select="concat('ocr_threads=', @ocr_threads, $newLine)"/>
    </xsl:template>

    <xsl:template match="EMAIL_EXCHANGE">
        <xsl:value-of select="concat('[', @name, ']', $newLine)"/>
        <xsl:value-of select="concat('password=', @password, $newLine)"/>
        <xsl:value-of select="concat('server=', @server, $newLine)"/>
        <xsl:value-of select="concat('username=', @username, $newLine)"/>
        <xsl:value-of select="concat('version=', @version, $newLine)"/>
    </xsl:template>

    <xsl:template match="ConnectionType">
        <xsl:apply-templates select="ODBC"/>
        <xsl:apply-templates select="FileShare"/>
        <xsl:apply-templates select="HDFSFileShare"/>
        <xsl:apply-templates select="EMAIL_EXCHANGE"/>
        <xsl:apply-templates select="Sql"/>
    </xsl:template>

    <xsl:template match="ODBC">
        <xsl:variable name="dest_db_name">
            <xsl:choose>
                <xsl:when test="ancestor::DataSet[1]/@name != ''">
                    <xsl:value-of select="ancestor::DataSet[1]/@name"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="append_or_not">
            <xsl:choose>
                <xsl:when test="count(descendant::Link) + count(descendant::Join) > 0">
                    <xsl:value-of select="concat('index_append=1', $newLine)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('index_append=0', $newLine)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="index_script_name">
            <xsl:choose>
                <xsl:when test="@name != ''">
                    <xsl:value-of select="@name"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$dest_db_name"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:value-of select="$newLine"/>
        <xsl:apply-templates select="./descendant::*[name() = 'Command']" mode="SQL"/>
        <xsl:value-of select="$newLine"/>

    </xsl:template>
    
    <xsl:template match="Command" mode="SQL">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="DataSetConfig/DataSet">
        <xsl:apply-templates select="ConnectionType"/>
    </xsl:template>

</xsl:stylesheet>
