<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:ext="http://exslt.org/common">
    <xsl:import href="create_xml_for_preprocess_index.xsl"/>
    <xsl:output method="text" omit-xml-declaration="yes"/>
   
    <xsl:variable name="multi_value_sep" select="'|'"/>

    <xsl:template match="text()"/>

    <xsl:template match="/">
        <xsl:variable name="preProcessXMLFor_truncateField">
            <xsl:apply-templates mode="preprocess_index"/>
        </xsl:variable>
        <xsl:apply-templates select="ext:node-set($preProcessXMLFor_truncateField)/*"/>
    </xsl:template>

    <!-- Template to process Join Tag in DataItem Node-->
    <xsl:template match="Join">
        <xsl:apply-templates select="Query"/>
    </xsl:template>

    <!-- Template to process Join Tag in DataItem Node-->
    <xsl:template match="Link">
        <xsl:param name="parentKey"/>

        <xsl:variable name="File_Source_Type">
            <xsl:choose>
                <xsl:when test="descendant::DataItem[1]/@fileSource != ''">
                    <xsl:value-of select="descendant::DataItem[1]/@fileSource"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:apply-templates select="Query">
            <xsl:with-param name="File_Source_Type" select="$File_Source_Type"/>
        </xsl:apply-templates>

        <xsl:apply-templates select="Join"/>

        <xsl:apply-templates select="DataItem"/>

    </xsl:template>

    <xsl:template name="validate_linked_node_by">
        <xsl:param name="linked_by_val"/>
        <xsl:variable name="Current_Name">
            <xsl:value-of select="ancestor-or-self::DataItem[1]/@name"/>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$linked_by_val = $Current_Name">
                <xsl:choose>
                    <xsl:when test="name(parent::*) != 'Linked'">
                        <xsl:message terminate="yes">
                            <xsl:value-of
                                select="concat('DataItem cannot have same name as Linked Node by value unless that DataItem is child of Linked Node for DataItem ', $Current_Name, $newLine)"
                            />
                        </xsl:message>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

    <!-- Template to process DataItem in any level-->
    <xsl:template match="DataItem">

        <xsl:variable name="DataItem_Type">
            <xsl:value-of select="@type"/>
        </xsl:variable>

        <xsl:choose>
            <xsl:when
                test="./preceding::*[name() = 'DataItem']/@name = @name or ./ancestor::*[name() = 'Linked']/@by = @name">
                <!-- Compare type of current type and earlier types -->
                <!-- Give Error if current type and any of existing types is different -->
                <xsl:for-each select="key('DataItems_By_Name', @name)">
                    <xsl:choose>
                        <xsl:when test="$DataItem_Type != @type">
                            <xsl:message terminate="yes">
                                <xsl:value-of
                                    select="concat('Type should be same for DataItems with same name for DataItem ', @name, $newLine)"
                                />
                            </xsl:message>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
            <xsl:when
                test="./following::*[name() = 'DataItem']/@name = @name or ./ancestor::*[name() = 'Linked']/@by = @name">
                <!-- Compare type of current type and earlier types -->
                <!-- Give Error if current type and any of existing types is different -->
                <xsl:for-each select="key('DataItems_By_Name', @name)">
                    <xsl:choose>
                        <xsl:when test="$DataItem_Type != @type">
                            <xsl:message terminate="yes">
                                <xsl:value-of
                                    select="concat('Type should be same for DataItems with same name for DataItem ', @name, $newLine)"
                                />
                            </xsl:message>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>

        <xsl:variable name="Linked_By">
            <xsl:value-of select="ancestor::*[name() = 'DataSet']/@Linked_By_Value"/>
        </xsl:variable>

        <xsl:call-template name="validate_linked_node_by">
            <xsl:with-param name="linked_by_val" select="$Linked_By"/>
        </xsl:call-template>

        <xsl:apply-templates select="Join"/>
        <xsl:apply-templates select="DataItem"/>
        <xsl:apply-templates select="Linked"/>

    </xsl:template>

    <xsl:template match="Linked">
        <xsl:choose>
            <xsl:when test="@by = ''">
                <xsl:message terminate="yes">
                    <xsl:text>by attribute is mandatory on Linked Node.</xsl:text>
                </xsl:message>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="ancestor::DataSet[1][@DataItem_same_as_linked = 'true']">
                <!-- No need to make it in Linked as indexing line will be made in DataItem -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat(@by, ': ', 'link=', @by, $newLine)"/>
            </xsl:otherwise>
        </xsl:choose>

        <xsl:apply-templates select="DataItem"/>
        <xsl:apply-templates select="Link"/>
    </xsl:template>

    <xsl:template match="Query">
        <xsl:apply-templates select="Linked"/>

        <xsl:apply-templates select="DataItem"/>

        <xsl:apply-templates select="FileItem"/>
    </xsl:template>

    <xsl:template match="FileItem">
        <xsl:variable name="FileIndex">
            <xsl:value-of select="concat(@fisc_mapped, ': field', $newLine)"/>
        </xsl:variable>

        <xsl:value-of select="$FileIndex"/>
        <xsl:apply-templates select="DataItem"/>
    </xsl:template>

    <xsl:template match="FileShare">
        <xsl:apply-templates select="DataItem"/>
    </xsl:template>

    <xsl:template match="HDFSFileShare">
        <xsl:apply-templates select="DataItem"/>
    </xsl:template>

    <xsl:template match="EMAIL_EXCHANGE">
        <xsl:apply-templates select="DataItem"/>
    </xsl:template>

    <xsl:template match="ConnectionType">

        <xsl:apply-templates select="ODBC"/>

        <xsl:apply-templates select="FileShare"/>

        <xsl:apply-templates select="HDFSFileShare"/>

        <xsl:apply-templates select="Sql"/>
        <xsl:apply-templates select="EMAIL_EXCHANGE"/>
    </xsl:template>

    <xsl:template name="Audit_Items">
        <xsl:variable name="Current_Node_Name">
            <xsl:value-of select="name(.)"/>
        </xsl:variable>

        <xsl:variable name="Append_If_Subdoc">
            <xsl:choose>
                <xsl:when test="descendant::Linked[1]/@by != ''">
                    <xsl:value-of select="concat('{', descendant::Linked[1]/@by, '}')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        
        <xsl:variable name="eic_process"
            select="concat('Stream_processMsg_process: field=eic_process', $Append_If_Subdoc)"/>
        <xsl:variable name="eic_ni_code"
            select="concat('Stream_niMsg_niCode: field=eic_nonindexed_code', $Append_If_Subdoc)"/>
        <xsl:variable name="eic_ni_error"
            select="concat('Stream_niMsg_error: field=eic_nonindexed_error', $Append_If_Subdoc)"/>
        <xsl:variable name="eic_ni_friendly"
            select="concat('Stream_niMsg_friendly: field=eic_nonindexed_friendly', $Append_If_Subdoc)"/>
        <xsl:variable name="eic_audit_status"
            select="concat('Stream_auditMsg_status: field=eic_audit_status', $Append_If_Subdoc, ' boolean=',$FISC_EIC_AUDIT_STATUS, $Append_If_Subdoc)"/>
        <xsl:variable name="eic_protobuf"
            select="concat('Stream_protobuf: field=eic_protobuf', $Append_If_Subdoc)"/>
        <xsl:variable name="eic_embed"
            select="concat('Stream_fileMsg_embedded: field=eic_embedded', $Append_If_Subdoc, ' boolean=',$FISC_EMBEDDED_FILE, $Append_If_Subdoc)"/>
        <xsl:variable name="eic_file_ext"
            select="concat('Stream_fileMsg_fileExt: field=eic_file_ext', $Append_If_Subdoc)"/>
        <xsl:value-of
            select="concat($eic_process, $newLine, $eic_ni_code, $newLine, $eic_ni_error, $newLine, $eic_ni_friendly, $newLine, $eic_audit_status, $newLine, $eic_protobuf, $newLine, $eic_embed, $newLine, $eic_file_ext, $newLine)"
        />
    </xsl:template>

    <xsl:template match="ODBC">
        <xsl:apply-templates select="Query"/>
    </xsl:template>

    <xsl:template match="Sql">
        <xsl:apply-templates select="Query"/>
    </xsl:template>

    <xsl:template match="DataSet">
        <xsl:variable name="Audit_String">
            <xsl:call-template name="Audit_Items"/>
        </xsl:variable>
        <xsl:variable name="list_icon_dataitems">
            <xsl:apply-templates select="./descendant-or-self::*[name() = 'Icon']" mode="Icon_items"/>
        </xsl:variable>
        <xsl:variable name="list_alias_eic_ref">
            <xsl:apply-templates mode="Alias_Eic" select="./descendant-or-self::*[name() = 'DataItem']"/>
        </xsl:variable>
                
        <xsl:for-each select="//*[generate-id(.) = generate-id(key('DataItems_By_Name', @name)[1])]">
            <xsl:variable name="delimited_name">
                <xsl:value-of select="concat('~',@name,'~')"/>
            </xsl:variable>
            <xsl:variable name="Icon_flag">
                <xsl:choose>
                    <xsl:when test="contains($list_icon_dataitems,$delimited_name)">
                        <xsl:value-of select="true()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="false()"/>
                    </xsl:otherwise>
                </xsl:choose>	
            </xsl:variable>
            <xsl:variable name="alias_and_eic_flag">
                <xsl:choose>
                    <xsl:when test="contains($list_alias_eic_ref,$delimited_name)">
                        <xsl:value-of select="true()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="false()"/>
                    </xsl:otherwise>
                </xsl:choose>    
            </xsl:variable>
            <!--Do something with the unique list of elements-->
            <xsl:call-template name="processDataItem">
                <xsl:with-param name="Icon" select="$Icon_flag"/>
                <xsl:with-param name="alias_and_eic" select="$alias_and_eic_flag"/>
            </xsl:call-template>
        </xsl:for-each>

        <xsl:apply-templates select="ConnectionType"/>
        <xsl:value-of select="$Audit_String"/>
        <xsl:value-of select="$newLine"/>

    </xsl:template>

    <xsl:template name="validate_same_split_for_same_DataItem">
        <xsl:variable name="split_char">
            <xsl:value-of select="@split"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="./preceding::*[name() = 'DataItem']/@name = @name">
                <!-- Compare split of current DataItem and earlier DataItems with same name -->
                <!-- Give Error if current split and any of preceding split is different -->
                <xsl:for-each select="key('DataItems_By_Name', @name)">
                    <xsl:choose>
                        <xsl:when test="$split_char != @split">
                            <xsl:message terminate="yes">
                                <xsl:value-of
                                    select="concat('Split should be same for DataItems with same name for DataItem ', @name, $newLine)"
                                />
                            </xsl:message>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="./following::*[name() = 'DataItem']/@name = @name">
                <!-- Compare split of current DataItem and earlier DataItems with same name -->
                <!-- Give Error if current split and any of preceding split is different -->
                <xsl:for-each select="key('DataItems_By_Name', @name)">
                    <xsl:choose>
                        <xsl:when test="$split_char != @split">
                            <xsl:message terminate="yes">
                                <xsl:value-of
                                    select="concat('Split should be same for DataItems with same name for DataItem ', @name, $newLine)"
                                />
                            </xsl:message>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="processDataItem">
        <xsl:param name="Icon"/>
        <xsl:param name="alias_and_eic"/>
        
        <xsl:call-template name="validate_same_split_for_same_DataItem"/>
        
        <xsl:variable name="parentKey">
            <xsl:value-of select="ancestor::Linked[1]/@by"/>
        </xsl:variable>

        <xsl:variable name="childOfLink">
            <xsl:choose>
                <xsl:when test="not(ancestor::Link)">
                    <xsl:value-of select="'NO'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'YES'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="field_icon_alias">
            <xsl:choose>
                <xsl:when test="$Icon='true'">
                    <xsl:call-template name="make_field_alias_for_icon">
                        <xsl:with-param name="dataitem_name" select="@name"/>
                        <xsl:with-param name="alias" select="@alias"/>
                        <xsl:with-param name="typefield" select="@typefield"/>
                        <xsl:with-param name="childOfLink" select="$childOfLink"/>
                        <xsl:with-param name="parentKey" select="$parentKey"/>                        
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="add_icon">
            <xsl:choose>
                <xsl:when test="string-length($field_icon_alias) &gt; 0">
                    <xsl:value-of select="concat(' ',$field_icon_alias,' ')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$field_icon_alias"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="display">
            <xsl:call-template name="get_display"/>
        </xsl:variable>

        <xsl:variable name="filter">
            <xsl:call-template name="get_filter"/>
        </xsl:variable>

        <xsl:variable name="filter_before_boolean">
            <xsl:value-of select="normalize-space(substring-before($filter, 'boolean'))"/>
        </xsl:variable>

        <xsl:variable name="filter_after_boolean">
            <xsl:value-of select="normalize-space(substring-after($filter, 'boolean'))"/>
        </xsl:variable>

        <xsl:variable name="filter_composite_string">
            <xsl:choose>
                <xsl:when test="contains($filter, 'boolean')">
                    <xsl:choose>
                        <xsl:when test="string-length($filter_before_boolean) &gt; 0">
                            <xsl:value-of
                                select="concat($filter_before_boolean, ' ', $HASH, ' boolean', $filter_after_boolean)"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat(' ', $HASH, ' boolean', $filter_after_boolean)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$filter"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="split_string">
            <xsl:call-template name="get_split_string"/>
        </xsl:variable>

        <xsl:variable name="Linked_added_to_final_index">
            <xsl:call-template name="get_linked_index"/>
        </xsl:variable>

        <xsl:variable name="parsedate">
            <xsl:call-template name="get_parse_date"/>
        </xsl:variable>

        <xsl:variable name="indexed">
            <xsl:call-template name="get_indexed"/>
        </xsl:variable>

        <xsl:variable name="dedup">
            <xsl:call-template name="get_dedup"/>
        </xsl:variable>

        <xsl:variable name="unique">
            <xsl:call-template name="get_unique"/>
        </xsl:variable>

        <xsl:variable name="add_to_unique">
            <xsl:call-template name="get_add_to_unique"/>
        </xsl:variable>

        <xsl:variable name="LineName_var">
            <xsl:call-template name="get_line_name"/>
        </xsl:variable>

        <xsl:variable name="LineDesc_pre">
            <xsl:value-of
                select="normalize-space(concat('~',$split_string,'~',$Linked_added_to_final_index,'~', $parsedate,'~', $indexed,'~',$dedup,'~',$display,'~',$add_icon,'~',$filter_composite_string,'~', $unique,'~'))"
            />
        </xsl:variable>
        
        <xsl:variable name="LineDesc">
            <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$LineDesc_pre"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="' '"/>
            </xsl:call-template>    
        </xsl:variable>
 
        <xsl:variable name="First_part_for_Unique_key">
            <xsl:if test="contains($unique, 'unique=Q')">
                <xsl:value-of
                    select="normalize-space(concat($split_string, ' ', $HASH, ' ', $unique, $filter, $add_to_unique))"
                />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="Second_part_for_Unique_key">
            <xsl:if test="contains($unique, 'unique=Q')">
                <xsl:value-of
                    select="normalize-space(concat($split_string, $indexed, $display, $dedup))"/>
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="Num_Fields_Line">
            <xsl:call-template name="GetNoOfOccurance">
                <xsl:with-param name="String" select="$LineDesc"/>
                <xsl:with-param name="SubString" select="'#field#'"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="Curated_Line_1">
            <xsl:choose>
                <xsl:when test="$Num_Fields_Line &gt; 1">
                    <xsl:call-template name="replace-string">
                        <xsl:with-param name="text" select="$LineDesc"/>
                        <xsl:with-param name="replace" select="'#field#'"/>
                        <xsl:with-param name="with" select="''"/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="Curated_Line_2">
            <xsl:choose>
                <xsl:when test="string-length($Curated_Line_1) &gt; 0">
                    <xsl:value-of select="normalize-space(concat('#field#', $Curated_Line_1))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space($LineDesc)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="Final_Index_Output">
            <xsl:call-template name="Remove_Dup_Massage_String">
                <xsl:with-param name="Unique_String_1" select="$First_part_for_Unique_key"/>
                <xsl:with-param name="Unique_String_2" select="$Second_part_for_Unique_key"/>
                <xsl:with-param name="LineName" select="$LineName_var"/>
                <xsl:with-param name="LineDescPost2" select="$Curated_Line_2"/>
                <xsl:with-param name="unique" select="$unique"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="Num_Fields_Final">
            <xsl:call-template name="GetNoOfOccurance">
                <xsl:with-param name="String" select="$Final_Index_Output"/>
                <xsl:with-param name="SubString" select="'field'"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="Output_without_field_duplicates">
            <xsl:choose>
                <xsl:when test="$Num_Fields_Final &gt; 1">
                    <xsl:call-template name="replace-string">
                        <xsl:with-param name="text" select="$Final_Index_Output"/>
                        <xsl:with-param name="replace" select="'#field#'"/>
                        <xsl:with-param name="with" select="' '"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="replace-string">
                        <xsl:with-param name="text" select="$Final_Index_Output"/>
                        <xsl:with-param name="replace" select="'#field#'"/>
                        <xsl:with-param name="with" select="' field '"/>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="Output_remove_crosshatch">
            <xsl:call-template name="replace-string">
                <xsl:with-param name="text" select="$Output_without_field_duplicates"/>
                <xsl:with-param name="replace" select="'#field#'"/>
                <xsl:with-param name="with" select="' field '"/>
            </xsl:call-template>
        </xsl:variable>

        <!-- No need to add empty line like DataItem: in index script-->
        <xsl:variable name="output_string_after_colon">
            <xsl:value-of select="substring-after($Output_remove_crosshatch, ':')"/>
        </xsl:variable>
        
        <xsl:call-template name="validate_index_output">
            <xsl:with-param name="output" select="$Output_remove_crosshatch"/>
        </xsl:call-template>
        
        <xsl:variable name="display_file_ref">
            <xsl:call-template name="get_display_alias_ref">
                <xsl:with-param name="Icon" select="$Icon"/>
            </xsl:call-template>    
        </xsl:variable>
        
        <xsl:variable name="display_to_be_replaced">
            <xsl:choose>
                <xsl:when test="string-length($display_file_ref) &gt; 0">
                    <xsl:value-of select="concat($display,$display_file_ref)"/>
                </xsl:when>
            </xsl:choose>    
        </xsl:variable>
        
        <xsl:variable name="output_if_string_replaced">
            <xsl:if test="string-length(normalize-space($output_string_after_colon)) &gt; 0 and string-length($display_file_ref) &gt; 0">
                <xsl:call-template name="replace-string">
                    <xsl:with-param name="text" select="$Output_remove_crosshatch"/>
                    <xsl:with-param name="replace" select="$display"/>
                    <xsl:with-param name="with" select="$display_to_be_replaced"/>
                </xsl:call-template>                
            </xsl:if>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="string-length($output_if_string_replaced) &gt; 0">
                <xsl:value-of select="concat($output_if_string_replaced,$newLine)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="string-length(normalize-space($output_string_after_colon)) &gt; 0">
                    <xsl:value-of select="concat($Output_remove_crosshatch, $newLine)"/>
                </xsl:if>    
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="DataSetConfig">
        <xsl:apply-templates select="DataSet"/>
    </xsl:template>

</xsl:stylesheet>
