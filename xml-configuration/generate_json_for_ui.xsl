<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:ext="http://exslt.org/common">
  <xsl:import href="create_xml_for_preprocess_ui.xsl"/>
     
  <xsl:key name="URL_By_DataItem_Name" match="URL" use="@name_of_dataitem"/>
  
  <xsl:output method="text" indent="no"/>

  <xsl:template match="text()"/>
  
  <xsl:template match="/">
    <xsl:variable name="preProcessXMLForDuplicates">
      <xsl:apply-templates mode="preprocess_ui"/>
    </xsl:variable>

    <xsl:apply-templates select="ext:node-set($preProcessXMLForDuplicates)/*"/>
  </xsl:template>

  <xsl:template match="/DataSetConfig">
    <xsl:apply-templates select="DataSet"/>
  </xsl:template>

  <xsl:template match="DataSet">

    <xsl:variable name="dbname">
      <xsl:value-of select="@db"/>
    </xsl:variable>

    <xsl:variable name="db_desc">
      <xsl:choose>
        <!-- if description is greater than the Max_len_desc -->
        <xsl:when test="string-length(@description) &gt; $Max_len_desc">
          <xsl:value-of select="substring(@description, 0, $Max_len_desc)"/>
        </xsl:when>
        <xsl:otherwise>
          <!-- otherwise print out the whole, un-truncated string -->
          <xsl:value-of select="@description"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$dbname = ''">
        <xsl:message terminate="yes">
          <xsl:text>db attribute is mandatory on DataSet Node.</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="$db_desc = ''">
        <xsl:message terminate="yes">
          <xsl:text>description attribute is mandatory on DataSet Node.</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="title">
      <xsl:value-of select="@title"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$title = ''">
        <xsl:message terminate="yes">
          <xsl:text>title attribute is mandatory on DataSet Node.</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>
    
    <xsl:call-template name="AllDataItems">
      <xsl:with-param name="sec_slot" select="$SecuritySlot"/>
      <xsl:with-param name="db" select="$dbname"/>
      <xsl:with-param name="db_description" select="$db_desc"/>
      <xsl:with-param name="title" select="$title"/>
    </xsl:call-template>   
  </xsl:template>

  
  <xsl:template match="Icon" mode="icon">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:value-of select="$Icon_tag"/>
    
    <xsl:variable name="parse_value">
    <xsl:choose>
      <xsl:when test="@parent_Node_Icon='DATAITEM' or @parent_Node_Icon='DISPLAY' or @parent_Node_Icon='PREVIEW'">
        <xsl:choose>
          <xsl:when test="@parse='true'">
            <xsl:value-of select="ancestor::DataItem[1]/@name"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="string-length(@parse) &gt; 0 and count(key('DataItems_By_Name', @parse)) &lt; 1">
              <xsl:message terminate="yes">
                <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has invalid parse attribute.',$newLine)"/>
              </xsl:message>  
            </xsl:if>
            <xsl:value-of select="@parse"/>
          </xsl:otherwise>
        </xsl:choose>  
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="string-length(@parse) &gt; 0">
            <xsl:if test="count(key('DataItems_By_Name', @parse)) &lt; 1">
              <xsl:message terminate="yes">
                <xsl:value-of select="concat(ancestor::DataSet[1]/@name, ' DataSet has invalid parse attribute.',$newLine)"/>
              </xsl:message>  
            </xsl:if>
            <xsl:value-of select="@parse"/>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>      
    </xsl:choose>
    </xsl:variable>
    
    <xsl:if test="string-length($parse_value) &gt; 0 and string-length(@parse) &gt; 0">          
      <xsl:value-of select="concat($double_quotes,'parse',$double_quotes)"/>
      <xsl:value-of select="concat(' : ',$double_quotes,$parse_value,$double_quotes,',')"/>
    </xsl:if>
    
    <!-- "default" : "fal fa-suitcase" }, -->
    <xsl:value-of select="concat($double_quotes,'default',$double_quotes)"/>
    <xsl:value-of select="concat(' : ',$double_quotes,@full_name_with_style_code,$double_quotes,'}')"/>    
    
  </xsl:template>
  
  <xsl:template match="DataItem[@metadata='true']" mode="metadata_info">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    
    <xsl:choose>
      <xsl:when test="not(@title) or @title = ''">
        <xsl:message terminate="yes">
          <xsl:value-of select="concat('title is mandatory as metadata is true on DataItem ',@name,$newLine)"/>
        </xsl:message>
      </xsl:when>
      <xsl:otherwise>        
        <xsl:value-of select="concat('{',$double_quotes,'title',$double_quotes,' : ')"/>
        <xsl:value-of select="concat($double_quotes,@title,$double_quotes)"/>
        <xsl:choose>
          <xsl:when test="not(@desc) or @desc=''">
            <xsl:value-of select="'},'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat(',',$double_quotes,'description',$double_quotes,' : ')"/>
            <xsl:value-of select="concat($double_quotes,@desc,$double_quotes,'},')"/>    
          </xsl:otherwise>
        </xsl:choose>        
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template match="DataItem">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>

    <xsl:apply-templates select="Filter"/>

  </xsl:template>

  <xsl:template match="Filter">

    <xsl:apply-templates select="Boolean | Hierarchical"/>

    <xsl:apply-templates select="SearchWithinResults"/>

    <xsl:apply-templates select="DateRangeField[not(ancestor::*[name() = 'Link'])]"/>

  </xsl:template>
  
  <xsl:template match="Highlight">
    
    <xsl:variable name="One_highlight_field">
      <xsl:call-template name="HighlightTextQuery"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$One_highlight_field != ''">
        <xsl:value-of select="concat($One_highlight_field,',')"/>
      </xsl:when>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template match="ExportToReport[@makeEntryUI = 'true']">
    
    <!-- only process if parent is DataItem-->
    <xsl:choose>
      <xsl:when test="name(parent::*) = 'DataItem'">
        <xsl:variable name="One_exportReport_field">
          <xsl:call-template name="ExportReportTextQuery"/>
        </xsl:variable>
        
        <xsl:choose>
          <xsl:when test="$One_exportReport_field != ''">
            <xsl:value-of select="concat($One_exportReport_field, ',')"/>
          </xsl:when>
        </xsl:choose>    
      </xsl:when>
    </xsl:choose>
   
  </xsl:template>

  <xsl:template match="DateRangeField[@makeEntryUI = 'true']">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>

    <xsl:variable name="Slot_dateRange_same_Name">
      <xsl:value-of
        select="//DataItem[@name = $Current_dataItem]/Filter/DateRangeField[@IsitFirstNode = 'true']/@slot"
      />
    </xsl:variable>


    <xsl:variable name="One_daterange_field">
      <xsl:call-template name="dateRangeTextQuery">
        <xsl:with-param name="Slot" select="$Slot_dateRange_same_Name"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$One_daterange_field != ''">
        <xsl:value-of select="concat($One_daterange_field, ',')"/>
      </xsl:when>
    </xsl:choose>


  </xsl:template>

  <xsl:template match="SearchWithinResults[@makeEntryUI = 'true']">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>

    <xsl:variable name="Prefix_SearchWithin_same_Name">
      <xsl:value-of
        select="//DataItem[@name = $Current_dataItem]/Filter/SearchWithinResults[@IsitFirstNode = 'true']/@termPrefix"
      />
    </xsl:variable>

    <xsl:call-template name="SearchWithinResultsQuery">
      <xsl:with-param name="Prefix" select="$Prefix_SearchWithin_same_Name"/>
      <xsl:with-param name="dataitem_name" select="$Current_dataItem"/>      
    </xsl:call-template>

  </xsl:template>

  <xsl:template match="Boolean | Hierarchical[@makeEntryUI = 'true']">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>


    <xsl:variable name="Prefix_Boolean_same_Name">
      <xsl:value-of
        select="ancestor-or-self::DataSet[1]//DataItem[@name = $Current_dataItem]/Filter/Boolean[@IsitFirstNode = 'true']/@termPrefix"
      />
    </xsl:variable>

    <xsl:variable name="Prefix_Hierarchy_same_Name">
      <xsl:value-of
        select="ancestor-or-self::DataSet[1]//DataItem[@name = $Current_dataItem]/Filter/Hierarchical[@IsitFirstNode = 'true']/@termPrefix"
      />
    </xsl:variable>

    <xsl:call-template name="FilterQuery">
      <xsl:with-param name="templateName" select="local-name()"/>
      <xsl:with-param name="Bool_Prefix" select="$Prefix_Boolean_same_Name"/>
      <xsl:with-param name="Hierarchy_Prefix" select="$Prefix_Hierarchy_same_Name"/>
    </xsl:call-template>

  </xsl:template>

  <!-- this is to process sort inside DataItem tag -->
  <xsl:template match="Sort[@makeEntryUI = 'true']">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="Current_dataItem">
      <xsl:value-of select="ancestor::DataItem[1]/@name"/>
    </xsl:variable>

    <xsl:variable name="Slot_Sort_same_Name">
      <xsl:value-of
        select="//DataItem[@name = $Current_dataItem]/Filter/Sort[@IsitFirstNode = 'true']/@slot"/>
    </xsl:variable>

    <xsl:variable name="One_Sort_field">
      <xsl:call-template name="sortText">
        <xsl:with-param name="slot" select="$Slot_Sort_same_Name"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$One_Sort_field != ''">
        <xsl:value-of select="concat($One_Sort_field, ',')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Metadata[@makeEntryUI = 'true']">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="one_metadata">
      <xsl:call-template name="MetadataFieldText"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$one_metadata != ''">
        <xsl:value-of select="concat($one_metadata, ',')"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <xsl:template match="Details[@makeEntryUI = 'true']">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="one_detail">
      <xsl:call-template name="ResultDetailText"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$one_detail != ''">
        <xsl:value-of select="concat($one_detail, ',')"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>
  
  <xsl:template match="DataItem" mode="validate_hierarchical_filter">
    <xsl:variable name="num_labels_current_item">
      <xsl:value-of select="@num_labels"/>
    </xsl:variable>
    <xsl:variable name="Label_value">
      <xsl:value-of select="child::*[name() = 'Filter']/Hierarchical/Labels/@Stripped_All_labels"/>
    </xsl:variable>
    
    <xsl:if test="@num_labels &gt; 0">
      <xsl:choose>
        <xsl:when test="@name = following::DataItem/@name">
          <xsl:for-each select="key('DataItems_By_Name', @name)">
            <xsl:choose>
              <xsl:when test="$num_labels_current_item != @num_labels">
                <xsl:message terminate="yes">
                    <xsl:value-of select="concat(@name,' DataItem has different number of labels on Hierarchical Filters for same name',$newLine)"/>                  
                </xsl:message>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="$Label_value != child::*[name() = 'Filter']/Hierarchical/Labels/@Stripped_All_labels">
                <xsl:message terminate="yes">
                  <xsl:value-of select="concat(@name,' DataItem has different value of labels on Hierarchical Filters for same name',$newLine)"/>
                </xsl:message>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>                         
        </xsl:when>
      </xsl:choose>      
    </xsl:if>
  </xsl:template>

  <xsl:template name="AllDataItems">
    <xsl:param name="sec_slot"/>
    <xsl:param name="db"/>
    <xsl:param name="db_description"/>
    <xsl:param name="title"/>

    <xsl:value-of select="concat('&quot;', $db, '&quot;', ':{')"/>
    <xsl:value-of
      select="concat('&quot;', 'title', '&quot;', ' : ', '&quot;', $title, '&quot;', ',')"/>

    <xsl:value-of
      select="concat('&quot;', 'description', '&quot;', ' : ', '&quot;', $db_description, '&quot;')"/>
    
    <xsl:apply-templates select="./descendant::*[name() = 'DataItem']" mode="validate_hierarchical_filter"/>
    
    <xsl:call-template name="make_metadata_info_section"/>

    <xsl:call-template name="date_main_level"/>

    <xsl:call-template name="search_within"/>

    <xsl:call-template name="filter_all"/>

    <xsl:call-template name="sort_main"/>

    <xsl:text>, "results" : {</xsl:text>
    
    <xsl:variable name="ExportReport_Main">
      <xsl:call-template name="processExportReport">        
        <xsl:with-param name="export_report_condition" select="./descendant::*[name() = 'ExportToReport' and not(ancestor::*[name() = 'Link'])][@makeEntryUI = 'true']"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Highlight_Main_Normal">
      <xsl:call-template name="processHighlight">
        <xsl:with-param name="highlight_condition" select="./descendant::*[name() = 'Highlight' and not(ancestor::*[name() = 'Link']) and not(ancestor::*[name() = 'Preview'])]"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Highlight_Main_Preview">
      <xsl:call-template name="processHighlight">
        <xsl:with-param name="highlight_condition" select="./descendant::*[name() = 'Highlight' and not(ancestor::*[name() = 'Link']) and ancestor::*[name() = 'Preview']]"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Icon_Main_Display">
      <xsl:call-template name="process_Icon">
        <xsl:with-param name="ICON_LEVEL" select="'MAIN_LEVEL'"/>
        <xsl:with-param name="FIRST_PARENT" select="'DATASET'"/>
        <xsl:with-param name="SECOND_PARENT" select="'DATAITEM'"/>
        <xsl:with-param name="DISPLAY_OR_PREVIEW" select="'DISPLAY'"/>
        <xsl:with-param name="Exclude_Flag" select="'N'"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Icon_Main_Preview">
      <xsl:call-template name="process_Icon">
        <xsl:with-param name="ICON_LEVEL" select="'MAIN_LEVEL'"/>
        <xsl:with-param name="FIRST_PARENT" select="'DATASET'"/>
        <xsl:with-param name="SECOND_PARENT" select="'DATAITEM'"/>
        <xsl:with-param name="DISPLAY_OR_PREVIEW" select="'PREVIEW'"/>
        <xsl:with-param name="Exclude_Flag" select="'Y'"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="MetadataMain_Normal">
      <xsl:call-template name="process_metadata_node">
        <xsl:with-param name="metadata_condition"
          select="./descendant::*[name() = 'Metadata' and not(ancestor::*[name() = 'Link']) and not(ancestor::*[name() = 'Preview'])][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="MetadataMain_Preview">
      <xsl:call-template name="process_metadata_node">
        <xsl:with-param name="metadata_condition"
          select="./descendant::*[name() = 'Metadata' and not(ancestor::*[name() = 'Link']) and ancestor::*[name() = 'Preview']]"
        />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Detail_Main">
      <xsl:call-template name="process_detail_node">
        <xsl:with-param name="detail_condition"
          select="./descendant::*[name() = 'Details' and not(ancestor::*[name() = 'Link']) and not(ancestor::*[name() = 'Preview'])][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Detail_Preview">
      <xsl:call-template name="process_detail_node">
        <xsl:with-param name="detail_condition"
          select="./descendant::*[name() = 'Details' and not(ancestor::*[name() = 'Link']) and ancestor::*[name() = 'Preview']][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>


    <xsl:variable name="Main_Display_Normal">
      <xsl:call-template name="Calculate_Main_Normal">
        <xsl:with-param name="Icon_Main_Normal" select="$Icon_Main_Display"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Main_Display_Preview">
      <xsl:call-template name="Calculate_Main_Preview">
        <xsl:with-param name="Icon_Main_Preview" select="$Icon_Main_Preview"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="AllUICombinedMain">
      <xsl:call-template name="create_Display_Comma_Sep">
        <xsl:with-param name="ExportReport" select="$ExportReport_Main"/>
        <xsl:with-param name="Display_Normal" select="$Main_Display_Normal"/>
        <xsl:with-param name="Metadata_Normal" select="$MetadataMain_Normal"/>
        <xsl:with-param name="Detail_Normal" select="$Detail_Main"/>
        <xsl:with-param name="Metadata_Preview" select="$MetadataMain_Preview"/>
        <xsl:with-param name="Display_Preview" select="$Main_Display_Preview"/>
        <xsl:with-param name="Detail_Preview" select="$Detail_Preview"/>
        <xsl:with-param name="Highlight_Normal" select="$Highlight_Main_Normal"/>
        <xsl:with-param name="Highlight_Preview" select="$Highlight_Main_Preview"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="$AllUICombinedMain"/>

    <xsl:variable name="Link_Count">
      <xsl:value-of select="count(./descendant::*[name() = 'Link'])"/>
    </xsl:variable>

    <xsl:if test="$Link_Count &gt; 0">
      <xsl:value-of select="$Subdoc_Start_Line"/>
      <xsl:value-of select="$Typefield_Line"/>
      <xsl:call-template name="Make_idfield_ui_line">
        <xsl:with-param name="Linked_By" select="./descendant::Linked[1]/@by"/>
      </xsl:call-template>
    </xsl:if>

    <xsl:for-each select="./descendant::*[name() = 'Linked']">
      <xsl:call-template name="Linked_Template">
        <xsl:with-param name="Link_Count" select="$Link_Count"/>
      </xsl:call-template>
    </xsl:for-each>

    <!-- Close Subdocs if it has been opened -->
    <xsl:if test="$Link_Count &gt; 0">
      <xsl:text>}</xsl:text>
    </xsl:if>

    <!-- close bracket for db and result -->
    <xsl:text>}}</xsl:text>
  </xsl:template>
  
  <xsl:template name="process_Icon">
    <xsl:param name="ICON_LEVEL"/>
    <xsl:param name="FIRST_PARENT"/>
    <xsl:param name="SECOND_PARENT"/>
    <xsl:param name="DISPLAY_OR_PREVIEW"/>
    <xsl:param name="Exclude_Flag"/>
    
    <xsl:variable name="Icon_DataSet">
      <xsl:call-template name="process_icon_attribute">
        <xsl:with-param name="icon_condition"
          select="./descendant::*[name() = 'Icon'][@Icon_level=$ICON_LEVEL][@parent_Node_Icon=$FIRST_PARENT]"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Icon_DataItem">
      <xsl:call-template name="process_icon_attribute">
        <xsl:with-param name="icon_condition"
          select="./descendant::*[name() = 'Icon'][@Icon_level=$ICON_LEVEL][@parent_Node_Icon=$SECOND_PARENT]"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Icon_DISP_OR_PREVIEW">
      <xsl:call-template name="process_icon_attribute">
        <xsl:with-param name="icon_condition"
          select="./descendant::*[name() = 'Icon'][@Icon_level=$ICON_LEVEL][@parent_Node_Icon=$DISPLAY_OR_PREVIEW]"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($Icon_DISP_OR_PREVIEW) &gt; 0">
        <xsl:value-of select="$Icon_DISP_OR_PREVIEW"/>  
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="string-length($Icon_DataItem) &gt; 0 and $Exclude_Flag !='Y'">
            <xsl:value-of select="$Icon_DataItem"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="$Exclude_Flag!='Y'">
              <xsl:value-of select="$Icon_DataSet"/>  
            </xsl:if>            
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template name="make_metadata_info_section">    
    <xsl:variable name="metadata_info">
      <xsl:apply-templates select="/descendant::*[name() = 'DataItem'][@metadata = 'true']" mode="metadata_info"/>
    </xsl:variable>
    
    <xsl:variable name="metadata">
      <xsl:choose>
        <xsl:when test="$metadata_info != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$metadata_info"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$metadata !=''">
        <xsl:value-of select="concat(',', $MetadataInfo_tag, $metadata, ']')"/>
      </xsl:when>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template name="processExportReport">
    <xsl:param name="export_report_condition"/>
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    
    <xsl:variable name="exportReport_raw">
      <xsl:apply-templates select="$export_report_condition">
        <xsl:sort select="@position" order="ascending" data-type="number"/>
      </xsl:apply-templates>
    </xsl:variable>
    
    <xsl:variable name="exportReport">
      <xsl:choose>
        <xsl:when test="$exportReport_raw != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$exportReport_raw"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$exportReport != ''">
        <xsl:value-of select="concat($exportReport_tag, $exportReport, ']')"/>
      </xsl:when>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template name="process_detail_node">
    <xsl:param name="detail_condition"/>
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="details_raw">
      <xsl:apply-templates select="$detail_condition">
        <xsl:sort select="@position" order="ascending" data-type="number"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="details">
      <xsl:choose>
        <xsl:when test="$details_raw != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$details_raw"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$details != ''">
        <xsl:value-of select="concat($Details_tag, $details, ']')"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="process_icon_attribute">
    <xsl:param name="icon_condition"/>
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>

    <!-- Terminate if more than 1 icon for this condition -->
    <xsl:variable name="count_icon_init">
      <xsl:value-of select="count($icon_condition)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_icon_init &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text> icon attribute defined on more than 1 DataItem at same level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>
    <!-- There is only 1 DataItem with icon attribute,so make the string -->
    <xsl:apply-templates select="$icon_condition" mode="icon"/>
  </xsl:template>

  <xsl:template name="process_metadata_node">
    <xsl:param name="metadata_condition"/>
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="metadata_raw">
      <xsl:apply-templates select="$metadata_condition">
        <xsl:sort select="@position" data-type="number" order="ascending"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="metadata">
      <xsl:choose>
        <xsl:when test="$metadata_raw != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$metadata_raw"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$metadata != ''">
        <xsl:value-of select="concat($Metadata_tag, $metadata, ']')"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>
  
  <xsl:template name="processHighlight">
    <xsl:param name="highlight_condition"/>
    
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="highlight_raw">
      <xsl:apply-templates select="$highlight_condition"/>       
    </xsl:variable>
    
    <xsl:variable name="highlight">
      <xsl:choose>
        <xsl:when test="$highlight_raw != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$highlight_raw"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$highlight != ''">
        <xsl:value-of select="concat($Highlight_tag, $highlight)"/>
      </xsl:when>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template name="sort_main">
    <xsl:call-template name="validate_one_dataitem_at_main_level"/>
    <xsl:variable name="Sort_MainLevel">
      <xsl:apply-templates
        select="/descendant::*[name() = 'Sort'][@makeEntryUI = 'true'][not(ancestor::*[name() = 'Link'])]">
        <xsl:sort data-type="number" select="@position" order="ascending"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="Sort">
      <xsl:choose>
        <xsl:when test="$Sort_MainLevel != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$Sort_MainLevel"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$Sort != ''">
        <xsl:value-of select="concat(',', $Sort_tag, $Sort, ']')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="filter_all">
    <xsl:variable name="Filter_Results">
      <xsl:apply-templates
        select="/descendant::*[name() = 'DataItem'][Filter/Boolean[@makeEntryUI = 'true'] or Filter/Hierarchical[@makeEntryUI = 'true']]">
        <xsl:sort select="@filterPosition" data-type="number" order="ascending"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="Filter">
      <xsl:choose>
        <xsl:when test="$Filter_Results != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$Filter_Results"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$Filter != ''">
        <xsl:value-of select="concat(',', $Filter_tag, $Filter, ']')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="date_main_level">
    <xsl:variable name="DateRange_MainLevel">
      <xsl:apply-templates
        select="/descendant::*[name() = 'DateRangeField'][@makeEntryUI = 'true'][not(ancestor::*[name() = 'Link'])]">
        <xsl:sort order="ascending" select="@position" data-type="number"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="DateRange">
      <xsl:choose>
        <xsl:when test="$DateRange_MainLevel != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$DateRange_MainLevel"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$DateRange != ''">
        <xsl:value-of select="concat(',', $DateRange_tag, $DateRange, ']')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="search_within">
    <xsl:variable name="SearchWithin_result">
      <xsl:apply-templates
        select="/descendant::*[name() = 'SearchWithinResults'][@makeEntryUI = 'true']">
        <xsl:sort select="@position" data-type="number" order="ascending"/>
      </xsl:apply-templates>
    </xsl:variable>

    <xsl:variable name="Search">
      <xsl:choose>
        <xsl:when test="$SearchWithin_result != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$SearchWithin_result"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$Search != ''">
        <xsl:value-of select="concat(',', $SearchWithin_tag, $Search, ']')"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="SearchWithinResultsText">
    <xsl:param name="Prefix"/>
    <xsl:param name="dataitem_name"/>
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable>
    <!-- Figure out title and desc values to use -->
    <xsl:variable name="searchWithinParent">
      <xsl:choose>
        <xsl:when test="@parent_flag_to_use = 'true'">
          <xsl:value-of select="concat(',', '&quot;', 'parent', '&quot;', ' : ', 'true')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="''"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="concat('{',$double_quotes,'name',$double_quotes,' : ','&quot;', $Field_name_to_use, '&quot;',',')"/>
    <xsl:call-template name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template>
    <xsl:value-of
      select="concat('&quot;', 'termPrefix', '&quot;', ':', '&quot;', $Prefix, '&quot;')"/>
    <xsl:value-of select="$searchWithinParent"/>
    <!-- Make onChangeTransform and onBlurTransform and Match elements -->
    <xsl:variable name="OnChange_item">
      <xsl:apply-templates select="ancestor-or-self::DataSet[1]//OnChange[@parent_dataitem=$dataitem_name]" mode="OnChangeMode"/>
    </xsl:variable>
    <xsl:if test="string-length($OnChange_item) &gt; 0">      
      <xsl:value-of select="concat(',',$double_quotes,'onChangeTransform',$double_quotes,': [')"/>
      <xsl:call-template name="strip-end-comma">
        <xsl:with-param name="text" select="$OnChange_item"/>
      </xsl:call-template>
      <xsl:value-of select="']'"/>
    </xsl:if>
    <xsl:variable name="OnBlur_item">
      <xsl:apply-templates select="ancestor-or-self::DataSet[1]//OnBlur[@parent_dataitem=$dataitem_name]" mode="OnBlurMode"/>
    </xsl:variable>
    <xsl:if test="string-length($OnBlur_item) &gt; 0">      
      <xsl:value-of select="concat(',',$double_quotes,'onBlurTransform',$double_quotes,': [')"/>
      <xsl:call-template name="strip-end-comma">
        <xsl:with-param name="text" select="$OnBlur_item"/>
      </xsl:call-template>
      <xsl:value-of select="']'"/>
    </xsl:if>
    <xsl:variable name="Transform_match">
      <xsl:apply-templates select="ancestor-or-self::DataSet[1]//Match[@parent_dataitem=$dataitem_name]" mode="OnMatch"/>
    </xsl:variable>
    <xsl:variable name="unique_match">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$Transform_match"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="$split_ext"/>
      </xsl:call-template>  
    </xsl:variable>
    <xsl:variable name="match_after_delimiter">
      <xsl:choose>
        <xsl:when test="string-length(substring-after($unique_match,$split_ext)) &gt; 0">
          <xsl:value-of select="substring-after($unique_match,$split_ext)"/>  
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="string-length($unique_match) &gt; 0">
              <xsl:choose>
                <xsl:when test="string-length(substring-before($unique_match,$split_ext)) &gt; 0">
                  <xsl:value-of select="substring-before($unique_match,$split_ext)"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$unique_match"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:variable>
 
    <xsl:if test="string-length($match_after_delimiter) &gt; 0">
      <xsl:value-of select="concat(',',$double_quotes,'match',$double_quotes)"/>
      <xsl:value-of select="concat(' : ',$double_quotes,$match_after_delimiter,$double_quotes)"/>
    </xsl:if>
    
    <xsl:value-of select="'}'"/>
  </xsl:template>
  
  <xsl:template match="Match" mode="OnMatch">
    <xsl:value-of select="concat('~',@regex,'~')"/>
  </xsl:template>
  
  <xsl:template match="OnBlur" mode="OnBlurMode">
    <xsl:variable name="Action_vals">
      <xsl:apply-templates select="descendant-or-self::*[name() = 'Action']" mode="ActionMode"/>
    </xsl:variable>
    <xsl:variable name="unique_on_change">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$Action_vals"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="$split_ext"/>
      </xsl:call-template>  
    </xsl:variable>
    <xsl:call-template name="ProcessActionNodes">
      <xsl:with-param name="unique_list" select="$unique_on_change"/>
    </xsl:call-template>    
  </xsl:template>
  
  
  <xsl:template match="OnChange" mode="OnChangeMode">
    <xsl:variable name="Action_vals">
      <xsl:apply-templates select="descendant-or-self::*[name() = 'Action']" mode="ActionMode"/>
    </xsl:variable>
    <xsl:variable name="unique_on_change">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
        <xsl:with-param name="delimitedValues" select="$Action_vals"/>
        <xsl:with-param name="CalledFromWithin" select="'NO'"/>
        <xsl:with-param name="Delimiter" select="$split_ext"/>
      </xsl:call-template>  
    </xsl:variable>
    <xsl:call-template name="ProcessActionNodes">
      <xsl:with-param name="unique_list" select="$unique_on_change"/>
    </xsl:call-template>    
  </xsl:template>
  
  <xsl:template name="ProcessActionNodes">
    <xsl:param name="unique_list"/>
    <!--{
    "type": "replace",
    "find": "([A-Z])\\w+",
    "replace": "passed",
    "modifiers": "g"
    }, 
    "upper",
    "trim",
    "lower"-->
    <!--**MULTI_SEP**upper==SINGLE_SEP====SINGLE_SEP====SINGLE_SEP==**MULTI_SEP**trim==SINGLE_SEP====SINGLE_SEP====SINGLE_SEP==**MULTI_SEP**lower==SINGLE_SEP====SINGLE_SEP====SINGLE_SEP==**MULTI_SEP**replace==SINGLE_SEP==([A-Z])\\w+==SINGLE_SEP==passed==SINGLE_SEP==g-->
    <!-- Process One type and call this function again for other types -->
    <xsl:choose>
      <xsl:when test="not(contains($unique_list, $split_ext))">
        <xsl:call-template name="processActionText">
          <xsl:with-param name="action_node" select="$unique_list"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="processActionText">
          <xsl:with-param name="action_node" select="substring-before($unique_list, $split_ext)"/>
        </xsl:call-template>        
        <xsl:call-template name="ProcessActionNodes">
          <xsl:with-param name="unique_list" select="substring-after($unique_list, $split_ext)"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>
  
  <xsl:template name="processActionText">
    <xsl:param name="action_node"/>
    
    <xsl:variable name="type">
      <xsl:value-of select="substring-before($action_node,$split_int)"/>
    </xsl:variable>
    <xsl:variable name="first_remaining">
      <xsl:value-of select="substring-after($action_node,$type)"/>
    </xsl:variable>
    <xsl:variable name="find_plus_remaining">
      <xsl:value-of select="substring-after($first_remaining,$split_int)"/>
    </xsl:variable>
    <xsl:variable name="find">
      <xsl:value-of select="substring-before($find_plus_remaining,$split_int)"/>
    </xsl:variable>
    <xsl:variable name="second_remaining">
      <xsl:value-of select="substring-after($find_plus_remaining,$split_int)"/>
    </xsl:variable>
    <xsl:variable name="replace">
      <xsl:value-of select="substring-before($second_remaining,$split_int)"/>
    </xsl:variable>
    <xsl:variable name="third_remaining">
      <xsl:value-of select="substring-after($second_remaining,$replace)"/>
    </xsl:variable>
    <xsl:variable name="modifiers">
      <xsl:value-of select="substring-after($third_remaining,$split_int)"/>
    </xsl:variable>
       
    <xsl:choose>
      <xsl:when test="string-length($type) &gt; 0">
        <xsl:choose>
          <xsl:when test="string-length($find) &lt; 1 and string-length($replace) &lt; 1 and string-length($modifiers) &lt; 1">
            <xsl:value-of select="concat($double_quotes,$type,$double_quotes,',')"/>
          </xsl:when>
          <xsl:otherwise>
            <!-- {
            "type": "replace",
            "find": "([A-Z])\\w+",
            "replace": "passed",
            "modifiers": "g"
            },  -->
            <xsl:value-of select="concat('{',$double_quotes,'type',$double_quotes)"/>
            <xsl:value-of select="concat(' : ',$double_quotes,$type,$double_quotes)"/>
            <xsl:if test="string-length($find) &gt; 0">
              <xsl:value-of select="concat(',',$double_quotes,'find',$double_quotes)"/>
              <xsl:value-of select="concat(' : ',$double_quotes,$find,$double_quotes)"/>
            </xsl:if>
            <xsl:if test="string-length($replace) &gt; 0">
              <xsl:value-of select="concat(',',$double_quotes,'replace',$double_quotes)"/>
              <xsl:value-of select="concat(' : ',$double_quotes,$replace,$double_quotes)"/>
            </xsl:if>
            <xsl:if test="string-length($modifiers) &gt; 0">
              <xsl:value-of select="concat(',',$double_quotes,'modifiers',$double_quotes)"/>
              <xsl:value-of select="concat(' : ',$double_quotes,$modifiers,$double_quotes)"/>
            </xsl:if>
            <xsl:value-of select="'}'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
    
    
  </xsl:template>
  
  
  <xsl:template match="Action" mode="ActionMode" >
    <xsl:value-of select="concat('~',@type,$split_int,@find,$split_int,@replace,$split_int,@modifiers,'~')"/>
  </xsl:template>

  <xsl:template name="ResultDetailText">
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable> 
    <xsl:value-of select="'{'"/>
    <xsl:call-template name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template> "name" : "<xsl:value-of select="$Field_name_to_use"/>", "prefix" :
      "<xsl:value-of select="@prefix_to_use"/>", "type" : "<xsl:value-of
      select="ancestor::DataItem[1]/@type_for_ui_json"/>" 
    <xsl:if test="ancestor::DataItem[1]/@mapped='true'">
      <xsl:value-of select="concat(',',$double_quotes,'map',$double_quotes,' : ')"/>
      <xsl:value-of select="concat($double_quotes,ancestor::DataItem[1]/@final_ui_label,$double_quotes)"/>
    </xsl:if>
    <xsl:value-of select="'}'"/>
  </xsl:template>

  <xsl:template name="MetadataFieldText">
    <xsl:variable name="DataType">
      <xsl:choose>
        <xsl:when test="ancestor::DataItem[1]/@type_for_ui_json != ''">
          <xsl:value-of select="ancestor::DataItem[1]/@type_for_ui_json"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'string'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable> { <xsl:call-template name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template> "name" : "<xsl:value-of select="$Field_name_to_use"/>", "type" :
      "<xsl:value-of select="$DataType"/>" <xsl:if test="@span_to_use = 'true'">, "span" : true </xsl:if>
    <xsl:if test="ancestor::DataItem[1]/@split != ''">, "multivalue" : true</xsl:if> 
    <xsl:if test="ancestor::DataItem[1]/@mapped='true'">
      <xsl:value-of select="concat(',',$double_quotes,'map',$double_quotes,' : ')"/>
      <xsl:value-of select="concat($double_quotes,ancestor::DataItem[1]/@final_ui_label,$double_quotes)"/>
    </xsl:if>
    <xsl:value-of select="'}'"/>
  </xsl:template>

  <xsl:template name="BooleanText">
    <xsl:param name="Prefix"/>
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable>    
    <xsl:value-of select="concat('{',$double_quotes,'name',$double_quotes,' : ','&quot;', $Field_name_to_use, '&quot;',',')"/> 
    <xsl:call-template
      name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template> "termPrefix" : <xsl:value-of
      select="concat($double_quotes, $Prefix, $double_quotes)"/>
    <xsl:if test="@subdoc_flag_to_use = 'true'"> , "subdoc" : true </xsl:if>
    <xsl:if test="ancestor::DataItem[1]/@mapped='true'">
      <xsl:value-of select="concat(',',$double_quotes,'map',$double_quotes,' : ')"/>
      <xsl:value-of select="concat($double_quotes,ancestor::DataItem[1]/@final_ui_label,$double_quotes)"/>
    </xsl:if>
    <xsl:if test="@length_to_use != ''"> , <xsl:value-of
        select="concat($double_quotes, 'length', $double_quotes, ' : ')"/>
      <xsl:value-of select="concat($double_quotes, @length_to_use, $double_quotes)"/>
    </xsl:if> } </xsl:template>

  <xsl:template name="HierarchicalText">
    <xsl:param name="Prefix"/>
    
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable>
    
    <xsl:value-of select="concat('{',$double_quotes,'name',$double_quotes,' : ','&quot;', $Field_name_to_use, '&quot;',',')"/>
    
    <xsl:call-template
      name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template>
    
    "termPrefix" : <xsl:value-of
      select="concat($double_quotes, $Prefix, $double_quotes)"/>, "hierarchical" : true 
    <xsl:if test="@subdoc_flag_to_use = 'true'"> , "subdoc" : true 
    </xsl:if>
    
    <xsl:if test="@separator_to_use != ''"> , "separator" : "<xsl:value-of
        select="@separator_to_use"/>" 
    </xsl:if>
    
    <xsl:if test="ancestor::DataItem[1]/@num_labels &gt; 0">
      <xsl:value-of select="concat(',',$double_quotes,'label',$double_quotes,' :{')"/>
      <xsl:value-of select="concat($double_quotes,'titles',$double_quotes,': [ ')"/>
      <xsl:value-of select="concat(ancestor::DataItem[1]/Filter/Hierarchical/Labels/@Stripped_All_labels,' ]}')"/>
    </xsl:if>
        
    } </xsl:template>

  <xsl:template name="put_title_desc_values">
    <xsl:param name="Title"/>
    <xsl:param name="Desc"/>
    <xsl:if test="$Title != ''">
      <xsl:value-of select="concat($double_quotes, 'title', $double_quotes, ' : ')"/>
      <xsl:value-of select="concat($double_quotes, $Title, $double_quotes, ',')"/>
    </xsl:if>
    <xsl:if test="$Desc != ''">
      <xsl:value-of select="concat($double_quotes, 'desc', $double_quotes, ' : ')"/>
      <xsl:value-of select="concat($double_quotes, $Desc, $double_quotes, ',')"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="ExportReportTextQuery">
    <xsl:value-of select="concat('{',$double_quotes,'name',$double_quotes,' : ')"/>
    <xsl:value-of select="concat($double_quotes, @name_to_use,$double_quotes,',')"/>
    <xsl:value-of select="concat($double_quotes,'title',$double_quotes,' : ')"/>
    <xsl:value-of select="concat($double_quotes,@title_to_use,$double_quotes)"/>
    <xsl:if test="ancestor::DataItem[1]/@mapped='true'">
      <xsl:value-of select="concat(',',$double_quotes,'map',$double_quotes,' : ')"/>
      <xsl:value-of select="concat($double_quotes, @name_to_use,$double_quotes)"/>
    </xsl:if>
    <xsl:if test="count(ancestor::DataItem[1]/Security) &gt; 0">
      <xsl:value-of select="concat(',',$double_quotes,'security',$double_quotes)"/>
      <xsl:value-of select="concat(' :{',$double_quotes,'highest',$double_quotes)"/>
      <xsl:value-of select="concat(' : ',$double_quotes,'highestSecurity',$double_quotes)"/>
      <xsl:value-of select="concat(',',$double_quotes,'notify',$double_quotes)"/>
      <xsl:value-of select="concat(' : ',$double_quotes,'securityNotify',$double_quotes)"/>
      <xsl:value-of select="concat(',',$double_quotes,'text',$double_quotes)"/>
      <xsl:value-of select="concat(' : ',$double_quotes,ancestor::DataSetConfig[1]/Security/Notification/@text,$double_quotes)"/>
      <xsl:value-of select="'}'"/>
    </xsl:if>
    <xsl:value-of select="'}'"/>    
  </xsl:template>
  
  <xsl:template name="HighlightTextQuery">
   <xsl:variable name="name_to_use">
      <xsl:choose>
        <xsl:when test="ancestor::DataItem[1]/@name!=''">
          <xsl:value-of select="ancestor::DataItem[1]/@name"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="title_to_use">
      <xsl:choose>
        <xsl:when test="@title!=''">
          <xsl:value-of select="@title"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="ancestor::DataItem[1]/@title!=''">
              <xsl:value-of select="ancestor::DataItem[1]/@title"/>    
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ancestor::DataItem[1]/@name"/>
            </xsl:otherwise>
          </xsl:choose>          
        </xsl:otherwise>
      </xsl:choose>  
    </xsl:variable>
    
    <xsl:variable name="true_flag">
      <xsl:choose>
        <xsl:when test="@trueValue!=''">
          <xsl:value-of select="@trueValue"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:message terminate="yes">
            <xsl:value-of select="concat('Attribute trueValue is mandatory in Highlight tag for DataItem ',$name_to_use)"/>
          </xsl:message>  
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="rgb_value">
      <xsl:choose>
        <xsl:when test="@rgb!=''">
          <xsl:value-of select="@rgb"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:message terminate="yes">
            <xsl:value-of select="concat('Attribute rgb is mandatory in Highlight tag for DataItem ',$name_to_use)"/>
          </xsl:message>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!--"name": "hwicd",
    "true": "Y",
    "title": "HWI",
    "rgb": "#123456"-->
    <xsl:value-of select="concat('{',$double_quotes,'name',$double_quotes,': ')"/>
    <xsl:value-of select="concat($double_quotes, $name_to_use,$double_quotes,',')"/>
    <xsl:value-of select="concat($double_quotes,'title',$double_quotes,': ')"/>
    <xsl:value-of select="concat($double_quotes,$title_to_use,$double_quotes,',')"/>
    <xsl:value-of select="concat($double_quotes,'true',$double_quotes,': ')"/>
    <xsl:value-of select="concat($double_quotes,$true_flag,$double_quotes,',')"/>
    <xsl:value-of select="concat($double_quotes,'color',$double_quotes,': ')"/>
    <xsl:value-of select="concat($double_quotes,$rgb_value,$double_quotes,'}')"/>
    
  </xsl:template>

  <xsl:template name="dateRangeTextQuery">
    <xsl:param name="Slot"/>
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable>
    <xsl:value-of select="concat('{',$double_quotes,'name',$double_quotes,' : ', '&quot;', $Field_name_to_use, '&quot;',',')"/>
      <xsl:call-template name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template>
    <xsl:value-of select="concat($double_quotes, 'meta', $double_quotes)"/> : { <xsl:value-of
      select="concat($double_quotes, 'slot', $double_quotes)"/> : <xsl:value-of select="$Slot"/>} } </xsl:template>

  <xsl:template name="sortText">
    <xsl:param name="slot"/>
    <xsl:variable name="default_sort">
      <xsl:choose>
        <xsl:when test="@order_to_use != ''">
          <xsl:value-of select="@order_to_use"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'desc'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="Field_name_to_use">
      <xsl:value-of select="ancestor::DataItem[1]/@final_ui_label"/>
    </xsl:variable> { <xsl:call-template name="put_title_desc_values">
      <xsl:with-param name="Title" select="@title_to_use"/>
      <xsl:with-param name="Desc" select="@desc_to_use"/>
    </xsl:call-template> "name" : "<xsl:value-of select="$Field_name_to_use"/>" , "slot" :
      <xsl:value-of select="$slot"/>, "default" : "<xsl:value-of select="$default_sort"/>" } </xsl:template>

  <xsl:template name="subdocsTextQuery">
    <xsl:param name="subdoc_display"/>
    <xsl:param name="subdoc_preview"/>
    <xsl:param name="Metadata_Normal"/>
    <xsl:param name="Metadata_Preview"/>
    <xsl:param name="Detail_Normal"/>
    <xsl:param name="Detail_Preview"/>
    <xsl:param name="subdoc_ExportReport"/>
    <xsl:param name="Highlight_Sub_Normal"/>
    <xsl:param name="Highlight_Sub_Preview"/>


    <xsl:variable name="AllUICombined">
      <xsl:call-template name="create_Display_Comma_Sep">
        <xsl:with-param name="ExportReport" select="$subdoc_ExportReport"/>
        <xsl:with-param name="Display_Normal" select="$subdoc_display"/>
        <xsl:with-param name="Metadata_Normal" select="$Metadata_Normal"/>
        <xsl:with-param name="Detail_Normal" select="$Detail_Normal"/>
        <xsl:with-param name="Metadata_Preview" select="$Metadata_Preview"/>
        <xsl:with-param name="Display_Preview" select="$subdoc_preview"/>
        <xsl:with-param name="Detail_Preview" select="$Detail_Preview"/>
        <xsl:with-param name="Highlight_Normal" select="$Highlight_Sub_Normal"/>
        <xsl:with-param name="Highlight_Preview" select="$Highlight_Sub_Preview"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="$AllUICombined != ''">
      <xsl:value-of select="$AllUICombined"/>
    </xsl:if>
  </xsl:template>

  <!-- this is to process Filter  tag -->
  <xsl:template name="FilterQuery">
    <xsl:param name="templateName"/>
    <xsl:param name="Bool_Prefix"/>
    <xsl:param name="Hierarchy_Prefix"/>

    <xsl:variable name="FilterItem">
      <xsl:choose>
        <xsl:when test="$templateName = 'Boolean'">
          <xsl:call-template name="BooleanText">
            <xsl:with-param name="Prefix" select="$Bool_Prefix"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="HierarchicalText">
            <xsl:with-param name="Prefix" select="$Hierarchy_Prefix"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$FilterItem != ''">
        <xsl:value-of select="concat($FilterItem, ',')"/>
      </xsl:when>
    </xsl:choose>

  </xsl:template>

  <!-- this is to process SearchWithinResults inside any tag -->
  <xsl:template name="SearchWithinResultsQuery">
    <xsl:param name="Prefix"/>
    <xsl:param name="dataitem_name"/>
    
    <xsl:variable name="One_searchwithin">
      <xsl:call-template name="SearchWithinResultsText">
        <xsl:with-param name="Prefix" select="$Prefix"/>
        <xsl:with-param name="dataitem_name" select="$dataitem_name"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$One_searchwithin != ''">
        <xsl:value-of select="concat($One_searchwithin, ',')"/>
      </xsl:when>
    </xsl:choose>


  </xsl:template>

  <!-- this is to process Link Tag inside any Linked Tag -->
  <xsl:template name="LinkQuery">
    <xsl:param name="nodePosition"/>
    <xsl:param name="totalPosition"/>
    <xsl:param name="Link_Name"/>

    <xsl:value-of select="concat(',', $double_quotes, $Link_Name, $double_quotes, ': {')"/>

    <xsl:variable name="Icon_Sub_Display">
      <xsl:call-template name="process_Icon">
        <xsl:with-param name="ICON_LEVEL" select="'LINK_LEVEL'"/>
        <xsl:with-param name="FIRST_PARENT" select="'LINK'"/>
        <xsl:with-param name="SECOND_PARENT" select="'DATAITEM'"/>
        <xsl:with-param name="DISPLAY_OR_PREVIEW" select="'DISPLAY'"/>
        <xsl:with-param name="Exclude_Flag" select="'N'"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Sub_Display">
      <xsl:call-template name="Calculate_Sub_Display">
        <xsl:with-param name="icon_sub_line" select="$Icon_Sub_Display"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Icon_Sub_Preview">
      <xsl:call-template name="process_Icon">
        <xsl:with-param name="ICON_LEVEL" select="'LINK_LEVEL'"/>
        <xsl:with-param name="FIRST_PARENT" select="'LINK'"/>
        <xsl:with-param name="SECOND_PARENT" select="'DATAITEM'"/>
        <xsl:with-param name="DISPLAY_OR_PREVIEW" select="'PREVIEW'"/>
        <xsl:with-param name="Exclude_Flag" select="'Y'"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Sub_Preview">
      <xsl:call-template name="Calculate_Sub_Preview">
        <xsl:with-param name="icon_sub_line" select="$Icon_Sub_Preview"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="ExportReport_Link">
      <xsl:call-template name="processExportReport">        
        <xsl:with-param name="export_report_condition" select="./descendant::*[name() = 'ExportToReport' and ancestor::*[name() = 'Link']][@makeEntryUI = 'true']"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Metadata_Link_Normal">
      <xsl:call-template name="process_metadata_node">
        <xsl:with-param name="metadata_condition"
          select="./descendant::*[name() = 'Metadata' and ancestor::*[name() = 'Link'] and not(ancestor::*[name() = 'Preview'])][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Metadata_Link_Preview">
      <xsl:call-template name="process_metadata_node">
        <xsl:with-param name="metadata_condition"
          select="./descendant::*[name() = 'Metadata' and ancestor::*[name() = 'Link'] and (ancestor::*[name() = 'Preview'])][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Detail_Link_Normal">
      <xsl:call-template name="process_detail_node">
        <xsl:with-param name="detail_condition"
          select="./descendant::*[name() = 'Details' and ancestor::*[name() = 'Link'] and not(ancestor::*[name() = 'Preview'])][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="Detail_Link_Preview">
      <xsl:call-template name="process_detail_node">
        <xsl:with-param name="detail_condition"
          select="./descendant::*[name() = 'Details' and ancestor::*[name() = 'Link'] and (ancestor::*[name() = 'Preview'])][@makeEntryUI = 'true']"
        />
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Highlight_sub_normal">
      <xsl:call-template name="processHighlight">
        <xsl:with-param name="highlight_condition" select="./descendant::*[name() = 'Highlight' and ancestor::*[name() = 'Link'] and not(ancestor::*[name() = 'Preview'])]"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:variable name="Highlight_sub_preview">
      <xsl:call-template name="processHighlight">
        <xsl:with-param name="highlight_condition" select="./descendant::*[name() = 'Highlight' and ancestor::*[name() = 'Link'] and ancestor::*[name() = 'Preview']]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$nodePosition = $totalPosition and $totalPosition = 1">
        <xsl:call-template name="subdocsTextQuery">
          <xsl:with-param name="subdoc_preview" select="$Sub_Preview"/>
          <xsl:with-param name="subdoc_display" select="$Sub_Display"/>
          <xsl:with-param name="Metadata_Normal" select="$Metadata_Link_Normal"/>
          <xsl:with-param name="Metadata_Preview" select="$Metadata_Link_Preview"/>
          <xsl:with-param name="Detail_Normal" select="$Detail_Link_Normal"/>
          <xsl:with-param name="Detail_Preview" select="$Detail_Link_Preview"/>
          <xsl:with-param name="subdoc_ExportReport" select="$ExportReport_Link"/>
          <xsl:with-param name="Highlight_Sub_Normal" select="$Highlight_sub_normal"/>
          <xsl:with-param name="Highlight_Sub_Preview" select="$Highlight_sub_preview"/>          
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$nodePosition &lt; $totalPosition and $nodePosition = 1">
        <xsl:call-template name="subdocsTextQuery">
          <xsl:with-param name="subdoc_preview" select="$Sub_Preview"/>
          <xsl:with-param name="subdoc_display" select="$Sub_Display"/>
          <xsl:with-param name="Metadata_Normal" select="$Metadata_Link_Normal"/>
          <xsl:with-param name="Metadata_Preview" select="$Metadata_Link_Preview"/>
          <xsl:with-param name="Detail_Normal" select="$Detail_Link_Normal"/>
          <xsl:with-param name="Detail_Preview" select="$Detail_Link_Preview"/>
          <xsl:with-param name="subdoc_ExportReport" select="$ExportReport_Link"/>
          <xsl:with-param name="Highlight_Sub_Normal" select="$Highlight_sub_normal"/>
          <xsl:with-param name="Highlight_Sub_Preview" select="$Highlight_sub_preview"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$nodePosition = $totalPosition">
        <xsl:call-template name="subdocsTextQuery">
          <xsl:with-param name="subdoc_preview" select="$Sub_Preview"/>
          <xsl:with-param name="subdoc_display" select="$Sub_Display"/>
          <xsl:with-param name="Metadata_Normal" select="$Metadata_Link_Normal"/>
          <xsl:with-param name="Metadata_Preview" select="$Metadata_Link_Preview"/>
          <xsl:with-param name="Detail_Normal" select="$Detail_Link_Normal"/>
          <xsl:with-param name="Detail_Preview" select="$Detail_Link_Preview"/>
          <xsl:with-param name="subdoc_ExportReport" select="$ExportReport_Link"/>
          <xsl:with-param name="Highlight_Sub_Normal" select="$Highlight_sub_normal"/>
          <xsl:with-param name="Highlight_Sub_Preview" select="$Highlight_sub_preview"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="subdocsTextQuery">
          <xsl:with-param name="subdoc_display" select="$Sub_Display"/>
          <xsl:with-param name="subdoc_preview" select="$Sub_Preview"/>
          <xsl:with-param name="Metadata_Normal" select="$Metadata_Link_Normal"/>
          <xsl:with-param name="Metadata_Preview" select="$Metadata_Link_Preview"/>
          <xsl:with-param name="Detail_Normal" select="$Detail_Link_Normal"/>
          <xsl:with-param name="Detail_Preview" select="$Detail_Link_Preview"/>
          <xsl:with-param name="subdoc_ExportReport" select="$ExportReport_Link"/>
          <xsl:with-param name="Highlight_Sub_Normal" select="$Highlight_sub_normal"/>
          <xsl:with-param name="Highlight_Sub_Preview" select="$Highlight_sub_preview"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:value-of select="'}'"/>

  </xsl:template>

  <xsl:template name="Linked_Template">
    <xsl:param name="Link_Count"/>
    
       
    <xsl:variable name="Icon_Linked">
      <xsl:call-template name="process_Icon">
        <xsl:with-param name="ICON_LEVEL" select="'LINKED_LEVEL'"/>
        <xsl:with-param name="FIRST_PARENT" select="'LINKED'"/>
        <xsl:with-param name="SECOND_PARENT" select="'DATAITEM'"/>
        <xsl:with-param name="DISPLAY_OR_PREVIEW" select="'DISPLAY'"/>
        <xsl:with-param name="Exclude_Flag" select="'N'"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:if test="string-length($Icon_Linked) &gt; 0">
      <xsl:value-of select="concat(',',$Icon_Linked)"/>
    </xsl:if>
    
    
    <xsl:for-each select="./descendant::*[name() = 'Link']">
      <xsl:variable name="Link_Name">
        <xsl:value-of select="./descendant-or-self::*[@typefield][1]/@typefield"/>
      </xsl:variable>
      <xsl:call-template name="LinkQuery">
        <xsl:with-param name="nodePosition">
          <xsl:number from="ConnectionType" level="any"/>
        </xsl:with-param>
        <xsl:with-param name="totalPosition" select="$Link_Count"/>
        <xsl:with-param name="Link_Name" select="$Link_Name"/>
      </xsl:call-template>
    </xsl:for-each>

  </xsl:template>

  <xsl:template name="create_Display_Comma_Sep">
    <xsl:param name="ExportReport"/>
    <xsl:param name="Display_Normal"/>
    <xsl:param name="Metadata_Normal"/>
    <xsl:param name="Detail_Normal"/>
    <xsl:param name="Metadata_Preview"/>
    <xsl:param name="Display_Preview"/>
    <xsl:param name="Detail_Preview"/>
    <xsl:param name="Highlight_Normal"/>
    <xsl:param name="Highlight_Preview"/>


    <xsl:variable name="Main_All">
      <xsl:if test="$Display_Normal != ''">
        <xsl:value-of select="concat($Display_Normal,',')"/>
      </xsl:if>
      <xsl:if test="$Metadata_Normal != ''">
        <xsl:value-of select="concat($Metadata_Normal,',')"/>        
      </xsl:if>     
      <xsl:if test="$Detail_Normal != ''">
        <xsl:value-of select="concat($Detail_Normal,',')"/>
      </xsl:if>      
      <xsl:if test="$ExportReport!=''">
          <xsl:value-of select="concat($ExportReport,',')"/>
      </xsl:if>
      <xsl:if test="$Highlight_Normal!=''">
          <xsl:value-of select="concat($Highlight_Normal,',')"/>
      </xsl:if>
    </xsl:variable>
    
    <xsl:variable name="stripped_Main_All">
      <xsl:choose>
        <xsl:when test="$Main_All != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$Main_All"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$Main_All"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="Preview_Without_tags">
      <xsl:if test="$Metadata_Preview != ''">
        <xsl:value-of select="concat($Metadata_Preview,',')"/>
      </xsl:if>
      <xsl:if test="$Display_Preview != ''">
        <xsl:value-of select="concat($Display_Preview,',')"/>
      </xsl:if>
      <xsl:if test="$Detail_Preview != ''">
        <xsl:value-of select="concat($Detail_Preview,',')"/>
      </xsl:if>
      <xsl:if test="$Highlight_Preview !=''">
        <xsl:value-of select="concat($Highlight_Preview,',')"/>
      </xsl:if>      
    </xsl:variable>
    
    <xsl:variable name="stripped_preview">
      <xsl:choose>
        <xsl:when test="$Preview_Without_tags != ''">
          <xsl:call-template name="strip-end-comma">
            <xsl:with-param name="text" select="$Preview_Without_tags"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$Preview_Without_tags"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="Preview_All">
      <xsl:choose>
        <xsl:when test="$stripped_preview != ''">
          <xsl:value-of select="concat($Preview_Pre, $stripped_preview, $Preview_Post)"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="AllUICombinedDisplay">
      <xsl:value-of select="$stripped_Main_All"/>
           
      <xsl:if test="$Preview_All != ''">
        <xsl:value-of select="concat(',',$Preview_All)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:value-of select="$AllUICombinedDisplay"/>
  </xsl:template>

  <xsl:template name="Calculate_Main_Normal">
    <xsl:param name="Icon_Main_Normal"/>
    <xsl:variable name="count_Caption_Main">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Display/Caption[not(ancestor::*[name() = 'Link'])])"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Caption_Main &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 caption at main level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="Caption_Main_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][./Display/Caption][not(ancestor::*[name() = 'Link'])]/@final_ui_label"/>
    </xsl:variable>

    <xsl:variable name="count_Snippet_Main">
      <xsl:value-of
        select="count(descendant-or-self::*[name() = 'DataItem']/Display/Sample[not(ancestor::*[name() = 'Link'])])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Snippet_Main &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 snippet at main level(not under Link)</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>


    <xsl:variable name="Snippet_Main_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][./Display/Sample][not(ancestor::*[name() = 'Link'])]/@final_ui_label"
      />
    </xsl:variable>


    <xsl:variable name="count_URL_Main">
      <xsl:value-of
        select="count(descendant-or-self::*[name() = 'DataItem']/Display/URL[not(ancestor::*[name() = 'Link'])])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_URL_Main &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 url at main level (not under Link)</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="URL_Main_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][./Display/URL][not(ancestor::*[name() = 'Link'])]/@final_ui_label"
      />
    </xsl:variable>
    
    <xsl:variable name="URL_Type_Main_Display">
      <xsl:value-of
        select="key('URL_By_DataItem_Name',descendant-or-self::*[name() = 'DataItem'][./Display/URL][not(ancestor::*[name() = 'Link'])]/@name)/@Type_label"
      />
    </xsl:variable>

    <xsl:variable name="Caption_Main_Line">
      <xsl:if test="$Caption_Main_Value != ''">
        <xsl:value-of
          select="concat('&quot;', 'caption', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Caption_Main_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="Snippet_Main_Line">
      <xsl:if test="$Snippet_Main_Value != ''">
        <xsl:if test="$Caption_Main_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'snippet', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Snippet_Main_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="URL_Main_Line">
      <xsl:if test="$URL_Main_Value != ''">
        <xsl:if test="$Caption_Main_Line != '' or $Snippet_Main_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'url', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $URL_Main_Value, '&quot;')"
        />
        <xsl:if test="$URL_Type_Main_Display!=''">
          <xsl:value-of select="concat(',','&quot;','type','&quot;',' : ','&quot;',normalize-space($URL_Type_Main_Display),'&quot;')"/>
        </xsl:if>
        <xsl:value-of select="concat( $newLine, '}')"/>        
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="All_Items_without_icon">
      <xsl:value-of select="concat($Caption_Main_Line, $Snippet_Main_Line, $URL_Main_Line)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$Icon_Main_Normal != ''">
        <xsl:choose>
          <xsl:when test="$All_Items_without_icon != ''">
            <xsl:value-of select="concat($All_Items_without_icon, ',', $Icon_Main_Normal)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$Icon_Main_Normal"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$All_Items_without_icon"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="Calculate_Main_Preview">
    <xsl:param name="Icon_Main_Preview"/>

    <xsl:variable name="count_Caption_Main">
      <xsl:value-of
        select="count(descendant-or-self::*[name() = 'DataItem']/Preview/Caption[not(ancestor::*[name() = 'Link'])])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Caption_Main &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 caption at main level (not under Link)</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="Caption_Main_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][./Preview/Caption][not(ancestor::*[name() = 'Link'])]/@final_ui_label"
      />
    </xsl:variable>

    <xsl:variable name="count_Snippet_Main">
      <xsl:value-of
        select="count(descendant-or-self::*[name() = 'DataItem']/Preview/Sample[not(ancestor::*[name() = 'Link'])])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Snippet_Main &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 snippet at main level (not under Link)</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>


    <xsl:variable name="Snippet_Main_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][./Preview/Sample][not(ancestor::*[name() = 'Link'])]/@final_ui_label"
      />
    </xsl:variable>


    <xsl:variable name="count_URL_Main">
      <xsl:value-of
        select="count(descendant-or-self::*[name() = 'DataItem']/Preview/URL[not(ancestor::*[name() = 'Link'])])"
      />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_URL_Main &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 url at main level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="URL_Main_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][./Preview/URL][not(ancestor::*[name() = 'Link'])]/@final_ui_label"
      />
    </xsl:variable>
    
    <xsl:variable name="URL_Type_Main_Value">
      <xsl:value-of
        select="key('URL_By_DataItem_Name',descendant-or-self::*[name() = 'DataItem'][./Preview/URL][not(ancestor::*[name() = 'Link'])]/@name)/@Type_label"
      />
    </xsl:variable>

    <xsl:variable name="Caption_Main_Line">
      <xsl:if test="$Caption_Main_Value != ''">
        <xsl:value-of
          select="concat('&quot;', 'caption', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Caption_Main_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="Snippet_Main_Line">
      <xsl:if test="$Snippet_Main_Value != ''">
        <xsl:if test="$Caption_Main_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'snippet', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Snippet_Main_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="URL_Main_Line">
      <xsl:if test="$URL_Main_Value != ''">
        <xsl:if test="$Caption_Main_Line != '' or $Snippet_Main_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'url', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $URL_Main_Value, '&quot;')"
        />
        <xsl:if test="$URL_Type_Main_Value!=''">
          <xsl:value-of select="concat(',','&quot;','type','&quot;',' : ','&quot;',normalize-space($URL_Type_Main_Value),'&quot;')"/>
        </xsl:if>
        <xsl:value-of select="concat( $newLine, '}')"/>
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="All_Preview">
      <xsl:value-of select="concat($Caption_Main_Line, $Snippet_Main_Line, $URL_Main_Line)"/>  
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="string-length($Icon_Main_Preview) &gt; 0">
        <xsl:choose>
          <xsl:when test="$All_Preview != ''">
            <xsl:value-of select="concat($All_Preview, ',', $Icon_Main_Preview)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$Icon_Main_Preview"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$All_Preview"/>
      </xsl:otherwise>
    </xsl:choose>    
    
  </xsl:template>

  <xsl:template name="Calculate_Sub_Display">
    <xsl:param name="icon_sub_line"/>
    <xsl:variable name="count_Caption_Sub">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Display/Caption)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Caption_Sub &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 caption at sub doc level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="Caption_Sub_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][Display/Caption]/@final_ui_label"/>
    </xsl:variable>

    <xsl:variable name="count_Snippet_Sub">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Display/Sample)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Snippet_Sub &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 snippet at Sub level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="Snippet_Sub_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][Display/Sample]/@final_ui_label"/>
    </xsl:variable>

    <xsl:variable name="count_URL_Sub">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Display/URL)"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$count_URL_Sub &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 url at subdoc level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="URL_Sub_Value">
      <xsl:value-of select="descendant-or-self::*[name() = 'DataItem'][Display/URL]/@final_ui_label"
      />
    </xsl:variable>
    
    <xsl:variable name="URL_Type_Sub_Display">
      <xsl:value-of
        select="key('URL_By_DataItem_Name',descendant-or-self::*[name() = 'DataItem'][Display/URL]/@name)/@Type_label"
      />
    </xsl:variable>
    

    <xsl:variable name="Caption_Sub_Line">
      <xsl:if test="$Caption_Sub_Value != ''">
        <xsl:value-of
          select="concat('&quot;', 'caption', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Caption_Sub_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="Snippet_Sub_Line">
      <xsl:if test="$Snippet_Sub_Value != ''">
        <xsl:if test="$Caption_Sub_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'snippet', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Snippet_Sub_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="URL_Sub_Line">
      <xsl:if test="$URL_Sub_Value != ''">
        <xsl:if test="$Caption_Sub_Line != '' or $Snippet_Sub_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'url', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $URL_Sub_Value, '&quot;')"
        />
        <xsl:if test="$URL_Type_Sub_Display!=''">
          <xsl:value-of select="concat(',','&quot;','type','&quot;',' : ','&quot;',normalize-space($URL_Type_Sub_Display),'&quot;')"/>
        </xsl:if>
        <xsl:value-of select="concat( $newLine, '}')"/>      
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="All_Items_without_icon">
      <xsl:value-of select="concat($Caption_Sub_Line, $Snippet_Sub_Line, $URL_Sub_Line)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$icon_sub_line != ''">
        <xsl:choose>
          <xsl:when test="$All_Items_without_icon != ''">
            <xsl:value-of select="concat($All_Items_without_icon, ',', $icon_sub_line)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$icon_sub_line"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$All_Items_without_icon"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="Calculate_Sub_Preview">
    <xsl:param name="icon_sub_line"/>

    <xsl:variable name="count_Caption_Sub_Preview">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Preview/Caption)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Caption_Sub_Preview &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 caption at sub doc level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="Caption_Sub_Preview_Value">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][Preview/Caption]/@final_ui_label"/>
    </xsl:variable>

    <xsl:variable name="count_Snippet_Sub_Preview">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Preview/Sample)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_Snippet_Sub_Preview &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 snippet at Sub level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="Snippet_Sub_Value_Preview">
      <xsl:value-of
        select="descendant-or-self::*[name() = 'DataItem'][Preview/Sample]/@final_ui_label"/>
    </xsl:variable>

    <xsl:variable name="count_URL_Sub_Preview">
      <xsl:value-of select="count(descendant-or-self::*[name() = 'DataItem']/Preview/URL)"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$count_URL_Sub_Preview &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>There cannot be more than 1 url at subdoc level</xsl:text>
        </xsl:message>
      </xsl:when>
    </xsl:choose>

    <xsl:variable name="URL_Sub_Value_Preview">
      <xsl:value-of select="descendant-or-self::*[name() = 'DataItem'][Preview/URL]/@final_ui_label"
      />
    </xsl:variable>
    
    <xsl:variable name="URL_Type_Sub_Preview">
      <xsl:value-of
        select="key('URL_By_DataItem_Name',descendant-or-self::*[name() = 'DataItem'][Preview/URL]/@name)/@Type_label"
      />
    </xsl:variable>

    <xsl:variable name="Caption_Sub_Line">
      <xsl:if test="$Caption_Sub_Preview_Value != ''">
        <xsl:value-of
          select="concat('&quot;', 'caption', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Caption_Sub_Preview_Value, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="Snippet_Sub_Line">
      <xsl:if test="$Snippet_Sub_Value_Preview != ''">
        <xsl:if test="$Caption_Sub_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'snippet', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $Snippet_Sub_Value_Preview, '&quot;', $newLine, '}')"
        />
      </xsl:if>
    </xsl:variable>

    <xsl:variable name="URL_Sub_Line">
      <xsl:if test="$URL_Sub_Value_Preview != ''">
        <xsl:if test="$Caption_Sub_Line != '' or $Snippet_Sub_Line != ''">
          <xsl:text>,</xsl:text>
        </xsl:if>
        <xsl:value-of
          select="concat('&quot;', 'url', '&quot;', ' : ', '{', $newLine, '&quot;', 'name', '&quot;', ':', '&quot;', $URL_Sub_Value_Preview, '&quot;')"
        />
        <xsl:if test="$URL_Type_Sub_Preview!=''">
          <xsl:value-of select="concat(',','&quot;','type','&quot;',' : ','&quot;',normalize-space($URL_Type_Sub_Preview),'&quot;')"/>
        </xsl:if>
        <xsl:value-of select="concat( $newLine, '}')"/> 
      </xsl:if>
    </xsl:variable>
    
    <xsl:variable name="All_Items_without_icon">
      <xsl:value-of select="concat($Caption_Sub_Line, $Snippet_Sub_Line, $URL_Sub_Line)"/>  
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$icon_sub_line != ''">
        <xsl:choose>
          <xsl:when test="$All_Items_without_icon != ''">
            <xsl:value-of select="concat($All_Items_without_icon, ',', $icon_sub_line)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$icon_sub_line"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$All_Items_without_icon"/>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

</xsl:stylesheet>
