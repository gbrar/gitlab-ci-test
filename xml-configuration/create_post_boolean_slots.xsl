<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:ext="http://exslt.org/common">
  <xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:include href="create_pre_boolean_slots.xsl"/>

  <xsl:template match="/">
    <xsl:variable name="preProcessXMLFor_boolean_slots">
      <xsl:apply-templates mode="preprocess_boolean_slots"/>
    </xsl:variable>
    <xsl:apply-templates select="ext:node-set($preProcessXMLFor_boolean_slots)/*"/>
  </xsl:template>

  <xsl:template match="DataSetConfig">
    <xsl:apply-templates select="DataSet"/>
  </xsl:template>

  <xsl:template match="DataSet">
    
    <xsl:variable name="slots_dataitem">
      <xsl:apply-templates mode="preprocess_boolean_slots"
        select="./descendant::*[name() = 'Sort' or name() = 'DateRangeField'][@makeEntryUI = 'true']"
      />
    </xsl:variable>

    <xsl:variable name="Unique_Slots">
      <xsl:call-template name="processUniqueValues" xml:space="preserve">
                <xsl:with-param name="delimitedValues" select="$slots_dataitem"/>
                <xsl:with-param name="CalledFromWithin" select="'NO'"/>
                <xsl:with-param name="Delimiter" select="','"/>
            </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="result_no_leading_comma">
      <xsl:call-template name="strip-beginning-comma">
        <xsl:with-param name="text" select="$Unique_Slots"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="string-length($result_no_leading_comma) &gt; 0">
      <xsl:choose>
        <xsl:when
          test="substring($result_no_leading_comma, string-length($result_no_leading_comma)) = ','">
          <xsl:value-of select="$result_no_leading_comma"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($result_no_leading_comma, ',')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>

    <xsl:for-each
      select="//DataItem[generate-id(.) = generate-id(key('DataItems_By_Name', @name)[1])]">
      <!--Do something with the unique list of elements-->
      <xsl:call-template name="processDataItem"/>
    </xsl:for-each>

  </xsl:template>

  <xsl:template name="processDataItem">

    <xsl:variable name="first_boolean_prefix">
      <xsl:call-template name="get_boolean_prefixes"/>
    </xsl:variable>

    <xsl:if test="string-length($first_boolean_prefix) &gt; 0">
      <xsl:value-of select="concat($first_boolean_prefix, ',', @final_dataitem_label, ',')"/>
    </xsl:if>

  </xsl:template>

  <xsl:template name="get_boolean_prefixes">

    <xsl:variable name="delimited_Boolean_prefix">
      <xsl:for-each select="key('DataItems_By_Name', @name)">
        <xsl:if test="@Is_Boolean_present = 'true'">
          <xsl:value-of select="concat(normalize-space(@Boolean_prefix), '**MULTI_SEP**')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>

    <xsl:if test="string-length($delimited_Boolean_prefix) &gt; 0">
      <xsl:value-of select="substring-before($delimited_Boolean_prefix, '**MULTI_SEP**')"/>
    </xsl:if>

  </xsl:template>

  <xsl:template match="DateRangeField[@makeEntryUI = 'true']" mode="preprocess_boolean_slots">
    <xsl:value-of select="concat(@slot, ',', ancestor::DataItem[1]/@final_dataitem_label, '~')"/>
  </xsl:template>

  <xsl:template match="Sort[@makeEntryUI = 'true']" mode="preprocess_boolean_slots">
    <xsl:value-of select="concat(@slot, ',', ancestor::DataItem[1]/@final_dataitem_label, '~')"/>
  </xsl:template>

</xsl:stylesheet>
